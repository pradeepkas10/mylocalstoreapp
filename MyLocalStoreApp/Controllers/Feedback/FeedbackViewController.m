//
//  FeedbackViewController.m
//  RConnect
//
//  Created by HiteshDhawan on 27/10/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "FeedbackViewController.h"
#import "MyLocalStoreApp.pch"

@interface FeedbackViewController ()
{
    NSInteger ratingIndex;
    CGFloat ratingValue;

    UIActivityIndicatorView *refreshActivityView;
    NSTimer * timer;
    int miliSecond;
    ASIHTTPRequest *requestFeedback;
    double lastTimeValue;
    BOOL showBgImage;
    
}

@end

@implementation FeedbackViewController
-(BOOL)prefersStatusBarHidden{
    return true;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [UIApplication sharedApplication].statusBarHidden = YES;
    
    self.navigationController.navigationBarHidden = YES;
    appDelegate.needAPIRefresh = NO;
    [self.btnRate setTitle:@"RATE" forState:UIControlStateNormal];
    self.btnRate.titleLabel.font = [UIFont fontWithName:LatoBold size:20];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"ISACTIVEFEEDBACK"])
    {
         self.activatedView.hidden = NO;
         self.deactivatedView.hidden = YES;
         
         [self.view sendSubviewToBack:self.deactivatedView];
         [self.view bringSubviewToFront:self.activatedView];
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"ISACTIVEFEEDBACK"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    else
    {
         self.activatedView.hidden = YES;
         self.deactivatedView.hidden = NO;
      
         [self.view sendSubviewToBack:self.activatedView];
         [self.view bringSubviewToFront:self.deactivatedView];
    }
     
    self.contentView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.contentView.layer.shadowOpacity = 1;
    self.contentView.layer.shadowOffset = CGSizeZero;
    self.contentView.layer.shadowRadius = 2;
    self.contentView.layer.cornerRadius = 3.0;
    
    self.contentView.layer.masksToBounds = NO;
}

- (IBAction)toggleSideMenuView:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];

     [self.scrollView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    
    // Load feedback
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    [appDelegate startIndicator];

    if (status) {
        [self loadFeedback];
    }
    else
    {
        self.dicFeedback = [[NSUserDefaults standardUserDefaults]objectForKey:DICFeedback];
        [self updatedatafromDict:self.dicFeedback];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    [self pauseTimer];
    
    // Cancel ASIHTTPRequest
    if (![requestFeedback isCancelled]) {
        
        [requestFeedback cancel];
        [requestFeedback clearDelegatesAndCancel];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark - API
#pragma mark API LOAD FEEDBACK

- (void)loadFeedback {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
//        NSString *_catString = @"SHOPEXPERIENCE";
        NSString *urlString = [NSString stringWithFormat:@"%@/ratings/categories/?UserID=%@&CategoryId=%@", BASEURL, appDelegate.strUserId,@""];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request startAsynchronous];
    }
    else {
        
        [refreshActivityView removeFromSuperview];
        _btnRefresh.hidden = NO;
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"No internet connection." andButtonTitle:@"Cancel"];
    }
    [refreshActivityView hidesWhenStopped];
}

#pragma mark
#pragma mark API RATE STORE

- (void)rateProduct {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        [SVProgressHUD showWithStatus:@"Submit Rating..." maskType:SVProgressHUDMaskTypeGradient];
        
        NSString *urlString = [NSString stringWithFormat:@"%@rating", BASEURL];
        NSString *_dateString = [HelperClass getDateString];
        
        NSString *ratingId = [[_dicFeedback valueForKey:@"PendingRating"] objectForKey:@"RatingId"];
//        NSString *StoreId = [_dicFeedback valueForKey:@"StoreId"];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%d%@", _dateString, appDelegate.strUserId, ratingId, (int)ratingValue, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        NSDictionary *paramDic = @{@"UserID":appDelegate.strUserId,
                                   @"RatingId":ratingId,
                                   @"Rating":[NSString stringWithFormat:@"%d", (int)ratingValue]};
                                  // @"StoreId":StoreId};
        NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
        [request setPostBody:[NSMutableData dataWithData:data]];
        
        [request startAsynchronous];
        
    }
    else {
        
        [refreshActivityView removeFromSuperview];
        _btnRefresh.hidden = NO;
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"No internet connection." andButtonTitle:@"Cancel"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [SVProgressHUD dismiss];
    [refreshActivityView removeFromSuperview];
    _btnRefresh.hidden = NO;
    
    appDelegate.needAPIRefresh = YES;

    [appDelegate stopIndicator];
    
    NSDictionary *dict_response = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];

    NSArray *arr_feedback = [[NSArray alloc]init];
    arr_feedback = dict_response[@"Categories"];
    _dicFeedback = arr_feedback[0];
    [self updatedatafromDict:self.dicFeedback];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [SVProgressHUD dismiss];
    _btnRefresh.hidden = NO;
    [refreshActivityView removeFromSuperview];
    [appDelegate stopIndicator];
    
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:request.error.localizedDescription andButtonTitle:@"Cancel"];
}



#pragma mark -
#pragma mark -
#pragma mark Local Methods
-(void)updatedatafromDict:(NSDictionary *)dict_feedback
{
    [appDelegate stopIndicator];
    
    self.dicFeedback = dict_feedback;
    [self pauseTimer];
    
    if ([[_dicFeedback allKeys] containsObject:@"Bonus"]) {
        
        [self pauseTimer];
        [self refreshButtonPressed:nil];
    }
    
    if ([[_dicFeedback valueForKey:@"RatingsPending"] integerValue] > 0) {
        
        self.activatedView.hidden = NO;
        self.deactivatedView.hidden = YES;
        [self.view sendSubviewToBack:self.deactivatedView];
        [self.view bringSubviewToFront:self.activatedView];

        if (IS_IPHONE_5  || IS_IPHONE_4_OR_LESS) {
            self.lblRateMore.font = [UIFont fontWithName:LatoRegular size:15.0];
            self.lblTotalRate.font = [UIFont fontWithName:LatoRegular size:15.0];
            self.lblShoppingTitle.font = [UIFont fontWithName:LatoBold   size:21.0];
            self.lblShoppingExp.font = [UIFont fontWithName:LatoRegular size:15.0];

            self.lblStatus.font = [UIFont fontWithName:LatoBold size:21.0];
        }
        else
        {
            self.lblRateMore.font = [UIFont fontWithName:LatoRegular size:16.0];
            self.lblTotalRate.font = [UIFont fontWithName:LatoRegular size:16.0];

        }
        NSString *staticStr = [NSString stringWithFormat:@"Thank you for visiting:"];

        self.thankYouLabel.attributedText = [self shoppingExperience:staticStr andTime:[NSString stringWithFormat:@"%@",[[[_dicFeedback valueForKey:@"PendingRating"] objectForKey:@"Store"] objectForKey:@"Name"]]];
        self.thankYouLabel.textAlignment = NSTextAlignmentCenter;
        self.thankYouLabel.hidden = NO;
        
        self.lblRateEnds.text = [NSString stringWithFormat:@"Rate the customer experience before time expires: %@", [self timeIntervaltoDate:[[_dicFeedback valueForKey:@"PendingRating"] objectForKey:@"TimeToLive"]]];
        
        int ratingVal = [[_dicFeedback valueForKey:@"RatingsSinceReward"] intValue] % 5;
        self.lblStatus.text = [NSString stringWithFormat:@"Status: %d out of 5 Ratings", ratingVal];
        self.lblRateMore.text = [NSString stringWithFormat:@"You need to give %ld more ratings.", (long)5 - ratingVal];
        self.lblTotalRate.text = [NSString stringWithFormat:@"Total number of ratings you have given your local stores: %d", [[_dicFeedback valueForKey:@"RatingsSinceReward"] intValue]];
        
        if ([[_dicFeedback valueForKey:@"RatingsSinceReward"] integerValue] > 0) {
            
            for (int i=200; i<=204; i++) {
                UIImageView *imageView = (UIImageView *)[self.view viewWithTag:i];
                if (i<ratingVal+200) imageView.image = [UIImage imageNamed:@"smile"];
                else imageView.image = [UIImage imageNamed:@"smile_grey"];
            }
        }
        
        [self startTimer];
        // Start countdown
        [self timeIntervaltoDate:[[_dicFeedback valueForKey:@"PendingRating"] objectForKey:@"TimeToLive"]];
    }
    else {
        
        self.activatedView.hidden = YES;
        self.deactivatedView.hidden = NO;
        [self.view sendSubviewToBack:self.activatedView];
        [self.view bringSubviewToFront:self.deactivatedView];

        if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
            self.lblRateMore.font = [UIFont fontWithName:LatoRegular size:13.0];
            self.lblTotalRate.font = [UIFont fontWithName:LatoRegular size:13.0];
            self.lblShoppingTitle.font = [UIFont fontWithName:LatoBold   size:19.0];
            self.lblShoppingExp.font = [UIFont fontWithName:LatoRegular size:13.0];
            self.lblStatus.font = [UIFont fontWithName:LatoBold size:19.0];
        }
       
        
        appDelegate.backlogVal = [[_dicFeedback valueForKey:@"RatingsPending"] intValue];
        
        int ratingVal = [[_dicFeedback valueForKey:@"RatingsSinceReward"] intValue]%5;
        self.lblStatus.text = [NSString stringWithFormat:@"Status: %d out of 5 Ratings", ratingVal];
        self.lblRateMore.text = [NSString stringWithFormat:@"You need to give %ld more ratings.", (long)5 - ratingVal];
        self.lblTotalRate.text = [NSString stringWithFormat:@"Total number of ratings you have given your local stores: %d", [[_dicFeedback valueForKey:@"RatingsSinceReward"] intValue]];
        
        self.lblRateEnds.text = @"";
        NSString *staticStr = [NSString stringWithFormat:@"Thank you for visiting:"];
        self.thankYouLabel.attributedText = [self shoppingExperience:staticStr andTime:staticStr];
        
        for (int i=100; i<=104; i++) {
            UIButton *btnRating = (UIButton *)[self.view viewWithTag:i];
            [btnRating setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
        }
        
        for (int i=200; i<=204; i++) {
            UIImageView *imageView = (UIImageView *)[self.view viewWithTag:i];
            if (i<ratingVal+200) imageView.image = [UIImage imageNamed:@"smile"];
            else imageView.image = [UIImage imageNamed:@"smile_grey"];
        }
        
        ratingValue = 0.0;
        [self pauseTimer];
    }
    
    if (ratingValue == 0)
    {
        self.btnRate.titleLabel.textColor = THEMECOLOR;//[UIColor darkGrayColor];
        self.btnRate.backgroundColor = [UIColor whiteColor];
        self.btnRate.layer.borderWidth = 0.5;
        self.btnRate.layer.borderColor = THEMECOLOR.CGColor;//[UIColor lightGrayColor].CGColor;
    }
    else
    {
        self.btnRate.titleLabel.textColor = [UIColor whiteColor];
        self.btnRate.backgroundColor = THEMECOLOR;
        self.btnRate.layer.borderWidth = 0.0;
        
    }
    
    [self.view setNeedsLayout];
    [self UpdateSideMenuTable];
    
}

-(void)UpdateSideMenuTable{
    
    NSInteger numberOfCoupons = [[NSUserDefaults standardUserDefaults] integerForKey:@"COUPONCOUNT"];
    NSInteger numberOfLoaylty = [[NSUserDefaults standardUserDefaults] integerForKey:@"LOYALTYCOUNT"];
    NSInteger numberOfRewards = [[NSUserDefaults standardUserDefaults] integerForKey:@"REWARDSCOUNT"];
    NSInteger numberOfOffers = [[NSUserDefaults standardUserDefaults] integerForKey:@"OFFERSCOUNT"];

    MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
    mo.arrCouponCount = (int)[[appDelegate.dicOffers objectForKey:@"Coupons"] count] - (int)numberOfCoupons;
    mo.arrLoyaltyCount = (int)[[appDelegate.dicOffers objectForKey:@"Loyalty"] count] - (int)numberOfLoaylty;
    mo.arrRewardsCount = (int)[[appDelegate.dicOffers objectForKey:@"Rewards"] count] - (int)numberOfRewards;
    mo.arrOffersCount = (int)[[appDelegate.dicOffers objectForKey:@"Offers"] count] - (int)numberOfOffers;

    mo.feedbackCount = appDelegate.backlogVal;
    
    [[NSUserDefaults standardUserDefaults] setInteger:[[appDelegate.dicOffers objectForKey:@"Loyalty"] count] forKey:@"LOYALTYCOUNT"];
    [[NSUserDefaults standardUserDefaults] setInteger:[[appDelegate.dicOffers objectForKey:@"Rewards"] count] forKey:@"REWARDSCOUNT"];
    [[NSUserDefaults standardUserDefaults] setInteger:[[appDelegate.dicOffers objectForKey:@"Coupons"] count] forKey:@"COUPONCOUNT"];
    [[NSUserDefaults standardUserDefaults] setInteger:[[appDelegate.dicOffers objectForKey:@"Offers"] count] forKey:@"OFFERSCOUNT"];

    [[NSUserDefaults standardUserDefaults] synchronize];
    [mo.tableView reloadData];
}



- (NSString *)timeIntervaltoDate:(NSString*)date
{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    NSDate *startDate = [NSDate date];
    
    NSDate *endDate = [f dateFromString:date];
    
    NSCalendar* currentCalendar = [NSCalendar currentCalendar];
    NSCalendarUnit unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *differenceComponents = [currentCalendar components:unitFlags fromDate:startDate toDate:endDate options:0];
    
    //    NSInteger yearDifference = [differenceComponents year];
    //    NSInteger monthDifference = [differenceComponents month];
    NSInteger dayDifference = [differenceComponents day];
    NSInteger hourDifference = [differenceComponents hour];
    NSInteger minuteDifference = [differenceComponents minute];
    NSInteger secondDifference = [differenceComponents second];
    
    int miliHours = (int)(((hourDifference * 60) * 60) * 1000);
    int miliMinuts = (int)((minuteDifference * 60) * 1000);
    int secondMili = (int)secondDifference * 1000;
    
    miliSecond = miliHours + miliMinuts + secondMili;
    
    if (dayDifference > 0) {
        return [NSString stringWithFormat:@"%02ld days",(long)dayDifference];
    }
    else {
        return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",(long)hourDifference, (long)minuteDifference, (long)secondDifference];
    }
    
    return nil;
}


- (NSMutableAttributedString *)shoppingExperience:(NSString *)nameString andTime:(NSString *)timeString {
    
    NSString *shoppingString = [NSString stringWithFormat:@"%@ %@", nameString,timeString];
    NSRange nameRange = [shoppingString rangeOfString:nameString];
    NSRange storeRange = [shoppingString rangeOfString:timeString];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:shoppingString];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:LatoRegular size:18] range:nameRange];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:LatoBold size:19] range:storeRange];
    
    return attributedString;
}



#pragma mark -
#pragma mark -
#pragma mark UIBarButtoItem Methods

- (IBAction)refreshButtonPressed:(id)sender {
    
    if (refreshActivityView == nil) {
        refreshActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        refreshActivityView.frame = self.btnRefresh.frame;
    }
    [refreshActivityView startAnimating];
 
    
    _btnRefresh.hidden = YES;
    [self.headerView addSubview:refreshActivityView];
    
    for (int i=100; i<=104; i++) {
        UIButton *btnRate = (UIButton *)[self.view viewWithTag:i];
        [btnRate setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
    }
    
    [self loadFeedback];
}


- (IBAction)submitRateButtonPressed:(id)sender {
    
    if ([[_dicFeedback valueForKey:@"RatingsPending"] integerValue] > 0 && ratingIndex > 0) {
        [self rateProduct];
    }
}

- (IBAction)starRatingButtonPressed:(id)sender {
    
    if ([[_dicFeedback valueForKey:@"RatingsPending"] integerValue] > 0) {
        
        NSInteger tagValue = ((UIButton *)sender).tag;
        ratingValue = 0.0;
        
        for (int i=100; i<=104; i++) {
            
            UIButton *btnRate = [self.view viewWithTag:i];
            if (i<=tagValue) {
                if (ratingIndex == i && ratingIndex == tagValue) {
                    
                    [btnRate setImage:[UIImage imageNamed:@"star_half"] forState:UIControlStateNormal];
                    ratingValue = ratingValue + .5;
                } else {
                    ratingValue = ratingValue + 1.0;
                    [btnRate setImage:[UIImage imageNamed:@"star_red"] forState:UIControlStateNormal];
                }
            }
            else {
                [btnRate setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
            }
        }
        if (ratingValue == 0)
        {
            self.btnRate.titleLabel.textColor = [UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
            self.btnRate.backgroundColor = [UIColor whiteColor];
        }
        else
        {
            self.btnRate.titleLabel.textColor = [UIColor whiteColor];
            self.btnRate.backgroundColor = THEMECOLOR;
        }
        NSLog(@"%@",self.btnRate);
        ratingIndex = tagValue;
    }
}


- (IBAction)backButtonPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark -
#pragma mark NSTimer Methods

- (void)updateTimer:(NSTimer *)timer {
    miliSecond -= 1000 ;
    [self populateLabelwithTime:miliSecond];
}

- (void)startTimer {
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer:) userInfo:nil repeats:YES];
}

-(void)pauseTimer {
    if ([timer isValid]) {
        [timer invalidate];
    }
}

- (void)populateLabelwithTime:(int)milliseconds {
    
    int seconds = milliseconds/1000;
    int minutes = seconds / 60;
    int hours = minutes / 60;
    
    if (seconds == 0 && minutes == 0 && hours == 0) {
        [self pauseTimer];
    }
    else {
        seconds -= minutes * 60;
        minutes -= hours * 60;
        
        NSString *remainingTime = [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, seconds];
        NSString *staticStr = [NSString stringWithFormat:@"Thank you for visiting:"];
        
        self.lblRateEnds.text = [NSString stringWithFormat:@"Rate the customer experience before time expires: %@", remainingTime];
    }
}

#pragma mark -
#pragma mark -
#pragma mark  Touches Began

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
        [self dismissViewControllerAnimated:true completion:nil];
    }
}
@end
