//
//  NewsViewController.h
//  RConnect
//
//  Created by Hitesh Dhawan on 15/07/1937 SAKA.
//  Copyright © 1937 SAKA Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsViewController : UIViewController 

@property (nonatomic, strong) NSArray *arrNews;

@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (nonatomic, weak) IBOutlet UITableView *tblNews;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionNews;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;


@property (nonatomic, weak) IBOutlet UIButton *right_button;

@end
