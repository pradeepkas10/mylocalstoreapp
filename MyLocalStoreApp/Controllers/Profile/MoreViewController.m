//
//  MoreViewController.m
//  k kiosk
//
//  Created by HiteshDhawan on 23/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "MoreViewController.h"
#import "MyLocalStoreApp.pch"
#import "UserGroupCell.h"
#import "UserGroupVC.h"

#define MyDateTimePickerHeight 260
#define SIZE [[UIScreen mainScreen] bounds].size

@interface MoreViewController ()<UIWebViewDelegate, MFMailComposeViewControllerDelegate, UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
    NSDictionary * responseDict;
    NSNumber *strIsUserMember;
    NSString *isUserMemberString;

    NSString *strGrouptag;
    NSString * str_edit;
    UIPickerView * genderPicker;
    UIToolbar *toolBar;
    NSString* str_Name;
    NSMutableArray *arrConsents;
    NSMutableArray *arrApprovedConsentId;
    NSMutableArray *arrRevokedConsentId;
    
}

@end


@implementation MoreViewController

#pragma mark -

- (void)viewDidLoad {
    
    [super viewDidLoad];
    arrApprovedConsentId = [[NSMutableArray alloc]init];
    arrRevokedConsentId = [[NSMutableArray alloc]init];
    str_Name = [[NSUserDefaults standardUserDefaults]valueForKey:@"UserName"];
    responseDict = [[NSDictionary alloc]init];
    [self.navigationController setNavigationBarHidden:YES];
    _arrUserGroup = [[NSArray alloc] init];
    [self.btnSaveChanges setTitle:@"EDIT" forState:UIControlStateNormal];
    
    self.viewProfile.layer.cornerRadius = 3.0;
    self.viewProfile.clipsToBounds = YES;
    self.viewProfile.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.viewProfile.layer.shadowOpacity = 1;
    self.viewProfile.layer.shadowOffset = CGSizeZero;
    self.viewProfile.layer.shadowRadius = 1.0;
    self.viewProfile.layer.masksToBounds = NO;

    
    self.openMyPageBtn.layer.borderWidth = 1.0;
    self.openMyPageBtn.layer.borderColor = THEMECOLOR.CGColor;
    self.openMyPageBtn.clipsToBounds = YES;
    self.openMyPageBtn.layer.masksToBounds = NO;

    [self disableTextfields];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.btnSaveChanges setTitle:@"EDIT" forState:UIControlStateNormal];
    [self LoadProfileMethod];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:YES];
    
    for (ASIHTTPRequest *request in ASIHTTPRequest.sharedQueue.operations) {
        if (![request isCancelled]) {
            [request cancel];
            [request setDelegate:nil];
        }
    }
}


#pragma mark - Load Profile Method
-(void)LoadProfileMethod{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    if (status) {
        [appDelegate startIndicator];
        
        NSString *strURL = [NSString stringWithFormat:@"%@user/?UserID=%@", BASEURL, appDelegate.strUserId];
        [self loadUserProfile:strURL];
    }
    else
    {
        NSDictionary *dicUser = [[NSUserDefaults standardUserDefaults]objectForKey:DICProfileData];
        if (dicUser != nil)
        {
            [self updateDatawithDict:dicUser];
        }
        else
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"My Local Stores" message:@"no internet connection." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Ok"
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     [self dismissViewControllerAnimated:true completion:nil];
                                                                 }];
            [alertController addAction:cancelAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}


#pragma mark - SideMenu Toggle

- (IBAction)toggleSideMenuView:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
    [self dismissViewControllerAnimated:YES completion:nil];
    });
}


#pragma mark - UITextFieldDelegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.btnSaveChanges setTitle:@"SAVE" forState:UIControlStateNormal];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark- gender picker

- (void)addGenderPickerView {

    [toolBar removeFromSuperview];

    genderPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 100, SCREEN_WIDTH, 100)];
    genderPicker.dataSource = self;
    genderPicker.delegate = self;
    genderPicker.backgroundColor = [UIColor whiteColor];
    genderPicker.showsSelectionIndicator = YES;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done1:)];
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - genderPicker.frame.size.height - 50, SCREEN_WIDTH, 50)];
    [toolBar setBarStyle:UIBarStyleBlackTranslucent];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton, nil];
    [toolBar setItems:toolbarItems];

    [self.view addSubview:genderPicker];
    [self.view addSubview:toolBar];

        //    countryPicker.hidden = YES;
        //    toolBar.hidden = YES;
    self.txtGender.inputView = genderPicker;
    self.txtGender.inputAccessoryView = toolBar;
    [self.view bringSubviewToFront:genderPicker];
    [self.view bringSubviewToFront:toolBar];
}

- (void)done1:(id)sender {

    [self.txtGender resignFirstResponder];
    [genderPicker removeFromSuperview];
    [toolBar removeFromSuperview];
    genderPicker.hidden = YES;
    toolBar.hidden = YES;
}

#pragma mark - UIPickerView Delegate Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
        return  2;
}


- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {

    if (row == 0)
        {
            return @"Male";
        }
        else
        {
            return  @"Female";
        }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (row == 0)
        self.txtGender.text = @"Male";
    else
        self.txtGender.text = @"Female";

    [self.txtGender resignFirstResponder];
}


#pragma mark - Button Actions

- (IBAction)genderBtnClick:(id)sender {
    [self removeAllViews];
    genderPicker.hidden = NO;
    [self dismissKeyboard];

    [self addGenderPickerView];
    [genderPicker reloadAllComponents];
}

- (void)removeAllViews {
    [genderPicker removeFromSuperview];
    [toolBar removeFromSuperview];
}


- (IBAction)OpenMyPagePressed:(id)sender
{
    NSString *myPageURL = responseDict[@"UserMyPage"];
    KioskWebViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"webController"];
    viewController.urlString = myPageURL;
    [self presentViewController:viewController animated:YES completion:nil];
}

- (IBAction)LogOutPressed:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"confirm" message:@"Are you sure you want to log out?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [SVProgressHUD showWithStatus:@"Logging Out..." maskType:SVProgressHUDMaskTypeBlack];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"UserAuthenticated"];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"UserId"];
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"PreferencesSet"];

        [[NSUserDefaults standardUserDefaults] synchronize];
        [appDelegate changeRootViewController];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                         }];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    //[self.view.window.rootViewController presentViewController:alertController animated:YES completion:nil];
    [self presentViewController:alertController animated:YES completion:nil];
    CFRunLoopWakeUp(CFRunLoopGetCurrent());

}


-(void)detailBtnClick:(UIButton *)sender  // For going to Group Detail Page
{
    UserGroupVC *detailsController = [mainStoryboard instantiateViewControllerWithIdentifier:@"UserGroupVC"];
    detailsController.dataDict = responseDict;
    detailsController.strGroupTag = [[_arrUserGroup objectAtIndex:sender.tag]objectForKey:@"GroupId"];
    [self presentViewController:detailsController animated:NO completion:nil];
}



- (IBAction)saveChangesButtonPressed:(id)sender {

    if ([self.btnSaveChanges.titleLabel.text isEqualToString:@"EDIT"]) {
        [self EnableTextfields];
        [self.btnSaveChanges setTitle:@"SAVE" forState:UIControlStateNormal];
    }
    else{
        // Update user profile
        if (self.txtMobileNo.text.length == 0 || [self.txtMobileNo.text isEqualToString:@""]) {
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"Please Enter Valid Mobile number."];
        }
        else if (self.txtEmail.text.length > 0)
        {
         if (![HelperClass isValidEmail:self.txtEmail.text] ) {
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"Please Enter a valid Email Address."];
        }
            else
            {
                [self SaveChangesMethod:false];
            }
        }
        else{
                [self SaveChangesMethod:false];
            }
        }
    }


#pragma mark - Save Changes method
-(void)SaveChangesMethod:(BOOL )userGroupSwitch{
    
    [self dismissKeyboard];
    [appDelegate startIndicator];
    
    NSString *strURL = [NSString stringWithFormat:@"%@user", BASEURL];
    NSString *_dateString = [HelperClass getDateString];
    
    // Generate signature
    NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId, SALT];
    NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
    
    
    
    NSString *pin = [NSString stringWithFormat:@"%@",_txtpin.text];
    NSString *mail = (_txtEmail.text) ? [NSString stringWithFormat:@"%@",_txtEmail.text] : @"";
    
    NSMutableDictionary *paramDic;
    if (userGroupSwitch == true)
    {
        NSDictionary * usergroupDict;

        usergroupDict = [[NSMutableDictionary alloc] initWithDictionary:@{@"GroupId":strGrouptag,
                                                                          @"IsUserMember":strIsUserMember,
                                                                          @"UserCode":@""}];
        NSArray * sendingArray = [NSArray arrayWithObject:usergroupDict];
        
        if ([pin isEqualToString:@""]) {
            paramDic = [[NSMutableDictionary alloc] initWithDictionary:@{@"UserID":appDelegate.strUserId,
                                                                         @"Name":str_Name,
                                                                         @"DateOfBirth":_txtDateOfBirth.text,
                                                                         @"Emails":mail,
                                                                         @"City":pin,
                                                                         @"UserGroups":sendingArray
                                                                         }];
        }
        else{
            paramDic = [[NSMutableDictionary alloc] initWithDictionary:@{@"UserID":appDelegate.strUserId,
                                                                         @"Name":str_Name,
                                                                         @"DateOfBirth":_txtDateOfBirth.text,
                                                                         @"Emails":mail,
                                                                         @"PostCode":pin,
                                                                         @"UserGroups":sendingArray
                                                                         }];
        }
    }
    else{
        if ([pin isEqualToString:@""]) {
            paramDic = [[NSMutableDictionary alloc] initWithDictionary:@{@"UserID":appDelegate.strUserId,
                                                                         @"Name":str_Name,
                                                                         @"DateOfBirth":_txtDateOfBirth.text,
                                                                         @"Emails":mail,
                                                                         @"City":pin,
                                                                         }];
        }
        else{
            paramDic = [[NSMutableDictionary alloc] initWithDictionary:@{@"UserID":appDelegate.strUserId,
                                                                         @"Name":str_Name,
                                                                         @"DateOfBirth":_txtDateOfBirth.text,
                                                                         @"Emails":mail,
                                                                         @"PostCode":pin,
                                                                         }];
        }
        [paramDic setObject:arrApprovedConsentId forKey:@"ApprovedConsents"];
        [paramDic setObject:arrRevokedConsentId forKey:@"RevokedConsents"];
    }

    NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
    
    ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:strURL]];
    [request setRequestMethod:@"PUT"];
    request.name = @"SaveChanges";
    [request setTimeOutSeconds:30.0];
    [request setDelegate:self];

    [request addRequestHeader:@"Accept" value:@"application/json"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
    [request addRequestHeader:@"X-Liquid-Signature" value:signature];

    [request setPostBody:[NSMutableData dataWithData:data]];
    
    [request startAsynchronous];

}

#pragma mark -
#pragma mark ASIHTTPRequest Methods

- (void)loadUserProfile:(NSString *)urlString {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    [appDelegate startIndicator];
    if (status) {
        
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request startAsynchronous];
    }
    else {
        
        [appDelegate stopIndicator];
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"No Internet Connection." andButtonTitle:@"Dismiss"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{

    if ([request.name isEqualToString:@"SaveChanges"]) {
//        NSString *strURL = [NSString stringWithFormat:@"%@user/?UserID=%@", BASEURL, appDelegate.strUserId];
//        [self loadUserProfile:strURL];
        NSDictionary *dicUser = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
        [[NSUserDefaults standardUserDefaults]setObject:dicUser forKey:DICProfileData];
        NSArray *dicConsents = [dicUser objectForKey:@"Consents"];
        [[NSUserDefaults standardUserDefaults]setObject:dicConsents forKey:DICConsents];

        [self updateDatawithDict:dicUser];

    }
    else{
        NSDictionary *dicUser = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
        [[NSUserDefaults standardUserDefaults]setObject:dicUser forKey:DICProfileData];
        NSArray *dicConsents = [dicUser objectForKey:@"Consents"];
        [[NSUserDefaults standardUserDefaults]setObject:dicConsents forKey:DICConsents];

        [self updateDatawithDict:dicUser];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
//    [SVProgressHUD dismiss];
    [appDelegate stopIndicator];
    
    [self disableTextfields];
    [self.btnSaveChanges setTitle:@"EDIT" forState:UIControlStateNormal];
    NSDictionary *dicUser = [[NSUserDefaults standardUserDefaults]objectForKey:DICProfileData];
    [self updateDatawithDict:dicUser];

    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:request.error.localizedDescription andButtonTitle:@"Dismiss"];

}

-(void)updateDatawithDict:(NSDictionary *)dicUser
{
    [appDelegate stopIndicator];

    responseDict = [NSDictionary dictionaryWithDictionary:dicUser];
    [self disableTextfields];
    [self.btnSaveChanges setTitle:@"EDIT" forState:UIControlStateNormal];
        //self.btnSaveChanges.backgroundColor = REDTHEMECOLOR;

        // Assign values to textfields with validation

    _arrUserGroup = [dicUser objectForKey:@"UserGroups"];
    NSMutableArray *consentArrayFetch = [[dicUser objectForKey:DICConsents] mutableCopy];
    arrConsents = [NSMutableArray array];
    
    for (NSDictionary *dict in consentArrayFetch) {
        if(![dict[@"Name"] isEqualToString: @"Master"]){
            [arrConsents addObject:dict];
        }
    }

    [_tableMore reloadData];
    if ([[dicUser allKeys] containsObject:@"Msn"]) {
        self.txtMobileNo.text = [dicUser objectForKey:@"Msn"];
        [[NSUserDefaults standardUserDefaults] setObject:[dicUser objectForKey:@"Msn"] forKey:@"MSN"];
    }
    if ([[dicUser allKeys] containsObject:@"DateOfBirth"]) {
        NSString *date = [dicUser objectForKey:@"DateOfBirth"];
        self.txtDateOfBirth.text = date;
        self.txtDateOfBirth.hidden = NO;
        [[NSUserDefaults standardUserDefaults] setObject:[dicUser objectForKey:@"DateOfBirth"] forKey:@"DateOfBirth"];
    }

    if ([[dicUser allKeys] containsObject:@"Name"]) {
        self.txtGender.text = @"Male";
    }

    if ([[dicUser allKeys] containsObject:@"City"]) {
    }

    if ([[dicUser allKeys] containsObject:@"PostCode"]) {
        self.txtpin.text = [dicUser objectForKey:@"PostCode"];
    }

    if ([[dicUser allKeys] containsObject:@"Emails"]) {
        NSArray * arr_Email = [dicUser objectForKey:@"Emails"];
        if (arr_Email.count >0) {
            self.txtEmail.text = arr_Email[0];
        }

    }

}





#pragma mark - Tableview
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 1 ) {
        return 60;
    }

     //play around with this value
    return UITableViewAutomaticDimension;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == 0) {
        return 40;
    }
    else
    {
        return UITableViewAutomaticDimension;
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
  UIView *viewH = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];

    if (section == 1)
    {
            //[[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextAlignment:NSTextAlignmentLeft];
        UILabel *lblText = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, [UIScreen mainScreen].bounds.size.width, 50)];
        lblText.font = [UIFont fontWithName:LatoBold size:18.0];

        if (section == 1)
        {
            viewH.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50);
            lblText.frame = CGRectMake(15, 10, [UIScreen mainScreen].bounds.size.width, 35);
            viewH.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:242.0/255.0 blue:236.0/255.0 alpha:1.0];
            lblText.text = @"PREFERENCES (Consents)";
        }

        lblText.font = [UIFont fontWithName:LatoBold size:18];
        lblText.backgroundColor = UIColor.clearColor;

        [viewH addSubview:lblText];
    }
    return viewH;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return _arrUserGroup.count;
    }
    else
    {
        return arrConsents.count;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellIdentifier = @"UserGroupCell";
    NSString *cellIdentifier1 = @"PrefrencesCell";
   
    UserGroupCell *cell;
    switch (indexPath.section) {
        case 0:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            cell.titleLbl.text = [[_arrUserGroup objectAtIndex:indexPath.row] objectForKey:@"GroupDescription"];
            NSString * temp = [[_arrUserGroup objectAtIndex:indexPath.row] objectForKey:@"UserConfigurable"];
            cell.detailBtn.tag = indexPath.row;
            cell.cellSwitch.tag = indexPath.row;
            NSString *checkForSwitch =  [NSString stringWithFormat:@"%@",[[_arrUserGroup objectAtIndex:indexPath.row] objectForKey:@"IsUserMember"]];
            if ([temp isEqualToString:@"CodeConfigurable"] && [checkForSwitch isEqualToString:@"0"] )
            {
                cell.descriptionLbl.text = @"Activate";
                cell.descriptionLbl.hidden = NO;
                cell.cellSwitch.hidden = YES;
                cell.detailBtn.hidden = NO;
                [cell.detailBtn addTarget:self action:@selector(detailBtnClick:) forControlEvents:UIControlEventTouchUpInside];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                
            }
            else if ([temp isEqualToString:@"Configurable"] || [checkForSwitch isEqualToString:@"1"] )
            {
                cell.descriptionLbl.hidden = YES;
                cell.cellSwitch.hidden = NO;
                cell.detailBtn.hidden = YES;
                NSString * switchBool = [NSString stringWithFormat:@"%@",[[_arrUserGroup objectAtIndex:indexPath.row] objectForKey:@"IsUserMember"]];
                if ([switchBool isEqualToString:@"1"])
                {
                    [cell.cellSwitch setOn:YES animated:true];
                }
                else if ([switchBool isEqualToString:@"0"])
                {
                    [cell.cellSwitch setOn:NO animated:true];
                }
                [cell.cellSwitch addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            cell.detailTextLabel.text = [[_arrUserGroup objectAtIndex:indexPath.row] objectForKey:@"GroupDescription"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            //Load data in this prototype cell
            break;
    }

        case 1:
        {
            NSDictionary * dicConsents = [[NSDictionary alloc ]init];
            dicConsents = arrConsents[indexPath.row];

            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
            cell.prefrenceLbl.text = [arrConsents[indexPath.row]objectForKey:@"Name"];
            
            if (![[[arrConsents[indexPath.row]objectForKey:@"CurrentVersion"] objectForKey:@"Title"] isEqualToString:@""] && [[arrConsents[indexPath.row]objectForKey:@"CurrentVersion"] objectForKey:@"Title"] != nil){
                cell.prefrenceLbl.text = [[arrConsents[indexPath.row]objectForKey:@"CurrentVersion"] objectForKey:@"Title"];
            }
            else{
                cell.prefrenceLbl.text = [arrConsents[indexPath.row]objectForKey:@"Name"];
            }
            
            
            NSDictionary * dic = [arrConsents[indexPath.row]objectForKey:@"CurrentVersion"];
            NSString * tag = [NSString stringWithFormat:@"%@",dic[@"Id"]];
            
            NSLog(@"TAG - %@ ---- %@",tag,[arrConsents[indexPath.row]objectForKey:@"Name"]);
            
            
            cell.preferencescellSwitch.tag = [dic[@"Id"] integerValue];

            if ([dicConsents[@"State"] isEqualToString:@"ConsentGiven"]) {
                [cell.preferencescellSwitch setOn:true];
                if (![arrApprovedConsentId containsObject:tag])
                {
                    [arrApprovedConsentId addObject:tag];
                }
            }
            else
            {
                [cell.preferencescellSwitch setOn:false];
                if (![arrRevokedConsentId containsObject:tag])
                {
                    [arrRevokedConsentId addObject:tag];
                }
            }


            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell.preferencescellSwitch addTarget:self action:@selector(switchValueChanged1:) forControlEvents:UIControlEventValueChanged];
            
            tableView.estimatedRowHeight = 40;
            tableView.rowHeight = UITableViewAutomaticDimension;


            break;
        }
        default:
            break;
    }
    return cell;
}


-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 1:
        {
            NSDictionary * dicConsents = [[NSDictionary alloc ]init];
            dicConsents = arrConsents[indexPath.row];
            
            if (![[[arrConsents[indexPath.row]objectForKey:@"CurrentVersion"] objectForKey:@"Description"] isEqualToString:@""] && [[arrConsents[indexPath.row]objectForKey:@"CurrentVersion"] objectForKey:@"Description"] != nil){
                
                NSString *titleStr = [[arrConsents[indexPath.row]objectForKey:@"CurrentVersion"] objectForKey:@"Title"];
                NSString * htmlString = [[arrConsents[indexPath.row]objectForKey:@"CurrentVersion"] objectForKey:@"Description"];
                NSString *str = [HelperClass stringByStrippingHTML:htmlString];
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:titleStr message:str preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:true completion:nil];
            }
            else if (![[[arrConsents[indexPath.row]objectForKey:@"CurrentVersion"] objectForKey:@"Title"] isEqualToString:@""] && [[arrConsents[indexPath.row]objectForKey:@"CurrentVersion"] objectForKey:@"Title"] != nil){
                
                NSString *titleStr = [[arrConsents[indexPath.row]objectForKey:@"CurrentVersion"] objectForKey:@"Title"];
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:titleStr message:@"" preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:true completion:nil];
                
            }
            else{
                NSString *titleStr = [arrConsents[indexPath.row] objectForKey:@"Name"];
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:titleStr message:@"" preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:true completion:nil];
            }
            break;
        }
        default:
            break;
    }
}


#pragma mark - SwitchValues Changed

-(void)switchValueChanged:(UISwitch *)sender
{
  strGrouptag = [[_arrUserGroup objectAtIndex:sender.tag] objectForKey:@"GroupId"];
    if (sender.isOn == 1)
    {
        strIsUserMember = @YES;//[NSNumber numberWithBool:true];
        isUserMemberString = @"true";
    }
    else
    {
        strIsUserMember = @NO;//[NSNumber numberWithBool:false];
        isUserMemberString = @"false";

    }
    [self SaveChangesMethod:strIsUserMember];
}


-(void)switchValueChanged1:(UISwitch *)sender
{
    NSString * tag = [NSString stringWithFormat:@"%ld",(long)sender.tag];
//    [self.btnSaveChanges setTitle:@"SAVE" forState:UIControlStateNormal];
    
    NSLog(@"TAG - %@",tag);

    if (sender.isOn) {
        [arrApprovedConsentId addObject:tag];
        if (![arrApprovedConsentId containsObject:tag])
        {
            [arrApprovedConsentId addObject:tag];
        }
        
        if ([arrRevokedConsentId containsObject:tag]) {
            [arrRevokedConsentId removeObject:tag];
        }
    }
    else
    {
        if ([arrApprovedConsentId containsObject:tag])
        {
            [arrApprovedConsentId removeObject:tag];
        }
        
        if (![arrRevokedConsentId containsObject:tag]) {
            [arrRevokedConsentId addObject:tag];
        }
    }
    [self SaveChangesMethod:false];

}


#pragma mark - TEXTFIELD DISABLE AND ENABLE

-(void)disableTextfields
{
    self.txtMobileNo.userInteractionEnabled = NO;
    self.txtEmail.userInteractionEnabled = NO;
    self.txtGender.userInteractionEnabled = NO;
    self.txtpin.userInteractionEnabled = NO;
}

-(void)EnableTextfields
{
    self.txtEmail.userInteractionEnabled = YES;
    [self.txtEmail becomeFirstResponder];
    self.txtGender.userInteractionEnabled = YES;
}


-(void)setImageOntextField:(UITextField *)textfield andImage:(NSString *)image withSize:(CGSize)size{
    UIImageView *imv = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, size.width, size.height)];
    imv.image = [UIImage imageNamed:image];
    textfield.rightViewMode = UITextFieldViewModeAlways;
    textfield.rightView = imv;
}


#pragma mark - System
-(BOOL)prefersStatusBarHidden{
    return true;
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
        //        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
        //        [mo showHome];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    [self.view endEditing:YES];
}

#pragma mark - Helper Methods

- (void)dismissKeyboard
{
    [self.txtDateOfBirth resignFirstResponder];
    [self.txtMobileNo resignFirstResponder];
    [self.txtGender resignFirstResponder];
}
- (NSString *) dateStringFromString :(NSString *) dateString {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ss aa"];
    NSDate *date = [dateFormat dateFromString:dateString];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"dd/MMMM/yyyy"];
    return [dateFormat stringFromDate:date];
}

@end
