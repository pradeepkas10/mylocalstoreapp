//
//  ContactUsVC.m
//  TFS
//
//  Created by Surbhi on 26/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

#import "ContactUsVC.h"
#import "MyLocalStoreApp.pch"

//#define phoneNumber @"+919910615909"
#define phoneNumber @"+917827247247" // TFS
//#define emailID @"abc@gmail.com"
#define emailID @"customerservice@mylocalstore.com" //Bigbite

@interface ContactUsVC ()<UIWebViewDelegate, MFMailComposeViewControllerDelegate>

@end

@implementation ContactUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.contactButton setAttributedTitle:[[NSAttributedString alloc]initWithString:phoneNumber attributes:@{NSForegroundColorAttributeName : THEMECOLOR, NSFontAttributeName : [UIFont fontWithName:LatoBold size:16.0],  NSUnderlineColorAttributeName : THEMECOLOR, NSUnderlineStyleAttributeName :@(NSUnderlineStyleSingle)}] forState:UIControlStateNormal];
    
    [self.emailButton setTitle:emailID forState:UIControlStateNormal];

    self.viewContact.layer.cornerRadius = 3.0;
    self.viewContact.clipsToBounds = YES;
    self.viewContact.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.viewContact.layer.shadowOpacity = 1;
    self.viewContact.layer.shadowOffset = CGSizeZero;
    self.viewContact.layer.shadowRadius = 1.0;
    self.viewContact.layer.masksToBounds = NO;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)toggleSideMenuView:(id)sender
{
    //    [self.navigationController toggleSideMenuView];
    dispatch_async(dispatch_get_main_queue(), ^{
    [self dismissViewControllerAnimated:YES completion:nil];
    });
}

- (IBAction)NUMBERPressed:(id)sender{
    
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phoneNumber]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Alert" andMessage:@"Call facility is not available."andButtonTitle:@"Ok"];
    }
    
    
    
}

- (IBAction)EMAILPressed:(id)sender;{

    NSString *app_version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *device_version = [UIDevice currentDevice].systemVersion;
    NSString *device_name = [UIDevice currentDevice].model;

    NSString *emailTitle = [NSString stringWithFormat:@"My Local Store (%@), %@ - %@",app_version,device_name,device_version];
    
    NSString *phone = [[NSUserDefaults standardUserDefaults] valueForKey:@"MSN"];
    NSString *dob = [[NSUserDefaults standardUserDefaults] valueForKey:@"DateOfBirth"];
    NSString *messageBody;

    if (dob != nil || ![dob isEqualToString:@""]) {
        messageBody = [NSString stringWithFormat:@"\n\n\nUser ID : %@\nDEVICE ID : %@\nMobile Number : %@\nBirthdate : %@",appDelegate.strUserId,appDelegate.strDeviceToken,phone,dob];
    }
    else{
        messageBody = [NSString stringWithFormat:@"\n\n\nUser ID : %@\nDEVICE ID : %@\nMobile Number : %@",appDelegate.strUserId,appDelegate.strDeviceToken,phone];
    }
    
    NSArray *toRecipents = [NSArray arrayWithObject:[NSString stringWithFormat:@"%@",emailID]];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail])
    {
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    }
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - SOCIAL LINKS

- (IBAction)FBPressed:(id)sender;{
    
    NSURL *url = [NSURL URLWithString:@"https://www.facebook.com/liquidbarcodes/"];
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
        
    }
    else
    {
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Alert" andMessage:@"Oops! Some error occurred. Please try again later."andButtonTitle:@"Ok"];
    }
}



// this action is affiliated to Snapchat Button in mainstoryboard. The width constant for which is set to 0 and the button is hidden, this will be added later
- (IBAction)SnapChatPressed:(id)sender; //Replacing Snapchatwith Twitter as asked by Client
{
    NSURL *url = [NSURL URLWithString:@"https://twitter.com/liquidbarcodes"];
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }
    else
    {
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Alert" andMessage:@"Oops! Some error occurred. Please try again later."andButtonTitle:@"Ok"];
    }
}



- (IBAction)InstaGramPressed:(id)sender; // REMOVE INSTAGRAM AND ADD LINKEDIN - by Client
{
    NSURL *url = [NSURL URLWithString:@"https://www.linkedin.com/company/725128/"];
    //    [[UIApplication sharedApplication] openURL:url];
    
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }
    else
    {
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Alert" andMessage:@"Oops! Some error occurred. Please try again later."andButtonTitle:@"Ok"];
    }
}


- (IBAction)YouTubePressed:(id)sender;{
    NSURL *url = [NSURL URLWithString:@"https://www.youtube.com/channel/UCTlSvDdTDqmBfQN7m0CC55g"];
    //    [[UIApplication sharedApplication] openURL:url];
    
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }
    else
    {
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Alert" andMessage:@"Oops! Some error occurred. Please try again later."andButtonTitle:@"Ok"];
    }
}

#pragma mark - Web Link

- (IBAction)webBtnPressed:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.liquidbarcodes.com/"] options:@{} completionHandler:nil];
    //"@"https://mylocalstorea.com"
}



-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
        //        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
        //        [mo showHome];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}



-(BOOL)prefersStatusBarHidden{
    return YES;
}
@end
