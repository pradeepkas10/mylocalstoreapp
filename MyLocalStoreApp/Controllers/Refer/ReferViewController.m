//
//  ReferViewController.m
//  K kiosk
//
//  Created by Hitesh Dhawan on 10/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "ReferViewController.h"
#import "ContactViewController.h"
#import <AddressBook/AddressBook.h>
#import <Contacts/Contacts.h>

@interface ReferViewController ()<MFMessageComposeViewControllerDelegate,UITextFieldDelegate>
{
    UIPickerView *countryPicker;
    UIToolbar *toolBar;
    NSString *smsTemplateStrng;
    __weak IBOutlet UIImageView *logoImage;
    NSString *shouldAnimate;
    BOOL iscontactView;
}
@end


@implementation ReferViewController

-(BOOL)prefersStatusBarHidden{
    return true;
}
- (void)viewDidLoad {
    [super viewDidLoad];
     self.mobileLabel.hidden = YES;
    [UIApplication sharedApplication].statusBarHidden = NO;

    // Fetch list of phone code
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"CountryPhoneCode" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    self.arrCountryCode = [[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil] objectForKey:@"countries"];
    
    // Dismiss keyboard gesture
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    
    UITapGestureRecognizer *imageTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchClick)];
    [self.logoImage addGestureRecognizer:imageTapGesture];

    
    // Ask contacts permission
    [self askContactsPermission];
    
    self.contactPicker = [[CNContactPickerViewController alloc] init];
    self.contactPicker.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (_isShareView) {
        self.shareView.hidden = NO;
        self.referView.hidden = YES;
        
        self.view.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0];
        [self.view bringSubviewToFront:self.shareView];
    }
    else{

        self.referView.hidden = NO;
        self.shareView.hidden = YES;
        self.view.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0];
        [self.view bringSubviewToFront:self.referView];
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    if (_isShareView == YES && iscontactView == NO) {
        [self performSelector:@selector(PostNotificationForUpdation) withObject:nil afterDelay:2.0];
    }
}

-(void)PostNotificationForUpdation{
    [appDelegate stopIndicatorSHARE];
    NSDictionary* userInfo = @{@"shouldAnimate": shouldAnimate};
    [[NSNotificationCenter defaultCenter]postNotificationName:@"SharingCompleted" object:userInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark Custom Methods

- (void)dismissKeyboard
{
    [self.txtMobileNumber resignFirstResponder];
    [self.txtMobile_Share resignFirstResponder];
}

- (BOOL)validateMobileNumber:(NSString *)mobileNumber
{
    NSString *mobileNumberPattern = @"[789][0-9]{9}";
    NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
    BOOL matched = [mobileNumberPred evaluateWithObject:mobileNumber];
    return matched;
}


- (void)askContactsPermission {
    
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
           // ABAddressBookRef addressBook = ABAddressBookCreate();
//            NSLog(@"Created address book ref: %@", addressBook);
        });
    }
}

#pragma mark
#pragma mark Country Picker
-(void)newMethod:(UITextField *)textFld
{
    [self addCountryPickerView];
    [countryPicker reloadAllComponents];
    [countryPicker selectRow:[self getIndexOfValue:textFld.text presentInArray:self.arrCountryCode forKey:@"code"] inComponent:0 animated:NO];
}



- (IBAction)countaryPickerClick:(id)sender {
    countryPicker.hidden = NO;
    [_txtMobileNumber resignFirstResponder];
    
    if (_txtPhoneCode.text.length == 0) _txtPhoneCode.text = [[self.arrCountryCode firstObject] objectForKey:@"code"];;
    [self performSelector:@selector(newMethod:) withObject:_txtPhoneCode afterDelay:0.4];
}

- (NSInteger) getIndexOfValue:(id) value presentInArray:(NSArray *)array forKey:(NSString *) key {
    
    NSInteger i = 0;
    
    for (i = 0; i < array.count; i++) {
        
        if ([array[i][key] isEqual:value]) {
            break;
        }
    }
    return i;
}
- (void)addCountryPickerView {
    
    [countryPicker removeFromSuperview];
    [toolBar removeFromSuperview];
    toolBar = nil;
    countryPicker = nil;
    
    countryPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 200, SCREEN_WIDTH, 200)];
    countryPicker.dataSource = self;
    countryPicker.delegate = self;
    countryPicker.backgroundColor = [UIColor whiteColor];
    countryPicker.showsSelectionIndicator = YES;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done:)];
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - countryPicker.frame.size.height - 50, SCREEN_WIDTH, 50)];
    [toolBar setBarStyle:UIBarStyleBlackTranslucent];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton, nil];
    [toolBar setItems:toolbarItems];
    
    [self.view addSubview:countryPicker];
    [self.view addSubview:toolBar];
    
    //    countryPicker.hidden = YES;
    //    toolBar.hidden = YES;
    
    self.txtPhoneCode.inputView = countryPicker;
    self.txtPhoneCode.inputAccessoryView = toolBar;
    
    [self.view bringSubviewToFront:countryPicker];
    [self.view bringSubviewToFront:toolBar];
}

- (void)done:(id)sender {
    
    [self.txtPhoneCode resignFirstResponder];
    [countryPicker removeFromSuperview];
    [toolBar removeFromSuperview];
    countryPicker.hidden = YES;
    toolBar.hidden = YES;
    
}



#pragma mark
#pragma mark - ACTIONS


- (IBAction)cancelButtonPressed:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
    [self dismissViewControllerAnimated:YES completion:nil];
    });
}

- (IBAction)sendButtonPressed:(id)sender {
    
    [self.txtMobileNumber resignFirstResponder];
    
    if (self.txtMobileNumber.text.length > 0) {
        NSString *strPhoneCode = self.txtPhoneCode.text;
        if ([strPhoneCode hasPrefix:@"+"]) {
            strPhoneCode = [strPhoneCode substringFromIndex:1];
        }
        NSString *strMobileNumber = [NSString stringWithFormat:@"%@%@", strPhoneCode, self.txtMobileNumber.text];
        strMobileNumber = [strMobileNumber stringByReplacingOccurrencesOfString:@"\u00a0" withString:@""];
        
        [SVProgressHUD showWithStatus:@"Validating..." maskType:SVProgressHUDMaskTypeGradient];
        NSString *strURL = [NSString stringWithFormat:@"%@validreferral/?UserId=%@&MSN=%@", BASEURL, appDelegate.strUserId, strMobileNumber];
        [self referFriend:strURL];
    }
    else{
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error." andMessage:@"Please enter your mobile number."];
    }

}

- (IBAction)contactsButtonPressed:(id)sender {
    
    iscontactView = YES;
    [self presentViewController:self.contactPicker animated:YES completion:nil];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [countryPicker removeFromSuperview];
    [toolBar removeFromSuperview];
    countryPicker.hidden = YES;
    toolBar.hidden = YES;
    countryPicker = nil;
    toolBar = nil;


    if (textField == _txtMobileNumber)
    {
        [UIView animateWithDuration:1.0 animations:^{
            self.mobileLabel.hidden = NO;
            self.txtMobileNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@" " attributes:nil];
            [HelperClass activatetextField:self.mobileLine height:self.mobileLineHeight];
            
        } completion:^(BOOL finished) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.mobileLabel.hidden = NO;
                [self.view bringSubviewToFront:self.mobileLabel];
                self.txtMobileNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@" " attributes:nil];
                [HelperClass activatetextField:self.mobileLine height:self.mobileLineHeight];
            });
        }];
        
    }
    if (textField == _txtMobile_Share)
    {
        [UIView animateWithDuration:1.0 animations:^{
            self.mobileLabel.hidden = NO;
            self.txtMobile_Share.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@" " attributes:nil];
            [HelperClass activatetextField:self.mobileLineshare height:self.mobileLineHeightshare];
            
        } completion:^(BOOL finished) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.mobileLabel.hidden = NO;
                [self.view bringSubviewToFront:self.mobileLabelshare];
                self.txtMobile_Share.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:nil];
                [HelperClass activatetextField:self.mobileLineshare height:self.mobileLineHeightshare];
            });
        }];
        
    }

    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField == _txtMobileNumber)
    {
        [UIView animateWithDuration:1.0 animations:^{
            self.mobileLabel.hidden = NO;
            self.txtMobileNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:nil];
            [HelperClass activatetextField:self.mobileLine height:self.mobileLineHeight];
            
        } completion:^(BOOL finished) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.mobileLabel.hidden = NO;
                [self.view bringSubviewToFront:self.mobileLabel];
                self.txtMobileNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:nil];
                [HelperClass DeactivatetextField:self.mobileLine height:self.mobileLineHeight];
                
                if ([self.txtMobileNumber.text isEqualToString: @"" ] || self.txtMobileNumber.text == nil) {
                    self.mobileLabel.hidden = YES;
                }
                else {
                    self.mobileLabel.hidden = NO;
                }
                
            });
        }];
    }
    else if (textField == _txtMobile_Share)
    {
        [UIView animateWithDuration:1.0 animations:^{
            self.mobileLabel.hidden = NO;
            self.txtMobile_Share.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:nil];
            [HelperClass activatetextField:self.mobileLineshare height:self.mobileLineHeightshare];
            
        } completion:^(BOOL finished) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.mobileLabel.hidden = NO;
                [self.view bringSubviewToFront:self.mobileLabelshare];
                self.txtMobile_Share.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:nil];
                [HelperClass DeactivatetextField:self.mobileLineshare height:self.mobileLineHeightshare];
                
                if ([self.txtMobile_Share.text isEqualToString: @"" ] || self.txtMobile_Share.text == nil) {
                    self.mobileLabelshare.hidden = YES;
                }
                else {
                    self.mobileLabelshare.hidden = NO;
                }
                
            });
        }];
    }
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField isEqual:_txtMobileNumber] || [textField isEqual:_txtMobile_Share]) {
        if (range.location > 9) {
            return NO;
        }
    }
    return YES;
}



#pragma mark -
#pragma mark UIPickerViewDataSource Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.arrCountryCode count];
}

#pragma mark -
#pragma mark ASIHTTPRequest Methods

- (void)referFriend:(NSString *)urlString {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];

    if (status) {

        NSString *strPhoneCode = self.txtPhoneCode.text;
        if ([strPhoneCode hasPrefix:@"+"]) {
            strPhoneCode = [strPhoneCode substringFromIndex:1];
        }
        NSString *strMobileNumber = [NSString stringWithFormat:@"%@%@", strPhoneCode, self.txtMobileNumber.text];
        strMobileNumber = [strMobileNumber stringByReplacingOccurrencesOfString:@"\u00a0" withString:@""];

        NSString *_dateString = [HelperClass getDateString];

        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@", _dateString, appDelegate.strUserId, strMobileNumber, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];

        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        request.name = @"REFERINGFRIEND";

        [request setRequestMethod:@"GET"];
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];

        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        [request startAsynchronous];
    }
    else{
        [SVProgressHUD dismiss];
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"No internet connection." andButtonTitle:@"Cancel"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [SVProgressHUD dismiss];
    
    NSDictionary *dicUser = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    
    if ([request.name isEqualToString:@"REFERINGFRIEND"]) {
        
        if ([[dicUser allKeys] containsObject:@"SmsTemplate"]) {
            
            smsTemplateStrng = [dicUser valueForKey:@"SmsTemplate"];
            [self SendRefferalMethod];
            
        }
         else {

            NSString *messageString = @"Already registered user.";
            if ([[dicUser allKeys] containsObject:@"ResponseStatus"] && [[[dicUser valueForKey:@"ResponseStatus"] allKeys] containsObject:@"Message"]) {
                messageString = [[dicUser valueForKey:@"ResponseStatus"] valueForKey:@"Message"];
            }
             [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:messageString andButtonTitle:@"Cancel"];
        }
    }
    else if ([request.name isEqualToString:@"REFERINGCHECK"]){
        if (dicUser.count == 0)
        {
            [SVProgressHUD dismiss];
            NSString *strPhoneCode = self.txtPhoneCode.text;
            if ([strPhoneCode hasPrefix:@"+"]) {
                strPhoneCode = [strPhoneCode substringFromIndex:1];
            }
            NSString *strMobileNumber =self.txtMobileNumber.text;
            
            // Send message
            [self prepareMessage:smsTemplateStrng andMobileNumber:strMobileNumber];
        }
    }
    else {
        
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"Already registered user." andButtonTitle:@"Cancel"];
    }
    
}


-(void)SendRefferalMethod{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeBlack];
        
        NSString *strPhoneCode = self.txtPhoneCode.text;
        if ([strPhoneCode hasPrefix:@"+"]) {
            strPhoneCode = [strPhoneCode substringFromIndex:1];
        }
        NSString *strMobileNumber = [NSString stringWithFormat:@"%@%@", strPhoneCode, self.txtMobileNumber.text];
        strMobileNumber = [strMobileNumber stringByReplacingOccurrencesOfString:@"\u00a0" withString:@""];
        
        NSString *urlString = [NSString stringWithFormat:@"%@referral", BASEURL];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@", _dateString, appDelegate.strUserId, strMobileNumber, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        request.name = @"REFERINGCHECK";

        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        NSDictionary *paramDic = @{@"UserID":appDelegate.strUserId,
                                   @"MSN":strMobileNumber};
        NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
        [request setPostBody:[NSMutableData dataWithData:data]];
        [request startAsynchronous];
    }
    else
    {
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:@"No internet connection." andButtonTitle:@"Cancel"];
    }

}


- (void)requestFailed:(ASIHTTPRequest *)request
{
    [SVProgressHUD dismiss];
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:request.error.localizedDescription andButtonTitle:@"Cancel"];
}

#pragma mark -
#pragma mark UIPickerViewDelegate Methods

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    
    return [NSString stringWithFormat:@"%@      %@", [[self.arrCountryCode objectAtIndex:row] objectForKey:@"code"], [[self.arrCountryCode objectAtIndex:row] objectForKey:@"name"]];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (_isShareView) {
        self.txtphoneCode_Share.text = [[self.arrCountryCode objectAtIndex:row] objectForKey:@"code"];
    }
    else{
        self.txtPhoneCode.text = [[self.arrCountryCode objectAtIndex:row] objectForKey:@"code"];
    }
}


#pragma mark -
#pragma mark ContactViewControllerDelegate Methods

- (void)selectedPersonContact:(NSString *)phoneNumber
{
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    NSString *firstChar = [phoneNumber substringToIndex:1];
    if ([firstChar isEqualToString:@"+"]) {
        
        phoneNumber = [phoneNumber substringFromIndex:3];
    }
    
    if (_isShareView) {
        self.txtMobile_Share.text = phoneNumber;
    }
    else{
        self.txtMobileNumber.text = phoneNumber;
    }
}

- (void)prepareMessage:(NSString *)msg andMobileNumber:(NSString *)number {
    
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    picker.messageComposeDelegate = self;
    NSArray* arrayOfStrings = [number componentsSeparatedByString:@""];
    
    picker.recipients = arrayOfStrings;
    picker.body = msg;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
            
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred." andMessage:@"Failed to send SMS." andButtonTitle:@"OK"];
            break;
        }
            
        case MessageComposeResultSent:
        {
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"" andMessage:@"SMS sent." andButtonTitle:@"OK"];
            break;
        }
            
        default:
            break;
    }
    
    [controller removeFromParentViewController];
    [controller dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark CNContactPickerDelegate Methods

- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty {

    NSString *phoneNumber = [NSString stringWithFormat:@"%@", [[contactProperty value] stringValue]];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    //NSString * countrycode =  //[number.value valueForKey:@"countryCode"]

    //self.arrCountryCode forKey:@"code"
    NSString *firstChar = [phoneNumber substringToIndex:1];
    NSString *code = [[NSString alloc]init];
    for (int i=0; i< self.arrCountryCode.count; i++) {

        if ([[contactProperty.value valueForKey:@"countryCode"]uppercaseString] ==  [self.arrCountryCode[i] valueForKey:@"Short_name"]) {
            code = [self.arrCountryCode[i] valueForKey:@"code"];

            if (_isShareView) {
                _txtphoneCode_Share.text = code;
            }
            else{
                _txtPhoneCode.text = code;
            }
            
            if ([firstChar isEqualToString:@"+"])
            {
                phoneNumber = [phoneNumber substringFromIndex:code.length];
            }
            break;
        }
    }

    
    if (_isShareView) {
        self.txtMobile_Share.text = phoneNumber;
    }
    else{
        self.txtMobileNumber.text = phoneNumber;
    }
    
    iscontactView = NO;
}

- (void)contactPickerDidCancel:(CNContactPickerViewController *)picker {
    
    iscontactView = YES;
}


-(void)setImageOntextField:(UITextField *)textfield andImage:(NSString *)image withSize:(CGSize)size{
    UIImageView *imv = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, size.width, size.height)];
    imv.image = [UIImage imageNamed:image];
    textfield.rightViewMode = UITextFieldViewModeAlways;
    textfield.rightView = imv;
}



#pragma mark
#pragma mark
#pragma mark - SHARE View



- (IBAction)ShareButtonButtonPressed:(id)sender {
    if (self.txtMobile_Share.text.length > 0) {
        [self hideAllELEMENTS];
        [appDelegate createCustomLoaderForAPIwithImage:@"gift.png" withAnimation:YES];//shareCoupon
        [self performSelector:@selector(ShareAPICalled) withObject:nil afterDelay:1.0];
    }
    else{
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred." andMessage:@"Please enter your mobile number."];
    }
}

-(void)ShareAPICalled{

    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status ) {

        NSString *strPhoneCode = self.txtphoneCode_Share.text;
        if ([strPhoneCode hasPrefix:@"+"]) {
            strPhoneCode = [strPhoneCode substringFromIndex:1];
        }
        NSString *strMobileNumber = [NSString stringWithFormat:@"%@%@", strPhoneCode, self.txtMobile_Share.text];
        strMobileNumber = [strMobileNumber stringByReplacingOccurrencesOfString:@"\u00a0" withString:@""];

        NSString *urlString = [NSString stringWithFormat:@"%@share", BASEURL];

        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@", _dateString, appDelegate.strUserId, self.couponID,SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        NSDictionary *paramDic = @{@"UserId":appDelegate.strUserId,
                                   @"ScheduleId":self.campeignID,
                                   @"ContentId":self.couponID,
                                   @"FriendUserRef":strMobileNumber
                                   };
        NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
        [request setPostBody:[NSMutableData dataWithData:data]];
        
        [request setDidFinishSelector:@selector(SHARErequestFinished:)];
        [request setDidFailSelector:@selector(SHARErequestFailed:)];
        
        [request startAsynchronous];
    }
    else {
        [appDelegate stopIndicatorSHARE];
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:@"No internet connection." andButtonTitle:@"Cancel"];
    }
    
}



- (void)sharingView:(NSString *)text
{
    NSMutableArray *sharingItems = [[NSMutableArray alloc]initWithObjects:text, nil];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:^{
        
        [self CancelSharePressed:nil];
    }];
}




- (void)SHARErequestFinished:(ASIHTTPRequest *)request {
    
//    [SVProgressHUD dismiss];
    [appDelegate stopIndicatorSHARE];

    NSDictionary *responseDict = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    NSString *responseCode = responseDict[@"ResultCode"];
    NSString *responseMessage = responseDict[@"ResultMessage"];
    
    
    if (request.responseStatusCode == 200){
        if ([responseCode isEqualToString: @"SharedByUrl"] || [responseCode isEqualToString:@"SharedOnSms"])
        {
            NSString *mssg = responseDict[@"Message"];
            // create UIACTIVITY CONTROLLER FOR SHARING THE LINK
            [self sharingView: mssg];
        }
        else if ([responseCode isEqualToString:@"SharedByApp"]){
            shouldAnimate = @"1";
            
            [UIView animateWithDuration:2.5 animations:^{
                [appDelegate createCustomLoaderForAPIwithImage:@"success" withAnimation:NO];
                
            } completion:^(BOOL finished) {
                [self.view removeFromSuperview];
                [self removeFromParentViewController];
            }];
        }
        else{
            NSString *titleString;
            NSString *messageString;
            
            if ([responseCode isEqualToString:@"CouldNotShare"]){
                titleString = @"Campaign was not shared.";
                messageString = @"You can only share with friends who have the app. Use the friend-friend feature.";
            }
            else{
                titleString = responseCode;
                messageString = responseMessage;
            }
            
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:titleString message:messageString preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 shouldAnimate = @"0";
                                                                 
                                                                 
                                                                 [self.view removeFromSuperview];
                                                                 [self removeFromParentViewController];
                                                             }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:Nil];
        }
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@",request.responseStatusMessage    ] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             shouldAnimate = @"0";
                                                             
                                                             
                                                             [self.view removeFromSuperview];
                                                             [self removeFromParentViewController];
                                                         }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:Nil];

    }
    
}

- (void)SHARErequestFailed:(ASIHTTPRequest *)request {
    
    [SVProgressHUD dismiss];
    [appDelegate stopIndicatorSHARE];
    [self ShowAllElements];
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:request.error.localizedDescription andButtonTitle:@"Cancel"];
}



- (IBAction)CancelSharePressed:(id)sender {

   
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.view.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         if (finished) {
                            shouldAnimate = @"0";
                              [self.view removeFromSuperview];
                             [self removeFromParentViewController];
                         }
                     }];
    
}



-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)touchClick {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}


-(void)hideAllELEMENTS{
    self.shareView.hidden = YES;
    self.referView.hidden = YES;
    self.view.backgroundColor = [UIColor clearColor];
    [self.view bringSubviewToFront:self.shareView];

}

-(void)ShowAllElements{
    if (_isShareView) {
        self.shareView.hidden = NO;
        self.referView.hidden = YES;
        self.view.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0];
        [self.view bringSubviewToFront:self.shareView];

    }
    else{
        self.referView.hidden = NO;
        self.shareView.hidden = YES;
        self.view.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0];
        [self.view bringSubviewToFront:self.referView];
    }
    
    
    

}
- (IBAction)backButtonPressed:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
    [self dismissViewControllerAnimated:YES completion:nil];
    });
}
@end
