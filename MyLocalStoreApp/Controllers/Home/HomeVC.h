//
//  HomeVC.h
//  Mother Dairy
//
//  Created by Surbhi on 24/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyLocalStoreApp.pch"

@interface HomeVC : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource, UIViewControllerTransitioningDelegate, UINavigationBarDelegate>

//@property (assign, nonatomic) id<CouponsViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UICollectionView *gridCoupon;
@property (weak, nonatomic) IBOutlet UICollectionView *gridLoyalty;
@property (weak, nonatomic) IBOutlet UIPageControl *pageController;
@property (weak, nonatomic) IBOutlet UIPageControl *pageController_Loyalty;

@property (weak, nonatomic) IBOutlet UIView *imgHeader;
@property (weak, nonatomic) IBOutlet UIImageView *rightArrowImage;


- (IBAction)RfreshButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeight;
@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;
@property (weak, nonatomic) IBOutlet UIButton *hamMenuBtn;


@property (weak, nonatomic) IBOutlet UIImageView *offerImageView;
@property (weak, nonatomic) IBOutlet UILabel *OfferDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *goToOfferButton;
- (IBAction)GoToOfferButtonPressed:(id)sender;


// for animation...
@property (nonatomic, strong) UIView * fromView;
@property (nonatomic, assign) CGRect initialRect;

@end
