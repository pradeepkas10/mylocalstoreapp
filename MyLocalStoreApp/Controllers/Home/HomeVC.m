//
//  HomeVC.m
//  Mother Dairy
//
//  Created by Surbhi on 24/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

#import "HomeVC.h"
#import "SSKeychain.h"
#import "Reachability.h"
#import "SVProgressHUD.h"
#import "ASIHTTPRequest.h"
#import "UIImageView+WebCache.h"
#import "PresentingAnimationController.h"
#import "DismissingAnimationController.h"
#import "VerticalSwipeInteractionController.h"
#import "HelperClass.h"
#import "NSString+PMUtils.h"
#import "AppDelegate.h"
#import "SplashViewController.h"
#import "MenuItemsTVC.h"
#import "MyLocalStoreApp.pch"

@interface HomeVC ()<UICollectionViewDelegateFlowLayout>
{
//  SplashViewController *splashController;
    NSMutableArray *arrCoupons;
    NSMutableArray *arrCouponsToDisplay;
    NSMutableArray *arrRewards;
    NSMutableArray *arrLoyalty;
    NSMutableArray *arrOffers;
    NSMutableArray *arrConsents;
    UIRefreshControl *refreshControl;
    UIActivityIndicatorView *refreshActivityView;
    UIBarButtonItem *rightButton;
    UIButton* right_button;
    NSTimer *timer;
    CGFloat lastContentOffset;
    
    //new
    NSTimer * feedbackTimer;
    int miliSecond;
    double lastTimeValue;
    BOOL isLoadedOffer;
}
@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [UIApplication sharedApplication].statusBarHidden = YES;
    //[appDelegate startIndicator];
    if (appDelegate.viewOnceLoaded == NO) {
        appDelegate.viewOnceLoaded = YES;
    }
    if (appDelegate.justLogedIn == YES) {
        // show Alert
        NSString *name = [[NSUserDefaults standardUserDefaults]valueForKey:@"UserName"];
        [appDelegate showWelcomePop:name];
        appDelegate.justLogedIn = NO;

        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self hideWelcomeMessage];
        });
    }
}

-(void)hideWelcomeMessage
{
    [appDelegate hideWelcomePop];
}

-(void)viewWillAppear:(BOOL)animated{
    if (!appDelegate.isLoader && appDelegate.strDeviceToken.length > 0) {
        appDelegate.strUserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserId"];
        appDelegate.strMobileNumber = [[NSUserDefaults standardUserDefaults] valueForKey:@"MSN"];
        [appDelegate registerDevice];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    
    [feedbackTimer invalidate];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updatemenuIcon) name:@"menuBtnUpdated" object:nil];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    [appDelegate startIndicator];
    
    if (status) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:DICOffers];
        appDelegate.dicOffers = nil;
        
        [[NSUserDefaults standardUserDefaults] synchronize];

        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(loadOffers) name:@"BACKLOGUpdated" object:nil];
        [appDelegate loadFeedbackBacklogValue:false];
    }
    else {
        appDelegate.dicOffers = [[NSUserDefaults standardUserDefaults]objectForKey:DICOffers];
        if (appDelegate.dicOffers != nil)
        {
            [self setUpView];
        }
        else
        {
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:@" No internet connection." andButtonTitle:@"cancel"];
        }
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [self removeTimer];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"menuBtnUpdated" object:nil];
    [feedbackTimer invalidate];
    feedbackTimer = nil;
    [self pauseFeedbackTimer];
}


-(void)setUpView{

    [appDelegate stopIndicator];
    [refreshControl endRefreshing];
    [refreshActivityView removeFromSuperview];
    _btnRefresh.hidden = NO;
    
    arrCouponsToDisplay = [[NSMutableArray alloc] init];

    arrCoupons = [[NSMutableArray alloc] init];
    NSArray * arr_sections = appDelegate.dicOffers[@"Sections"];

    for (int i=0; i<arr_sections.count; i++) {
        NSDictionary *dicContent = arr_sections[i];
        if ([dicContent[@"Name"] isEqualToString:@"Coupons"]) {

            for (NSDictionary *dic in [dicContent objectForKey:@"Content"]) {
                        [arrCoupons addObject:dic];
                        NSString * thumbURLStr = [dic valueForKey:@"ThumbUrl"];
                        if (![thumbURLStr isEqual:[NSNull null]] && thumbURLStr != nil  && thumbURLStr.length > 0) {
                        [arrCouponsToDisplay addObject:dic];
                        }
                    }
        }
        else if ([dicContent[@"Name"] isEqualToString:@"Loyalty"])
        {
                    arrLoyalty = [[NSMutableArray alloc] init];
                    for (NSDictionary *dic in [dicContent objectForKey:@"Content"]) {
                        [arrLoyalty addObject:dic];
                    }
        }
        else if ([dicContent[@"Name"] isEqualToString:@"Rewards"])
        {
                    arrRewards = [[NSMutableArray alloc] init];
                    for (NSDictionary *dic in [dicContent objectForKey:@"Content"]) {
                        [arrRewards addObject:dic];
                        NSString * thumbURLStr = [dic valueForKey:@"ThumbUrl"];
                        if (![thumbURLStr isEqual:[NSNull null]] && thumbURLStr != nil && thumbURLStr.length > 0 ) {
                            [arrCouponsToDisplay addObject:dic];
                        }
                    }
        }
        else if ([dicContent[@"Name"] isEqualToString:@"Offers"])
        {
                    arrOffers = [[NSMutableArray alloc] init];
                    for (NSDictionary *dic in [dicContent objectForKey:@"Content"]) {
                        [arrOffers addObject:dic];
                    }
        }
    }

    if (arrOffers.count > 0) {

        [self pauseFeedbackTimer];
        NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arrOffers objectAtIndex:0] valueForKey:@"ThumbUrl"]]];
        
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        UIImage *image = [UIImage imageWithData:imageData];

        self.offerImageView.image = image; //[UIImage imageNamed:@"scratchHomeScreen"];
        NSDictionary *attribute1 = @{NSForegroundColorAttributeName : THEMECOLOR, NSFontAttributeName : [UIFont fontWithName:LatoBold size:16.0] };
        NSMutableAttributedString *str1 = [[NSMutableAttributedString alloc] initWithString:[[arrOffers objectAtIndex:0] valueForKey:@"Description"] attributes:attribute1];
        self.OfferDescriptionLabel.attributedText = str1;
        [self.goToOfferButton setTitle:@"GO!" forState:UIControlStateNormal];
        self.goToOfferButton.hidden = NO;
    }
    else if(appDelegate.backlogVal > 0){
        self.offerImageView.image = [UIImage imageNamed:@"FeedbackStar"];
        self.offerImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        NSString *temp = [self timeIntervaltoDate:appDelegate.feedbackTime];
        [self startFeedbackTimer];
        
        NSString *feedbackSTR = [NSString stringWithFormat:@"%@ minutes remaining to rate!",temp];
        NSRange timeRange = [feedbackSTR rangeOfString:[NSString stringWithFormat:@"%@ minutes",temp]];
        NSRange remainSTR = [feedbackSTR rangeOfString:@" remaining to rate!"];
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:feedbackSTR];
        [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:LatoRegular size:17.5] range:remainSTR];
        [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:LatoBold size:17.5] range:timeRange];
        [attributedString addAttribute:NSForegroundColorAttributeName value:THEMECOLOR range:timeRange];
        [attributedString addAttribute:NSForegroundColorAttributeName value:THEMECOLOR range:remainSTR];
        
        self.OfferDescriptionLabel.attributedText = attributedString;
        self.offerImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.goToOfferButton setTitle:@"" forState:UIControlStateNormal];
        self.goToOfferButton.hidden = YES;
    }
    else{
        self.offerImageView.image = [UIImage imageNamed:@"referHomeScreen"];
        NSDictionary *attribute1 = @{NSForegroundColorAttributeName : THEMECOLOR, NSFontAttributeName : [UIFont fontWithName:LatoBold size:16.0] };
        NSDictionary *attribute2 = @{NSForegroundColorAttributeName : THEMECOLOR, NSFontAttributeName : [UIFont fontWithName:LatoRegular size:14.0]};

        NSMutableAttributedString *str1 = [[NSMutableAttributedString alloc] initWithString:@"Refer a friend\n" attributes:attribute1];
        NSMutableAttributedString *str2 = [[NSMutableAttributedString alloc] initWithString:@"and win exciting rewards!" attributes:attribute2];

        
        [self pauseFeedbackTimer];
        NSMutableAttributedString * finalStr = [[NSMutableAttributedString alloc] initWithAttributedString:str1];
        [finalStr appendAttributedString:str2];

        [self.OfferDescriptionLabel setAttributedText: finalStr];
        self.offerImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.goToOfferButton setTitle:@"RECRUIT" forState:UIControlStateNormal];
        self.goToOfferButton.hidden = NO;
    }
    
    _pageController.numberOfPages = arrCouponsToDisplay.count;
    _pageController_Loyalty.numberOfPages = arrLoyalty.count;
    
    [self UpdateSideMenuTable];

    [self.gridCoupon reloadData];

    if (arrCouponsToDisplay.count > 1) {
        int index = (int)arrCouponsToDisplay.count * 100;//MaxSections/(int)arrCouponsToDisplay.count;
        [self.gridCoupon scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0 ] atScrollPosition:UICollectionViewScrollPositionLeft animated:false];
        lastContentOffset = self.gridCoupon.contentOffset.x;
        _pageController.currentPage = 0;
        [self addTimer];
    }
    [self.gridLoyalty reloadData];
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    
    NSString *today = [formatter stringFromDate:[NSDate date]];
    NSString *previousSaveddate = [[NSUserDefaults standardUserDefaults] stringForKey:@"PreferenceCheckUpDate"];
    NSDate *prevDate = [formatter dateFromString:previousSaveddate];
    NSString *dateToCompare = [formatter stringFromDate:prevDate];
    
    NSComparisonResult result = [dateToCompare compare:[formatter stringFromDate:[NSDate date]]];

    switch (result)
    {
        case NSOrderedAscending:
        {
//            NSLog(@"%@ is less than %@", dateToCompare, today);
            [self targetMethodToCheckPrefernces];
        }
        break;
        case NSOrderedDescending: NSLog(@"%@ is greater %@", dateToCompare, today); break;
        case NSOrderedSame: NSLog(@"%@ is equal to %@", dateToCompare, today); break;
        default: NSLog(@"erorr dates %@, %@", dateToCompare, today); break;
    }
}

-(void)targetMethodToCheckPrefernces{
    if(arc4random() % 10 == 1)
    {
//        NSLog(@"Called Random Function for Preference Check");
        //call your function
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"PreferencesSet"];
        [self loadUserProfile];
    }
}


//MARK: - Time Interval New Method
- (NSString *)timeIntervaltoDate:(NSString*)date
{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    NSDate *startDate = [NSDate date];
    
    NSDate *endDate = [f dateFromString:date];
    
    NSCalendar* currentCalendar = [NSCalendar currentCalendar];
    NSCalendarUnit unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *differenceComponents = [currentCalendar components:unitFlags fromDate:startDate toDate:endDate options:0];
    
    NSInteger dayDifference = [differenceComponents day];
    NSInteger hourDifference = [differenceComponents hour];
    NSInteger minuteDifference = [differenceComponents minute];
    NSInteger secondDifference = [differenceComponents second];
    
    int miliHours = (int)(((hourDifference * 60) * 60) * 1000);
    int miliMinuts = (int)((minuteDifference * 60) * 1000);
    int secondMili = (int)secondDifference * 1000;
    
    miliSecond = miliHours + miliMinuts + secondMili;
    
    if (dayDifference > 0) {
        return [NSString stringWithFormat:@"%02ld days",(long)dayDifference];
    }
    else {

        double finalValue = (hourDifference *60)+minuteDifference ;
        lastTimeValue = finalValue;
         return [NSString stringWithFormat:@"%.0f",lastTimeValue];
    }
    
    return nil;
}


#pragma mark -
#pragma mark Custom Methods

- (void)loadOffers {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BACKLOGUpdated" object:nil];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        NSString *urlString = [NSString stringWithFormat:@"%@content/?UserId=%@", BASEURL, appDelegate.strUserId];
//        NSString *urlString = [NSString stringWithFormat:@"%@shopoffers/?UserId=%@&CategoryIds", BASEURL, appDelegate.strUserId];

        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:100.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
//        NSLog(@"URLSTRING: %@",urlString);
//        NSLog(@"HEADERS : %@",request.requestHeaders);
        
        [request startAsynchronous];
    }
    else {
        
        [refreshControl endRefreshing];
        [refreshActivityView removeFromSuperview];
        _btnRefresh.hidden = NO;
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:@" No internet connection." andButtonTitle:@"cancel"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    _btnRefresh.hidden = NO;
    NSDictionary * response = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    
//    NSLog(@"RESPONSE : %@",response);
    
    if ([[response allKeys] containsObject:@"ResponseStatus"]) {
        [appDelegate stopIndicator];
        
        
        if ([[[response objectForKey:@"ResponseStatus"] objectForKey:@"ErrorCode"] isEqualToString:@"48"]) {
            
            //CAll API here
            [self loadUserProfile];
        }
        else{
            NSString *errMessage = [[response objectForKey:@"ResponseStatus"] objectForKey:@"Message"];
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:[NSString stringWithFormat:@"Error Code %@",[[response objectForKey:@"ResponseStatus"] objectForKey:@"ErrorCode"]] andMessage:errMessage];
        }
    }
    else if ([[response allKeys] containsObject:@"Sections"] && [response objectForKey:@"Sections"] != nil){
        
        appDelegate.dicOffers = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
        
        [NSUserDefaults.standardUserDefaults setObject:appDelegate.dicOffers forKey:DICOffers];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self performSelector:@selector(setUpView) withObject:nil afterDelay:0.1];
        
    }
    [appDelegate stopIndicator];
    
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    [refreshControl endRefreshing];
    
    [refreshActivityView removeFromSuperview];
    _btnRefresh.hidden = NO;
    _pageController.numberOfPages = 0;
    appDelegate.dicOffers = [[NSUserDefaults standardUserDefaults]objectForKey:DICOffers];
    [self setUpView];
}


- (void)updatemenuIcon {
    if (self.navigationController.isSideMenuOpen == YES){
        //redCross
        [self.hamMenuBtn setImage:[UIImage imageNamed:@"redCross"] forState:UIControlStateNormal];
    }else{
        //hamMenuIcon
        [self.hamMenuBtn setImage:[UIImage imageNamed:@"hamMenuIcon"] forState:UIControlStateNormal];
    }
}




#pragma mark -
#pragma mark Feedback NSTimer Methods

- (void)updateFeedbackTimer:(NSTimer *)timer {
    
    if(appDelegate.backlogVal > 0){
    
        isLoadedOffer = YES;
        
        miliSecond -= 60000 ;
        int seconds = miliSecond/1000;
        int minutes = seconds / 60;
        int hours = minutes / 60;
        
        if (seconds == 0 && minutes == 0 && hours == 0) {
            [self pauseFeedbackTimer];
        }
        else {
            seconds -= minutes * 60;
            minutes -= hours * 60;
            double finalValue = (hours *60)+minutes;
            NSString *remainingTime = [NSString stringWithFormat:@"%.0f",finalValue];
            [self.OfferDescriptionLabel setText:[NSString stringWithFormat:@"%@%@%@",remainingTime,@" ",@"Mins remain. Give feedback!"]];
        }
    }
}

- (void)startFeedbackTimer {
    if (feedbackTimer == nil){
        feedbackTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(updateFeedbackTimer:) userInfo:nil repeats:YES];

    }
}

-(void)pauseFeedbackTimer {
    if ([feedbackTimer isValid]) {
        [feedbackTimer invalidate];
    }
}

- (void)populateLabelwithTime:(int)milliseconds {
    
    if (lastTimeValue != 0)
    {
    lastTimeValue = lastTimeValue -1;
    }
    [self.OfferDescriptionLabel setText:[NSString stringWithFormat:@"%.0f%@%@",lastTimeValue,@" ",@"Mins remain. Give feedback!"]];
}


-(void)UpdateSideMenuTable{
    
    NSInteger numberOfCoupons = [[NSUserDefaults standardUserDefaults] integerForKey:@"COUPONCOUNT"];
    NSInteger numberOfLoaylty = [[NSUserDefaults standardUserDefaults] integerForKey:@"LOYALTYCOUNT"];
    NSInteger numberOfRewards = [[NSUserDefaults standardUserDefaults] integerForKey:@"REWARDSCOUNT"];
    NSInteger numberOfOffers = [[NSUserDefaults standardUserDefaults] integerForKey:@"OFFERSCOUNT"];

    MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
    mo.arrCouponCount = (int)[[appDelegate.dicOffers objectForKey:@"Coupons"] count] - (int)numberOfCoupons;
    mo.arrRewardsCount = (int)[[appDelegate.dicOffers objectForKey:@"Rewards"] count] - (int)numberOfRewards;
    mo.arrLoyaltyCount = (int)[[appDelegate.dicOffers objectForKey:@"Loyalty"] count] - (int)numberOfLoaylty;
    mo.arrOffersCount = (int)[[appDelegate.dicOffers objectForKey:@"Offers"] count] - (int)numberOfOffers;
    mo.feedbackCount = appDelegate.backlogVal;
    
    [[NSUserDefaults standardUserDefaults] setInteger:[[appDelegate.dicOffers objectForKey:@"Loyalty"] count] forKey:@"LOYALTYCOUNT"];
    [[NSUserDefaults standardUserDefaults] setInteger:[[appDelegate.dicOffers objectForKey:@"Rewards"] count] forKey:@"REWARDSCOUNT"];
    [[NSUserDefaults standardUserDefaults] setInteger:[[appDelegate.dicOffers objectForKey:@"Coupons"] count] forKey:@"COUPONCOUNT"];
    [[NSUserDefaults standardUserDefaults] setInteger:[[appDelegate.dicOffers objectForKey:@"Offers"] count] forKey:@"OFFERSCOUNT"];

    [[NSUserDefaults standardUserDefaults] synchronize];
    [mo.tableView reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (IBAction)toggleSideMenuView:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
    [self.navigationController toggleSideMenuView];
    });
}

#pragma mark
#pragma mark - FlowLayout
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(collectionView.bounds.size.width, ([UIScreen mainScreen].bounds.size.height - 119)/2);
}

#pragma mark - CollectionView
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if ([collectionView isEqual:self.gridCoupon]) {
        if (arrCouponsToDisplay.count == 1) {
            return 1;
        }
        return arrCouponsToDisplay.count * 1000 ;
    }
    else{

        if (arrLoyalty.count > 0) {
            return 1;
        }
        return arrLoyalty.count;
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier;
    if ([collectionView isEqual:self.gridCoupon]) {
        cellIdentifier = @"CouponCellGRID";
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:1200];
        
        int index;

        if (arrCouponsToDisplay.count == 1) {
            index = 0;
        }
        else {
            index = (int)indexPath.row % arrCouponsToDisplay.count;
        }
        //todo
        
        if ([[arrCouponsToDisplay objectAtIndex:index] objectForKey:@"ThumbUrl"] != nil) {

            [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [[[arrCouponsToDisplay objectAtIndex:index] objectForKey:@"ThumbUrl"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
                
                if (image && cacheType == SDImageCacheTypeNone) {
                    imageView.alpha = 0.0;
                    [UIView animateWithDuration:3.0 animations:^{
                        imageView.alpha = 1.0;
                    }];
                }
            }];

            //[imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [[[arrCouponsToDisplay objectAtIndex:index] objectForKey:@"ThumbUrl"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]] placeholderImage:[UIImage imageNamed:@"placeholder"] options:SDWebImageRefreshCached];
        }
        else
        {
            imageView.image = nil;
            imageView.backgroundColor = [UIColor whiteColor];
            
        }        
        
        return cell;
    }
    else{
        cellIdentifier = @"LoyalityCellGRID";
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:1201];        
       
        
        int index;
        if (arrLoyalty.count == 1) {
            index = 0;
        }
        else {
            index = (int)indexPath.row ;
        }

        if ([[arrLoyalty objectAtIndex:index] objectForKey:@"ThumbUrl"] == nil) {
            [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [[[arrLoyalty objectAtIndex:index] objectForKey:@"TopCouponImageUrl"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
                if (image && cacheType == SDImageCacheTypeNone) {
                    imageView.alpha = 0.0;
                    [UIView animateWithDuration:3.0 animations:^{
                        imageView.alpha = 1.0;
                    }];
                }
            }];
          //  [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [[[arrLoyalty objectAtIndex:index] objectForKey:@"TopCouponImageUrl"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]] placeholderImage:[UIImage imageNamed:@"placeholder"] options:SDWebImageRefreshCached];
        }
        else{
            [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [[[arrLoyalty objectAtIndex:index] objectForKey:@"ThumbUrl"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
                if (image && cacheType == SDImageCacheTypeNone) {
                    imageView.alpha = 0.0;
                    [UIView animateWithDuration:3.0 animations:^{
                        imageView.alpha = 1.0;
                    }];
                }
            }];
            
           // [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [[[arrLoyalty objectAtIndex:index] objectForKey:@"ThumbUrl"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]] placeholderImage:[UIImage imageNamed:@"placeholder"] options:SDWebImageRefreshCached];
        }
        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([collectionView isEqual:self.gridCoupon]) {
        
        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
        NSUInteger selecetdIndex;
        
        if (indexPath.item >= arrCouponsToDisplay.count) {
            int newIndex = ((int)indexPath.item) % arrCouponsToDisplay.count;
            NSDictionary *tempDict = [arrCouponsToDisplay objectAtIndex:newIndex];
            if ([arrCoupons containsObject:tempDict])
            {
                selecetdIndex = [arrCoupons indexOfObject:tempDict];
                mo.selectedPageOnDetail = selecetdIndex;
                [mo tableView:mo.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
            }
            else if  ([arrRewards containsObject:tempDict])
            {
                selecetdIndex = [arrRewards indexOfObject:tempDict];
                mo.selectedPageOnDetail = selecetdIndex;
                [mo tableView:mo.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
            }
            else
            {
            selecetdIndex = newIndex;
            }
        }
        else{
            selecetdIndex = (int)indexPath.item;
            if (selecetdIndex < arrCoupons.count) {
                            mo.selectedPageOnDetail = selecetdIndex;
                            [mo tableView:mo.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
                        }
                        else{
                            mo.selectedPageOnDetail = selecetdIndex - (int)arrCoupons.count;
                            [mo tableView:mo.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
                        }
        }
    }
    else{
        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
        mo.selectedPageOnDetail = (int)indexPath.item;
        [mo tableView:mo.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    }
}


//MARK: - AUTO SCROLLING
-(void)addTimer{
    
    if (timer) {
        [timer invalidate];
    }
    
    timer = [NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(nextPage) userInfo:nil repeats:true];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

-(void)nextPage{
    
    int MaxSections;
    if (arrCouponsToDisplay.count == 1) {
        MaxSections = (int)arrCouponsToDisplay.count;
    }
    else {
        MaxSections = (int)arrCouponsToDisplay.count * 1000;
    }
    lastContentOffset = self.gridCoupon.contentOffset.x;
    
    NSArray *ar = [self.gridCoupon indexPathsForVisibleItems];
    
    if (ar.count > 0) {
        NSIndexPath *currentIndexPathReset = ar[0];
        
        if(currentIndexPathReset.item >= MaxSections - 1)
        {
            currentIndexPathReset = [NSIndexPath indexPathForItem:0 inSection:0];//NSIndexPath(forItem:0, inSection:0)
            _pageController.currentPage = 0;
            [_gridCoupon scrollToItemAtIndexPath:currentIndexPathReset atScrollPosition:UICollectionViewScrollPositionRight animated:false];
        }
        else
        {
            int newIndex = ((int)currentIndexPathReset.item + 1) % arrCouponsToDisplay.count;
            int newIndex2 = ((int)currentIndexPathReset.item + 1);

            _pageController.currentPage = newIndex;
            [_gridCoupon scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:newIndex2 inSection:0 ] atScrollPosition:UICollectionViewScrollPositionLeft animated:true];
        }
    }
}

-(void)removeTimer
{
    [timer invalidate];
    //[feedbackTimer invalidate];
}

// UIScrollView' delegate method
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self removeTimer];
}



-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSArray *ar = [self.gridCoupon indexPathsForVisibleItems];
    
    if (lastContentOffset < scrollView.contentOffset.x) {
        [self CalculateNextPage];
    }
    else{
        //[self CalculatePreviousPage];

        if (ar.count > 1) {
            NSIndexPath *currentIndexPathReset = ar[0];
            NSIndexPath *newIndexPathReset = ar[1];
            
            int oldIndex = ((int)currentIndexPathReset.item) % arrCouponsToDisplay.count;
            int newIndex = ((int)newIndexPathReset.item) % arrCouponsToDisplay.count;
            
            int ind = MIN(oldIndex, newIndex);
            
            if (ind ==  newIndex) {
                // previous
                _pageController.currentPage = newIndex;
                int newIndexToScroll = ((int)newIndexPathReset.item);
                
                
                [_gridCoupon scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:newIndexToScroll inSection:0 ] atScrollPosition:UICollectionViewScrollPositionRight animated:false];
            }
            else{
                _pageController.currentPage = oldIndex;
                int newIndexToScroll = ((int)currentIndexPathReset.item);
                
                
                [_gridCoupon scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:newIndexToScroll inSection:0 ] atScrollPosition:UICollectionViewScrollPositionRight animated:false];
            }
        }
        else if (ar.count == 1){
            
            NSIndexPath *currentIndexPathReset = ar[0];
            int index = ((int)currentIndexPathReset.item) % arrCouponsToDisplay.count;

            _pageController.currentPage = index;
            int newIndexToScroll = ((int)currentIndexPathReset.item);
            
            [_gridCoupon scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:newIndexToScroll inSection:0 ] atScrollPosition:UICollectionViewScrollPositionRight animated:false];
        }
    }
    lastContentOffset = scrollView.contentOffset.x;
    
    if (arrCouponsToDisplay.count > 1) {
        [self addTimer];
    }
}


-(void)CalculateNextPage{
    int MaxSections;
    if (arrCouponsToDisplay.count == 1) {
        MaxSections = (int)arrCouponsToDisplay.count;
    }
    else {
        MaxSections = (int)arrCouponsToDisplay.count * 1000;
    }

    NSArray *ar = [self.gridCoupon indexPathsForVisibleItems];
    if (ar.count > 0) {
        NSIndexPath *currentIndexPathReset = ar[0];
        
        if(currentIndexPathReset.item >= MaxSections - 1)
        {
            currentIndexPathReset = [NSIndexPath indexPathForItem:0 inSection:0];
            _pageController.currentPage = 0;
        }
        else
        {
            int newIndex = ((int)currentIndexPathReset.item + 1) % arrCouponsToDisplay.count;
            _pageController.currentPage = newIndex;
        }
    }
}

-(void)CalculatePreviousPage{
    int MaxSections;
    if (arrCouponsToDisplay.count == 1) {
        MaxSections = (int)arrCouponsToDisplay.count;
    }
    else {
        MaxSections = (int)arrCouponsToDisplay.count * 1000;
    }
    
    NSArray *ar = [self.gridCoupon indexPathsForVisibleItems];
    if (ar.count > 0) {
        NSIndexPath *currentIndexPathReset = ar[0];
        
        if(currentIndexPathReset.item >= MaxSections - 1)
        {
            currentIndexPathReset = [NSIndexPath indexPathForItem:0 inSection:0];
            _pageController.currentPage = 0;
        }
        else
        {
            int newIndex = ((int)currentIndexPathReset.item + 1) % arrCouponsToDisplay.count;
            _pageController.currentPage = newIndex;
        }
    }
    
}


#pragma mark -
#pragma mark UIBarButtoItem Methods


- (IBAction)RfreshButtonPressed:(id)sender {
    [self removeTimer];
    if (refreshActivityView == nil) {
        refreshActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        refreshActivityView.frame = self.btnRefresh.frame;
    }
    [refreshActivityView startAnimating];
    [refreshActivityView hidesWhenStopped];
    [self.imgHeader addSubview:refreshActivityView];

    
    _btnRefresh.hidden = YES;
    [appDelegate startIndicator];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(loadOffers) name:@"BACKLOGUpdated" object:nil];
    [appDelegate loadFeedbackBacklogValue:false];
}

- (IBAction)GoToOfferButtonPressed:(id)sender;
{
    MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];

    if (arrOffers.count > 0){

        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CouponDetailsViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"detailsController"];
        dashboard.title = @"OFFERS"; // Stamp
        dashboard.selectedPageOnDetail = 0;

        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
    }
    else if(appDelegate.backlogVal > 0){
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"ISACTIVEFEEDBACK"];
        [mo tableView:mo.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0]];
    }
    else{
        [mo tableView:mo.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:8 inSection:0]];
        mo.LastselectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self hideWelcomeMessage];
}


-(BOOL)prefersStatusBarHidden{
    return YES;
}

#pragma mark
#pragma mark - GET USER PROFILE

- (void)loadUserProfile {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    [appDelegate startIndicator];
    if (status) {
        
        NSString *urlString = [NSString stringWithFormat:@"%@user/?UserID=%@", BASEURL, appDelegate.strUserId];
        
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request setDidFinishSelector:@selector(requestFinished_UserProfile:)];
        [request setDidFailSelector:@selector(requestFailed_UserProfile:)];
        
        
        [request startAsynchronous];
    }
    else {
        
        [appDelegate stopIndicator];
        [CustomAlertController showAlertOnController:appDelegate.window.rootViewController withAlertType:Simple withTitle:@"Error occurred" andMessage:@"No internet connection." andButtonTitle:@"Ok"];
    }
}

- (void)requestFinished_UserProfile:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    NSDictionary *dicUser = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    [[NSUserDefaults standardUserDefaults]setObject:dicUser forKey:DICProfileData];
    NSArray *dicConsents = [dicUser objectForKey:@"Consents"];
//    NSLog(@"%@",dicConsents);
    [[NSUserDefaults standardUserDefaults]setObject:dicConsents forKey:DICConsents];
    
    
    
    for (NSDictionary *dict in dicConsents) {
        NSDictionary * dic = [dict objectForKey:@"CurrentVersion"];
        if ([dict[@"Name"] isEqualToString:@"Master"]) {
            
            appDelegate.privacyPolicyStr = @"";
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:PrivacyPolicy];
            
            appDelegate.privacyPolicyStr = [dic objectForKey:@"PrivacyPolicy"];
            appDelegate.minimumAgeFactor = [dic objectForKey:@"MinimumAge"];
            appDelegate.dobStringMinimumAge = [dic objectForKey:@"MinimumAgeText"];
            appDelegate.registrationTitle = [dic objectForKey:@"Title"];
        }
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:dicConsents forKey:DICConsents];
    [[NSUserDefaults standardUserDefaults]setObject:appDelegate.privacyPolicyStr forKey:PrivacyPolicy];
    [[NSUserDefaults standardUserDefaults]setObject:appDelegate.minimumAgeFactor forKey:MINAge];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // SAVE PREFERENCE CHANGE DATE
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    NSString *today = [formatter stringFromDate:[NSDate date]];
    [[NSUserDefaults standardUserDefaults] setObject:today forKey:@"PreferenceCheckUpDate"];
    
    [self MakeDecision];
}

-(void)MakeDecision{
    
    NSArray *consents = [[NSUserDefaults standardUserDefaults] objectForKey:DICConsents];
    NSString *masterConsetState;
    for (NSDictionary *dicr in consents) {
        if ([dicr[@"Name"] isEqualToString:@"Master"]){
            masterConsetState = dicr[@"State"];
        }
        else{
            //check if any of preferences has changed?
        }
    }
    
    
    if (![masterConsetState isEqualToString:@"ConsentGiven"]){
        //Terms
        
        TermsViewController *dashboard = [mainStoryboard instantiateViewControllerWithIdentifier:@"termsController"];
        dashboard.isloggedin = true;
        dashboard.isNewMasterConsent = true;
        
        MenuItemsTVC *sidemenu = [MenuItemsTVC sharedInstance];
        ENSideMenuNavigationController *naviController = [[ENSideMenuNavigationController alloc] initWithMenuViewController:sidemenu contentViewController:dashboard];
        naviController.navigationBarHidden = YES;
        appDelegate.isLoader = YES;
        
        CGFloat sidemenuWide = SCREEN_WIDTH - 75  ;
        [naviController.sideMenu setMenuWidth:sidemenuWide];
        
        [appDelegate.window makeKeyAndVisible];
        [appDelegate.window setRootViewController:naviController];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"PreferencesSet"] == false){
        PreferencesViewController *registerController = [mainStoryboard instantiateViewControllerWithIdentifier:@"PreferencesViewController"];
        registerController.isNewMasterConsent = false;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:registerController];
        navController.navigationBarHidden = YES;
        appDelegate.window.rootViewController = navController;
    }
    else{
        [self loadOffers];
    }
}


- (void)requestFailed_UserProfile:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    
    [CustomAlertController showAlertOnController:appDelegate.window.rootViewController withAlertType:Simple withTitle:@"Error" andMessage:request.error.localizedDescription andButtonTitle:@"Ok"];
}



@end
