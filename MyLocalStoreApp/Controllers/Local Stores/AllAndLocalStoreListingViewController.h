//
//  AllAndLocalStoreListingViewController.h
//  MyLocalStoreApp
//
//  Created by Surbhi on 14/12/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllAndLocalStoreListingViewController : UIViewController


@property (nonatomic, weak) IBOutlet UIView *viewContainer;
@property (nonatomic, weak) IBOutlet UIView *viewFilter;
@property (nonatomic, weak) IBOutlet UITableView *tblView;

- (IBAction)SideButtonPressed:(id)sender;
- (void)FilterButtonPressed:(id)sender ;
- (void)SectionButtonPressed:(id)sender;

@end
