//
//  AddMyLocalStoreViewController.h
//  MyLocalStoreApp
//
//  Created by Surbhi on 14/12/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddMyLocalStoreViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITableView *tblFavStore;
@property (nonatomic, weak) IBOutlet UIButton *btnEdit;
@property (nonatomic, weak) IBOutlet UIButton *btnAdd;

- (IBAction)SideButtonPressed:(id)sender;

- (IBAction)AddStoreButtonPressed:(id)sender;
- (IBAction)EditButtonPressed:(id)sender;


@end
