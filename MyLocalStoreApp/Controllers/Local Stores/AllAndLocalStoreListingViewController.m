//
//  AllAndLocalStoreListingViewController.m
//  MyLocalStoreApp
//
//  Created by Surbhi on 14/12/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import "AllAndLocalStoreListingViewController.h"
#import "MyLocalStoreApp.pch"
#import "AllStoreCollectionViewController.h"
#import "LocalCollectionViewController.h"
#import "FilterTimeCell.h"
#import "FilterPriceCell.h"
#import "FilterDistanceCell.h"

@interface AllAndLocalStoreListingViewController () <RMPScrollingMenuBarControllerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    
    UIButton *filterBtn;
    RMPScrollingMenuBarController * RmpMenuCntrllr;
    UIView *viewS;
    NSArray *arrSectionTitle;
    NSMutableArray *arrupdate;

    NSMutableDictionary *dictFilter;

}

@end

@implementation AllAndLocalStoreListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrSectionTitle = @[@"PICKUP TIME",@"DISTANCE",@"PRICE"];
    arrupdate = [NSMutableArray arrayWithCapacity:3];
    [arrupdate addObject:@"0"];
    [arrupdate addObject:@"0"];
    [arrupdate addObject:@"0"];

    _tblView.tableFooterView = [[UIView alloc]init];
    
    dictFilter = [NSMutableDictionary dictionaryWithObjectsAndKeys: @01 ,@"pickUpTimeStart",@7.4,@"pickUpTimeEnd",@12.50,@"Distance",@12,@"MaxPrice",@5,@"MinPrice",nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    RmpMenuCntrllr = [[RMPScrollingMenuBarController alloc]init];
    
    AllStoreCollectionViewController *allStoreCollectionView = [mainStoryboard instantiateViewControllerWithIdentifier:@"AllStoreCollectionViewController"];
    
    LocalCollectionViewController *allLocalCollectionView = [mainStoryboard instantiateViewControllerWithIdentifier:@"LocalCollectionViewController"];
    
    [RmpMenuCntrllr setViewControllers:@[allStoreCollectionView,allLocalCollectionView]];
    
    RmpMenuCntrllr.delegate = self;
    RmpMenuCntrllr.menuBar.indicatorColor = [UIColor clearColor];
    
    RmpMenuCntrllr.menuBar.showsIndicator = NO;
    RmpMenuCntrllr.menuBar.showsSeparatorLine = NO;
    RmpMenuCntrllr.view.backgroundColor = [UIColor whiteColor];
    
    RmpMenuCntrllr.menuBar.frame = CGRectZero;
    [self addChildViewController:RmpMenuCntrllr];
    [RmpMenuCntrllr.view setFrame:CGRectMake(0.0f, 0, self.view.frame.size.width, self.view.frame.size.height-0)];
    
    
    [RmpMenuCntrllr willMoveToParentViewController:self];

    [self.viewContainer addSubview:RmpMenuCntrllr.view];
    

    [RmpMenuCntrllr didMoveToParentViewController:self];
    
    
    filterBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-40,0, 30, 30)];
    [self.viewContainer addSubview:filterBtn];
    [filterBtn setImage:[UIImage imageNamed:@"filter_icon"] forState:UIControlStateNormal];

    [filterBtn addTarget:self action:@selector(FilterButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [filterBtn setHidden:true];


}

- (IBAction)SideButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnCancelClicked:(id)sender {
    [_viewFilter removeFromSuperview];
}

- (IBAction)btnSaveFilterClicked:(id)sender {

    NSLog(@"button reset clicekd");
    
}

- (IBAction)btnResetClicked:(id)sender {
    NSLog(@"reset is cliecked");
    float numD ;
    int seconds , hours ;
    float number = modff([[dictFilter objectForKey:@"pickUpTimeStart"] floatValue], &numD);
    number = (number * 60.00) ;
    seconds = number ;
    hours = numD;
    float finalVal = hours + (seconds/100);
    NSLog(@"final value === %f",finalVal);
   // [dictFilter setObject:[NSNumber numberWithFloat:(finalVal)]  forKey:@"pickUpTimeStart"];

    number = modff([[dictFilter objectForKey:@"pickUpTimeEnd"] floatValue], &numD);
    number = (number * 60.00) ;
    seconds = number ;
    hours = numD;
    numD = hours + (seconds/100);
    
    

  //  [dictFilter setObject:[NSNumber numberWithFloat:(numD)]  forKey:@"pickUpTimeEnd"];
    
    NSLog(@"dict == %@",dictFilter);

}

- (void)FilterButtonPressed:(id)sender {
    NSLog(@"filter button pressed");
    [self.viewContainer addSubview:_viewFilter];
    
}

- (void)SectionButtonPressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"section button %ld",(long)btn.tag);

    if ([arrupdate[btn.tag]  isEqual: @"1"]){
        arrupdate[btn.tag] = @"0";
    }else{
        arrupdate[btn.tag] = @"1";
    }
    [_tblView beginUpdates];
    [_tblView reloadSections:[NSIndexSet indexSetWithIndex:btn.tag] withRowAnimation:UITableViewRowAnimationFade];
    [_tblView endUpdates];
    
}

- (NSString*)convertNumberToTimeString:(float)number {
    float numH ;
    int seconds , hours ;
    float numS = modff(number, &numH);
    numS = (number * 60.00) ;
    seconds = numS ;
    hours = numH;
    
    return [NSString stringWithFormat:@"%02d : %02d",hours, seconds];;
    
}



-(void)menuBarController:(RMPScrollingMenuBarController *)menuBarController didSelectViewController:(UIViewController *)viewController{
    
    if ([viewController isKindOfClass:[LocalCollectionViewController class]]){
        [filterBtn setHidden:false];
    }else{
        [filterBtn setHidden:true];
    }
}

- (RMPScrollingMenuBarItem*)menuBarController:(RMPScrollingMenuBarController *)menuBarController menuBarItemAtIndex:(NSInteger)index{
    
    if (index == 0){
        RMPScrollingMenuBarItem* item = [[RMPScrollingMenuBarItem alloc] init];
        item.title = @"ALL STORES";
        return item;
    }
    
    RMPScrollingMenuBarItem* item = [[RMPScrollingMenuBarItem alloc] init];
    item.title = @"LOCAL";
    return item;
}

- (void)rangeSliderValueDidChange:(MARKRangeSlider *)slider{
    NSLog(@"rage slider did cnahge ");
    if (slider.tag == 0){
    FilterTimeCell *filterCell = (FilterTimeCell*)[_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    float numD ;
    int seconds , hours ;
    float number = modff(filterCell.rangeSlider.leftValue, &numD);
    number = (number * 60.00) ;
    seconds = number ;
    hours = numD;
        filterCell.leftLbl.text = [NSString stringWithFormat:@"%02d : %02d",hours, seconds];
        [dictFilter setObject:[NSNumber numberWithFloat:(filterCell.rangeSlider.leftValue)]  forKey:@"pickUpTimeStart"];

    number = modff(filterCell.rangeSlider.rightValue, &numD);
    number = (number * 60.00) ;
    seconds = number ;
    hours = numD;
        filterCell.rightLbl.text = [NSString stringWithFormat:@"%02d : %02d",hours, seconds];
        [dictFilter setObject:[NSNumber numberWithFloat:(filterCell.rangeSlider.rightValue)]  forKey:@"pickUpTimeEnd"];


    NSLog(@"%@ - %@", filterCell.leftLbl.text, filterCell.rightLbl.text);
    }
    if (slider.tag == 1){
        FilterDistanceCell *filterCell = (FilterDistanceCell*)[_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        
        [dictFilter setObject:[NSNumber numberWithDouble:slider.rightValue]  forKey:@"Distance"];
        filterCell.rightLbl.text = [NSString stringWithFormat:@"%02.01fKM",slider.rightValue];

    }

    if (slider.tag == 2){
        FilterPriceCell *filterCell = (FilterPriceCell*)[_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
        int leftValue = slider.leftValue;
        filterCell.leftLbl.text = [NSString stringWithFormat:@"$%d",leftValue];

        int rightValue = slider.rightValue;
        filterCell.rightLbl.text = [NSString stringWithFormat:@"$%d",rightValue];
        
        [dictFilter setObject:[NSNumber numberWithInt:leftValue]  forKey:@"MinPrice"];
        [dictFilter setObject:[NSNumber numberWithInt:rightValue]  forKey:@"MaxPrice"];


    }
    
    
}





#pragma mark -
#pragma mark UITableViewDataSource Methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (([arrupdate[section]  isEqual: @"1"])){
        return 1;
    }
    
    return  0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        static NSString *cellIdentifier = @"FilterTimeCell";
        FilterTimeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        [cell toUpdateUI];
        [cell.rangeSlider addTarget:self action:@selector(rangeSliderValueDidChange:) forControlEvents:UIControlEventValueChanged];
        [cell.rangeSlider setMinValue:0 maxValue:23.99];//max min value
        cell.rangeSlider.minimumDistance = 0.5;
        cell.rangeSlider.tag = indexPath.section;
        
        [cell.rangeSlider setLeftValue: [[dictFilter objectForKey:@"pickUpTimeStart"]floatValue] rightValue:[[dictFilter objectForKey:@"pickUpTimeEnd"]floatValue]]; //left and right value
        
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self rangeSliderValueDidChange:cell.rangeSlider];
        });
        
        return cell;
    }
    if (indexPath.section == 1){
        static NSString *cellIdentifier = @"FilterDistanceCell";
        FilterDistanceCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        [cell toUpdateUI];
        [cell.rangeSlider addTarget:self action:@selector(rangeSliderValueDidChange:) forControlEvents:UIControlEventValueChanged];
        [cell.rangeSlider setMinValue:0 maxValue:50];//max min value
        cell.rangeSlider.minimumDistance = 0.1;
        cell.rangeSlider.tag = indexPath.section;
        
        [cell.rangeSlider setLeftValue:-1 rightValue:[[dictFilter objectForKey:@"Distance"]doubleValue]]; //left and right value

        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self rangeSliderValueDidChange:cell.rangeSlider];
        });
        return cell;
    }
    if (indexPath.section == 2){
        static NSString *cellIdentifier = @"FilterPriceCell";
        FilterPriceCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        [cell toUpdateUI];
        [cell.rangeSlider addTarget:self action:@selector(rangeSliderValueDidChange:) forControlEvents:UIControlEventValueChanged];
        [cell.rangeSlider setMinValue:0 maxValue:50];//max min value
        cell.rangeSlider.minimumDistance = 1;
        cell.rangeSlider.tag = indexPath.section;
        
        [cell.rangeSlider setLeftValue:[[dictFilter objectForKey:@"MinPrice"]integerValue] rightValue:[[dictFilter objectForKey:@"MaxPrice"]integerValue] ]; //left and right value

        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self rangeSliderValueDidChange:cell.rangeSlider];
        });
        
        return cell;
    }
    
    return  [[UITableViewCell alloc]init];
    
    

}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *viewH = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    UIButton *btnPlus = [[UIButton alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-40, 10, 30,30)];
     [btnPlus setImage:[UIImage imageNamed:@"black_plus"] forState:UIControlStateNormal];
    [btnPlus setImage:[UIImage imageNamed:@"black_minus"] forState:UIControlStateSelected];
    
    [btnPlus addTarget:self action:@selector(SectionButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    btnPlus.tag = section;
    UILabel *lblText = [[UILabel alloc]initWithFrame:CGRectMake(20, 10, [UIScreen mainScreen].bounds.size.width/2, 30)];
    lblText.text = arrSectionTitle[section];
    
    [viewH addSubview:lblText];
    [viewH addSubview:btnPlus];
    
   if([arrupdate[section]  isEqual: @"1"]){
       lblText.textColor = [UIColor colorWithRed:242/255.0 green:82/255.0 blue:104/255.0 alpha:1.0];
       btnPlus.selected = YES;
   }else{
       lblText.textColor = [UIColor blackColor];
       btnPlus.selected = NO;
   }
    
    return viewH;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 115;
    
}

@end
