//
//  AllStoreCollectionViewController.m
//  MyLocalStoreApp
//
//  Created by Surbhi on 14/12/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import "AllStoreCollectionViewController.h"
#import "AllStoreCollectionCell.h"

@interface AllStoreCollectionViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@end

@implementation AllStoreCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark
#pragma mark - FlowLayout
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((collectionView.bounds.size.width/2)-1, 180);
    
}


#pragma mark - DataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 10;
}



-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
       static NSString *cellIdentifier = @"AllStoreCollectionCell";
         AllStoreCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
       return  cell;
    
  }


@end
