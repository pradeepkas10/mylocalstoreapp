//
//  StoreListingViewController.m
//  MyLocalStoreApp
//
//  Created by Surbhi on 14/12/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import "StoreListingViewController.h"
#import "AddStoreCell.h"


@interface StoreListingViewController ()<UITableViewDelegate , UITableViewDataSource>

@end

@implementation StoreListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)SideButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)AddFavButtonPressed:(id)sender {
   
    CGPoint point =  [sender convertPoint:CGPointZero toView:self.tblAddStores];
    NSIndexPath *indexPath = [self.tblAddStores indexPathForRowAtPoint:point];
    
    AddStoreCell *cell = [_tblAddStores cellForRowAtIndexPath:indexPath];
    cell.btnAddFav.selected = !cell.btnAddFav.selected;
    
    [_tblAddStores reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
   
}


#pragma mark -
#pragma mark UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  15;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"AddStoreCell";
    AddStoreCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [cell.btnAddFav addTarget:self action:@selector(AddFavButtonPressed:) forControlEvents:UIControlEventTouchUpInside];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
    
    
     return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 65;
    
}




@end
