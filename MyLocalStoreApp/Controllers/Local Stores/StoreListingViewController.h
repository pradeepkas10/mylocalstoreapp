//
//  StoreListingViewController.h
//  MyLocalStoreApp
//
//  Created by Surbhi on 14/12/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreListingViewController : UIViewController

- (IBAction)SideButtonPressed:(id)sender;

@property (nonatomic, weak) IBOutlet UITableView *tblAddStores;


@end
