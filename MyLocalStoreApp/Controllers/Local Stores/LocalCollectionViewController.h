//
//  LocalCollectionViewController.h
//  MyLocalStoreApp
//
//  Created by Surbhi on 14/12/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocalCollectionViewController : UIViewController

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@end
