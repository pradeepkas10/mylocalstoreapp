//
//  AddMyLocalStoreViewController.m
//  MyLocalStoreApp
//
//  Created by Surbhi on 14/12/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import "AddMyLocalStoreViewController.h"
#import "StoreListingViewController.h"
#import "AllAndLocalStoreListingViewController.h"
#import "FavStoreListingCell.h"
#import "MyLocalStoreApp.pch"

@interface AddMyLocalStoreViewController () <UITableViewDelegate,UITableViewDataSource>{
    
    NSMutableArray *listingArr;
}
@end

@implementation AddMyLocalStoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"StoreA",@"storeName",@"delhi,west",@"location", nil];
    NSMutableDictionary *dict1 = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"StoreB",@"storeName",@"delhi,west",@"location", nil];
    NSMutableDictionary *dict2 = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"StoreC",@"storeName",@"delhi,west",@"location", nil];

    listingArr = [[NSMutableArray alloc]init];
    [listingArr addObject:dict];
    [listingArr addObject:dict1];
    [listingArr addObject:dict2];
    
    _btnAdd.layer.borderColor = [UIColor grayColor].CGColor;
    _btnAdd.layer.borderWidth = 1.0;
    

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)SideButtonPressed:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
    [self dismissViewControllerAnimated:YES completion:nil];
    });
}

- (IBAction)EditButtonPressed:(id)sender {
    if (_tblFavStore.isEditing){
        [_tblFavStore setEditing:false animated:YES];
        _btnEdit.selected = false;
    }else{
        [_tblFavStore setEditing:true animated:YES];
        _btnEdit.selected = true;
    }
    [_tblFavStore reloadData];

    
}

- (IBAction)AddStoreButtonPressed:(id)sender {
    
    //StoreListingViewController *detailsController = [mainStoryboard instantiateViewControllerWithIdentifier:@"StoreListingViewController"];
    
      AllAndLocalStoreListingViewController *detailsController = [mainStoryboard instantiateViewControllerWithIdentifier:@"AllAndLocalStoreListingViewController"];
    
    [self presentViewController:detailsController animated:NO completion:nil];
    
}

#pragma mark -
#pragma mark ASIHTTPRequest Methods

- (void)loadStores {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        //disabling back button till request not finished
        
        NSString *urlString = [NSString stringWithFormat:@"%@shopoffers/?UserID=%@&CategoryIds=", BASEURL,appDelegate.strUserId];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId,SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        [request setRequestMethod:@"GET"];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request setDidFinishSelector:@selector(requestFinished:)];
        [request setDidFailSelector:@selector(requestFailed:)];
        [request startAsynchronous];
        
    }
    else {
//        [_btnRefresh setHidden:NO];
//        [refreshControl endRefreshing];
//        [refreshActivityView removeFromSuperview];
//        _btnRefresh.hidden = NO;
        
        [appDelegate stopIndicator];
        
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:@"No internet connection." andButtonTitle:@"Cancel"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [appDelegate.locationManager startUpdatingLocation];
    NSDictionary *dicStores = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    
    if ([[dicStores allKeys] containsObject:@"Stores"]) {
        
        NSMutableArray *storeArr = [[NSMutableArray alloc] initWithArray:[dicStores objectForKey:@"Stores"]];
//        [self SortStoresBasisOnDistance:storeArr];
        // Put code here thats at bottom page
    }
    
    
    [appDelegate stopIndicator];
//    [_btnRefresh setHidden:NO];
//    [refreshControl endRefreshing];
//    [refreshActivityView removeFromSuperview];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
//    [_btnRefresh setHidden:NO];
//    [refreshControl endRefreshing];
//    [refreshActivityView removeFromSuperview];
    
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:request.error.localizedDescription andButtonTitle:@"Cancel"];
    
}



#pragma mark -
#pragma mark UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return listingArr.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1 ;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"FavStoreListingCell";
    FavStoreListingCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    //[cell.btnFav addTarget:self action:@selector(AddFavButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSMutableDictionary *dict = listingArr[indexPath.item];
    cell.lblName.text = [dict objectForKey:@"storeName"];
    cell.lblAddress.text = [dict objectForKey:@"location"];
    if (tableView.editing){
        cell.btnFav.hidden = true;
        cell.viewForLeftConstraint.constant = 15;
    }else{
        cell.btnFav.hidden = false;
        cell.viewForLeftConstraint.constant = 55;
    }
    
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    NSMutableDictionary *dict = [listingArr objectAtIndex:sourceIndexPath.item];
    [listingArr removeObjectAtIndex:sourceIndexPath.item];
    [listingArr insertObject:dict atIndex:destinationIndexPath.item];
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [listingArr removeObjectAtIndex:indexPath.item];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}



-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}



@end
