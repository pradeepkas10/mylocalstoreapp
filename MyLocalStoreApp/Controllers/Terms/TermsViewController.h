//
//  TermsViewController.h
//  K kiosk
//
//  Created by Hitesh Dhawan on 10/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyLocalStoreApp.pch"
#import "RegisterViewController.h"

@protocol updateTerms <NSObject>
- (void)updatetermfunction;
@end


@interface TermsViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIWebView *twebView;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property BOOL isloggedin;
@property BOOL isNewMasterConsent;

@property (nonatomic, strong) NSString * MasterConsentsId;


@property (weak, nonatomic) IBOutlet UIView *imgHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;
@property (weak, nonatomic) IBOutlet UILabel *lblComingSoon, *lbl_header;
@property (strong, nonatomic) NSString *str_title;


@property (weak, nonatomic) IBOutlet UIView *actionView;
@property (weak, nonatomic) IBOutlet UIButton *denyButton;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *actionViewHeight;

- (IBAction)DenyButtonPressed:(id)sender;
- (IBAction)AcceptButtonPressed:(id)sender;

@property (nonatomic, weak) id<updateTerms> delegate;


@end
