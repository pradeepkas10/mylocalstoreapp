//
//  TermsViewController.m
//  K kiosk
//
//  Created by Hitesh Dhawan on 10/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "TermsViewController.h"
#import "RegisterViewController.h"
#import "MyLocalStoreApp.pch"

@interface TermsViewController ()
{
    UIActivityIndicatorView *refreshActivityView;
    NSString *urlString;
    NSURLRequest *request;
    NSMutableArray *approvedConsentArray;
    NSMutableArray *revokedConsentArray;
    NSDictionary *userDiction;
    NSString *DateOfBirth;

}
@end

@implementation TermsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [UIApplication sharedApplication].statusBarHidden = YES;
    
    self.denyButton.layer.cornerRadius = self.denyButton.frame.size.height/2;
    self.denyButton.layer.borderWidth = 1;
    self.acceptButton.layer.cornerRadius = self.acceptButton.frame.size.height/2;
    self.acceptButton.layer.borderWidth = 1;
    self.acceptButton.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.twebView.layer.cornerRadius = 7.0;
    self.twebView.clipsToBounds = YES;
    self.twebView.layer.shadowColor = [UIColor whiteColor].CGColor;
    self.twebView.layer.shadowOpacity = 1;
    self.twebView.layer.shadowOffset = CGSizeZero;
    self.twebView.layer.shadowRadius = 2.5;
    self.twebView.layer.masksToBounds = NO;
    [self.activityIndicator stopAnimating];
    [self.activityIndicator setColor:[UIColor grayColor]];
    

    [_menuButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    
    if (self.isloggedin == true){
        
        if (self.isNewMasterConsent == true){
            self.imgHeader.hidden = false;
            self.menuButton.hidden = true;
            self.actionView.hidden = false;
            self.actionViewHeight.constant = 75;
            self.view.backgroundColor = [UIColor.whiteColor colorWithAlphaComponent:1.0];
        }
        else{
            self.imgHeader.hidden = false;
            self.menuButton.hidden = false;

            self.actionView.hidden = true;
            self.actionViewHeight.constant = 0;
            self.view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:1.0];
        }
    }
    else{
        self.imgHeader.hidden = true;
        self.menuButton.hidden = true;
        self.actionView.hidden = false;
        self.actionViewHeight.constant = 75;
        self.view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0.5];
    }

    urlString = @"";
    urlString = [[NSUserDefaults standardUserDefaults] stringForKey:PrivacyPolicy];
    [self.twebView loadHTMLString:urlString baseURL:nil];

    [self.twebView setDelegate:self];

    
    approvedConsentArray = [NSMutableArray array];
    revokedConsentArray = [NSMutableArray array];

    userDiction = [NSDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:DICProfileData]];
    DateOfBirth = userDiction[@"DateOfBirth"];
    
    
    NSArray * arrConsents = [[NSUserDefaults standardUserDefaults] objectForKey:DICConsents];
    
    for (int i=0; i<arrConsents.count; i++)
    {
        NSDictionary * dicConsents = [[NSDictionary alloc ]init];
        dicConsents = arrConsents[i];
        NSLog(@"%@",dicConsents);
        
        if ([[dicConsents objectForKey:@"State"] isEqualToString:@"ConsentGiven" ]) {
            NSString *idstr = [NSString stringWithFormat:@"%@",[[dicConsents objectForKey:@"CurrentVersion" ] objectForKey:@"Id"]];
            [approvedConsentArray addObject:idstr];
        }
        else{
            if ([dicConsents[@"Name"] isEqualToString:@"Master"]) {
                _MasterConsentsId = [NSString stringWithFormat:@"%@",[[dicConsents objectForKey:@"CurrentVersion" ] objectForKey:@"Id"]];
            }
            else{
                NSString *idstr = [NSString stringWithFormat:@"%@",[[dicConsents objectForKey:@"CurrentVersion" ] objectForKey:@"Id"]];
                [revokedConsentArray addObject:idstr];
            }
        }
        if ([dicConsents[@"Name"] isEqualToString:@"Master"]) {
            
            _MasterConsentsId = [NSString stringWithFormat:@"%@",[[dicConsents objectForKey:@"CurrentVersion" ] objectForKey:@"Id"]];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleSideMenuView:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}

-(void)viewDidAppear:(BOOL)animated{
    [self.twebView setDelegate:self];
}


-(void)viewDidDisappear:(BOOL)animated{
//    [self.twebView setDelegate:nil];
    urlString = @"";
    [self.twebView loadHTMLString:urlString baseURL:nil];
}

#pragma mark -
#pragma mark UIBarButtoItem Methods


- (IBAction)refreshButtonPressed:(id)sender
{
    urlString = [[NSUserDefaults standardUserDefaults] stringForKey:PrivacyPolicy];
    [self.twebView loadHTMLString:urlString baseURL:nil];
}

#pragma mark -
#pragma mark UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
    [appDelegate startIndicator];
    _btnRefresh.hidden = YES;
    
    [self.activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [appDelegate stopIndicator];
    int fontSize;

    
    if (self.isloggedin == true){
            fontSize = 250;
    }
    else{
        fontSize = 200;
    }
    
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'", fontSize];
    [_twebView stringByEvaluatingJavaScriptFromString:jsString];
    
    [refreshActivityView removeFromSuperview];
    _btnRefresh.hidden = NO;
    
    [self.activityIndicator stopAnimating];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIColor colorWithRed: 207.0/255.0 green:17.0/255.0 blue:47.0/255.0 alpha:1.0];
    [appDelegate stopIndicator];
    [refreshActivityView removeFromSuperview];
    _btnRefresh.hidden = NO;
    
    [self.activityIndicator stopAnimating];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65 && self.isloggedin == true && self.isNewMasterConsent == false) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (self.isloggedin == false){
        [self dismissViewControllerAnimated:YES completion:^{
            [self.delegate updatetermfunction];
        }];
    }
}

-(BOOL)prefersStatusBarHidden{
    return true;
}



- (IBAction)DenyButtonPressed:(id)sender {
    if (self.isloggedin == true && self.isNewMasterConsent == true) {
        //SENDING TO HOME SCREEN
        [self GoToHomeScreen];
    }
    else{
        [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"accept"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self dismissViewControllerAnimated:YES completion:^{
            [self.delegate updatetermfunction];
        }];
    }
}

- (IBAction)AcceptButtonPressed:(id)sender {
    
    if (self.isloggedin == true && self.isNewMasterConsent == true) {
        
        [approvedConsentArray addObject:self.MasterConsentsId];
        [self NewMasterConsentApproved];
    }
    else{
        [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"accept"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self dismissViewControllerAnimated:YES completion:^{
            [self.delegate updatetermfunction];
        }];
    }
}

#pragma mark - NewConsentApproved
-(void)NewMasterConsentApproved{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        [appDelegate startIndicator];

        NSString *strURL = [NSString stringWithFormat:@"%@user", BASEURL];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];

        //GETTING USER PROFILE
        ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:strURL]];
        [request setRequestMethod:@"PUT"];
        request.name = @"SaveChanges";
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        NSMutableDictionary *paramDic;
        paramDic = [[NSMutableDictionary alloc] initWithDictionary:@{@"UserID":appDelegate.strUserId,@"DateOfBirth":DateOfBirth,}];
        [paramDic setObject:approvedConsentArray forKey:@"ApprovedConsents"];
        [paramDic setObject:revokedConsentArray forKey:@"RevokedConsents"];
        NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];

        NSLog(@"%@",strURL);
        NSLog(@"%@",_dateString);
        NSLog(@"%@",signature);
        NSLog(@"%@",request.requestHeaders);
        NSLog(@"%@",paramDic);

        
        [request setPostBody:[NSMutableData dataWithData:data]];
        [request startAsynchronous];
    }
    else {
        
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"No internet connection." andButtonTitle:@"Cancel"];
        
    }
}


- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSLog(@"%@",request.responseHeaders);
    NSLog(@"%@",request.responseStatusMessage);

    NSDictionary *dicUser = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    [[NSUserDefaults standardUserDefaults]setObject:dicUser forKey:DICProfileData];
    NSArray *dicConsents = [dicUser objectForKey:@"Consents"];
    [[NSUserDefaults standardUserDefaults]setObject:dicConsents forKey:DICConsents];

    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"PreferencesSet"];
    [appDelegate stopIndicator];
//    [self GoToHomeScreen];
    [self GoToPreferenceSelectionScreen];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:request.error.localizedDescription  andButtonTitle:@"Cancel"];
}


#pragma mark - Go To Gome Screen


-(void)GoToHomeScreen{
    HomeVC *home = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeVC"];
    MenuItemsTVC *sidemenu = [MenuItemsTVC sharedInstance];
    
    ENSideMenuNavigationController *naviController = [[ENSideMenuNavigationController alloc] initWithMenuViewController:sidemenu contentViewController:home];
    naviController.navigationBarHidden = YES;
    
    CGFloat sidemenuWide = SCREEN_WIDTH - 60 ;
    [naviController.sideMenu setMenuWidth:sidemenuWide];
    
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"PreferencesSet"];
    [appDelegate.window setRootViewController:naviController];
    
}

-(void)GoToPreferenceSelectionScreen {
    PreferencesViewController *next = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PreferencesViewController"];
    next.isNewMasterConsent = self.isNewMasterConsent;
    [self presentViewController:next animated:true completion:nil];
}

@end
