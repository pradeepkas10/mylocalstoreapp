//
//  KioskWebViewController.m
//  k kiosk
//
//  Created by HiteshDhawan on 03/12/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "KioskWebViewController.h"
#import "MyLocalStoreApp.pch"

@implementation KioskWebViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:_urlString]];
    [_kwebView loadRequest:requestObj];
    [_kwebView setDelegate:self];
    _kwebView.scrollView.bounces = false;
}

//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleDefault;
//}

-(BOOL)prefersStatusBarHidden{
    return YES;
}


- (IBAction)backButtonPressred:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)updateButtons
{
    _bbitemBack.enabled = _kwebView.canGoBack;
    _bbitemForward.enabled = _kwebView.canGoForward;
}


#pragma mark - 
#pragma mark UIWebViewDelegate Methods

-(void)webViewDidStartLoad:(UIWebView *)webView {
    
    [_activityLoader startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [_activityLoader stopAnimating];
    [self updateButtons];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:( NSError *)error {
    
    [_activityLoader stopAnimating];
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {

        [self performSelector:@selector(dismisView) withObject:nil afterDelay:0.2];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

#pragma mark
#pragma mark - cancel Button Pressed:
- (IBAction)CancelButtonPressred:(id)sender;
{

    [self performSelector:@selector(dismisView) withObject:nil afterDelay:0.2];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)dismisView
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OfferCancelled" object:nil];

}


@end
    
