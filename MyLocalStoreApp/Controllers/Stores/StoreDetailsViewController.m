//
//  StoreDetailsViewController.m
//  RConnect
//
//  Created by Hitesh Dhawan on 06/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "StoreDetailsViewController.h"
#import "MyLocalStoreApp.pch"


@interface StoreDetailsViewController ()<GMSMapViewDelegate>
{
    CGFloat _lastContentOffset;
    GMSMarker *markerStore;
    UIRefreshControl *refreshControl;
    UIBarButtonItem *rightButton;
    UIActivityIndicatorView *refreshActivityView;

}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation StoreDetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [UIApplication sharedApplication].statusBarHidden = YES;

    [self loadAverageRating];
    [self setAppearance];
    
}

-(void)viewWillAppear:(BOOL)animated{
//    [FIRAnalytics logEventWithName:kFIREventSelectContent
//                        parameters:@{
//                                     kFIRParameterItemID:[NSString stringWithFormat:@"id-%@",@"STORE DETAIL"],
//                                     kFIRParameterItemName:@"STORE DETAIL",
//                                     kFIRParameterContentType:@""
//                                     }];

}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:YES];
    
    for (ASIHTTPRequest *request in ASIHTTPRequest.sharedQueue.operations) {
        if (![request isCancelled]) {
            [request cancel];
            [request setDelegate:nil];
        }
    }
}

#pragma mark - Open Google Map
-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    
    if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]]) {
        
        NSString *addSre = [NSString stringWithFormat:@"comgooglemaps://?daddr=%@,%@&directionsmode=transit",[self.receivedDictionary valueForKey:@"Latitude"],[self.receivedDictionary valueForKey:@"Longitude"]];//@"comgooglemaps://?center=%f,%f&zoom=12"//daddr
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:addSre]];
    }
    else {
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Map Error" andMessage:@"Google maps is not installed."];
    }
    return  true;
}

//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleDefault;
//}

- (IBAction)slideButtonPressed:(id)sender
{
    self.bttnSlideView.selected = !self.bttnSlideView.isSelected;
    [self slideView];
}

- (void)setAppearance
{
    // Bring Slide Button Up
    [self.viewGoogleMaps bringSubviewToFront:self.bttnSlideView];
    
    // Making Slide Button SemiCircular
    [self.bttnSlideView.layer setCornerRadius:self.bttnSlideView.frame.size.width/2];
    
    // Setting Location co-ordinates for Map View
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake([[self.receivedDictionary valueForKey:@"Latitude"] floatValue], [[self.receivedDictionary valueForKey:@"Longitude"] floatValue]);
    
    // Configuring Maps for Use
    [self.viewGoogleMaps animateToLocation:location];
    [self.viewGoogleMaps animateToZoom:12];
    self.viewGoogleMaps.myLocationEnabled=YES;
    self.viewGoogleMaps.settings.compassButton=YES;
    self.viewGoogleMaps.settings.scrollGestures=YES;
    self.viewGoogleMaps.settings.rotateGestures=YES;
    self.viewGoogleMaps.settings.zoomGestures=YES;
    self.viewGoogleMaps.settings.indoorPicker=YES;
    self.viewGoogleMaps.settings.consumesGesturesInView=YES;
    self.viewGoogleMaps.settings.tiltGestures=YES;
//    self.viewGoogleMaps.settings.accessibilityPerformMagicTap
    self.viewGoogleMaps.delegate = self;
    
    
    // Setting Up Marker View
    markerStore = [[GMSMarker alloc] init];
    markerStore.position = location;
    markerStore.icon = [UIImage imageNamed:@"marker"];
    
    
    // Setting Title To Marker View
    //markerStore.title = [NSString stringWithFormat:@"Location"];
    markerStore.snippet = [NSString stringWithFormat:@"%@",[self.receivedDictionary valueForKey:@"Name"]];
    
    markerStore.map = self.viewGoogleMaps;
    [self.viewGoogleMaps setSelectedMarker:markerStore];
    
    //SET IMAGE
    self.imgBrand.image = [UIImage imageNamed:@"locate"];
    
//    NSString *storeImg = [self.receivedDictionary valueForKey:@"Logo"];
//    if (![storeImg isEqual:[NSNull null]] && storeImg != nil) {
//        [self.imgBrand sd_setImageWithURL:[NSURL URLWithString:storeImg]
//                     placeholderImage:nil];//[UIImage imageNamed:@"logoBig.png"]
//    }
//    else{
//        NSString *storeNameSTR =  [NSString stringWithFormat:@"%@",[self.receivedDictionary valueForKey:@"Name"]];
//
////        self.imgBrand.image = [UIImage imageNamed:@"header_logo"];
//        if ([storeNameSTR containsString:@"F&V"]){
//            self.imgBrand.image = [UIImage imageNamed:@"header3_logo"];
//        }
//        else if ([storeNameSTR containsString:@"MS"]){
//            self.imgBrand.image = [UIImage imageNamed:@"header_logo"];
//        }
//
//        
//    }
    
    //TITLE
    self.lblTitle.text = [self.receivedDictionary valueForKey:@"Name"];
    
    //RATING & TOTAL AVERAGE
    //in load rating method
    
    
    
    //ADDRESS
    NSString *address = [self.receivedDictionary valueForKey:@"Address"];
    NSString *zip = [self.receivedDictionary valueForKey:@"Zip"];
    NSString *city = [self.receivedDictionary valueForKey:@"City"] ;
    NSString *email = [self.receivedDictionary valueForKey:@"Email"] ;
    
    if (![email isEqual:[NSNull null]] && email != nil ) {
        
        
        self.txtEmail.text = email;
    }
    
    NSString *finalAddressStr ;
    if (![address isEqual:[NSNull null]] && address != nil && ![zip isEqual:[NSNull null]] && zip != nil && ![city isEqual:[NSNull null]]  && city != nil) {
        
        finalAddressStr = [NSString stringWithFormat:@"%@%@%@%@%@",address,@", ",zip,@", ",city];
        self.lblAddress.text = finalAddressStr;
    }
    else if (![address isEqual:[NSNull null]] && address != nil && ![zip isEqual:[NSNull null]] && zip != nil ) {
        
        finalAddressStr = [NSString stringWithFormat:@"%@%@%@",address,@", ",zip];
        self.lblAddress.text = finalAddressStr;
    }
    else if (![address isEqual:[NSNull null]] && address != nil && ![city isEqual:[NSNull null]] && city != nil ) {
        
        finalAddressStr = [NSString stringWithFormat:@"%@%@%@",address,@", ",city];
        self.lblAddress.text = finalAddressStr;
    }
    else if (![zip isEqual:[NSNull null]] && zip != nil && ![city isEqual:[NSNull null]] && city != nil ) {
        
        finalAddressStr = [NSString stringWithFormat:@"%@%@%@",zip,@", ",city];
        self.lblAddress.text = finalAddressStr;
    }
    else if (![address isEqual:[NSNull null]] && address != nil  ) {
        
        finalAddressStr = [NSString stringWithFormat:@"%@",address];
        self.lblAddress.text = finalAddressStr;
    }
    
    else if (![zip isEqual:[NSNull null]] && zip != nil  ) {
        
        finalAddressStr = [NSString stringWithFormat:@"%@",zip];
        self.lblAddress.text = finalAddressStr;
    }
    else if (![city isEqual:[NSNull null]] && city != nil ) {
        
        finalAddressStr = [NSString stringWithFormat:@"%@",city];
        self.lblAddress.text = finalAddressStr;
    }

    
    //DISTANCE
    self.lblDistance.text = [NSString stringWithFormat:@"%.2f km", [[self.receivedDictionary objectForKey:@"Distance"] floatValue]];


    
    //CONTACT US
    
    
    //TELEPHONE
    //    self.txtPhoneNumber.attributedText = [[NSAttributedString alloc]initWithString:[self.receivedDictionary objectForKey:@"Telephone"] attributes:@{NSForegroundColorAttributeName : [UIColor blackColor],  NSUnderlineColorAttributeName : REDTHEMECOLOR, NSUnderlineStyleAttributeName :@(NSUnderlineStyleSingle)}];
    
    
    self.txtPhoneNumber.attributedText = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",[self.receivedDictionary objectForKey:@"Telephone"]] attributes:@{NSUnderlineStyleAttributeName :@(NSUnderlineStyleNone)}];
    //NSForegroundColorAttributeName : THEMECOLOR ,
    
    //    NSMutableAttributedString *strPhone = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",[self.receivedDictionary objectForKey:@"Telephone"]] attributes:@{}];
    
    //EMAIL
   //  font = UIFont(name: LatoRegular, size: 15.0) ?? UIFont.systemFontOfSize(15.0)
UIFont *font = [UIFont fontWithName: LatoRegular size:15];
    self.txtEmail.attributedText = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",[self.receivedDictionary objectForKey:@"Email"]] attributes:@{NSUnderlineStyleAttributeName :@(NSUnderlineStyleNone),NSForegroundColorAttributeName:UIColor.lightGrayColor,NSFontAttributeName:font}] ;

    
    
    
    //OPENING HOURS
    
    if ([[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:0] != [NSNull null] && ![[[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:0] isEqualToString:@""]) {
        self.lblTime1.textColor = [UIColor colorWithRed:170./255.0 green:170./255.0 blue:170./255.0 alpha:1];
        self.lblTime1.text = [[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:0];
    }
    if ([[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:1] != [NSNull null] && ![[[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:1] isEqualToString:@""]) {
        self.lblTime2.textColor = [UIColor colorWithRed:170./255.0 green:170./255.0 blue:170./255.0 alpha:1];
        self.lblTime2.text = [[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:1];
    }
    if ([[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:2] != [NSNull null] && ![[[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:2] isEqualToString:@""]) {
        self.lblTime3.textColor = [UIColor colorWithRed:170./255.0 green:170./255.0 blue:170./255.0 alpha:1];
        self.lblTime3.text = [[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:2];
    }
    if ([[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:3] != [NSNull null] && ![[[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:3] isEqualToString:@""]) {
        self.lblTime4.textColor = [UIColor colorWithRed:170./255.0 green:170./255.0 blue:170./255.0 alpha:1];
        self.lblTime4.text = [[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:3];
    }
    if ([[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:4] != [NSNull null] && ![[[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:4] isEqualToString:@""]) {
        self.lblTime5.textColor = [UIColor colorWithRed:170./255.0 green:170./255.0 blue:170./255.0 alpha:1];
        self.lblTime5.text = [[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:4];
    }
    if ([[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:5] != [NSNull null] && ![[[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:5] isEqualToString:@""]) {
        self.lblTime6.textColor = [UIColor colorWithRed:170./255.0 green:170./255.0 blue:170./255.0 alpha:1];
        self.lblTime6.text = [[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:5];
    }
    if ([[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:6] != [NSNull null] && ![[[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:6] isEqualToString:@""]) {
        self.lblTime7.textColor = [UIColor colorWithRed:170./255.0 green:170./255.0 blue:170./255.0 alpha:1];
        self.lblTime7.text = [[self.receivedDictionary valueForKey:@"OpeningHours"] objectAtIndex:6];
    }
    
    
    
    //COMMENTS
    self.lblNote.text = [self.receivedDictionary objectForKey:@"Note"];
    
    //SCROLLVIEW SETUP
    
    CGFloat scrollViewHeight = 0.0f;
    CGFloat scrollViewWidth = 0.0f;
    
    for (UIView *view in _scrollView.subviews) {
        CGFloat height = (view.frame.size.height + view.frame.origin.y);
        scrollViewHeight = ((height > scrollViewHeight) ? height : scrollViewHeight);
        
        CGFloat width = (view.frame.size.width + view.frame.origin.x);
        scrollViewWidth = ((width > scrollViewWidth) ? width : scrollViewWidth);
    }
    
    [_scrollView setContentSize:(CGSizeMake(scrollViewWidth, scrollViewHeight))];
}


#pragma mark -
#pragma mark ConfiguringSlideView Methods

- (void)slideView
{
    CGPoint topOffset = CGPointMake(0,0);
    
    if (self.scrollView.contentOffset.x == topOffset.x && self.scrollView.contentOffset.y == topOffset.y) {
        
        [self.scrollView setContentOffset:CGPointMake(0, self.viewStoreDetails.frame.origin.y +80) animated:YES];
    }
    else {
        
        [self.scrollView setContentOffset:topOffset animated:YES];
    }

}


#pragma mark - 
#pragma mark UIScrollViewDelegate Methdods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    _lastContentOffset = scrollView.contentOffset.y;
}

-(void) viewDidAppear:(BOOL)animated
{
    CGFloat scrollViewHeight = self.viewStore1.frame.size.height + self.viewStore2.frame.size.height;
     CGFloat scrollViewWidth = SCREEN_WIDTH;
    [_scrollView setContentSize:(CGSizeMake(scrollViewWidth, scrollViewHeight))];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if (_lastContentOffset > scrollView.contentOffset.y) {
        self.bttnSlideView.selected = NO;
    }
    else {
        self.bttnSlideView.selected = NO;
    }
}

- (IBAction)backButtonPressed:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark ASIHTTPRequest Methods

- (void)loadAverageRating {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        NSString *pCat = @"20";
        
        NSString *urlString = [NSString stringWithFormat:@"%@ratings/statistics/store?StoreId=%@&CategoryId=%@", BASEURL, [_receivedDictionary valueForKey:@"Id"], pCat];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@", _dateString, [_receivedDictionary valueForKey:@"Id"],pCat, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request startAsynchronous];
    }
    else {
        [_btnRefresh setHidden:NO];
        [refreshControl endRefreshing];
        [refreshActivityView removeFromSuperview];
        _btnRefresh.hidden = NO;

    
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:@"No internet connection." andButtonTitle:@"Cancel"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [SVProgressHUD dismiss];
    NSDictionary *dicRating = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    
    if ([[dicRating objectForKey:@"AvRating"] integerValue] > 0) {
        
        for (int i=100; i<=104; i++) {
            UIImageView *imgView = (UIImageView *)[self.view viewWithTag:i];
            CGFloat rate = [[dicRating objectForKey:@"AvRating"] floatValue];

            NSString *str=[NSString stringWithFormat:@"%f",rate];
            NSArray *arr=[str componentsSeparatedByString:@"."];
            int tempInt=[[arr lastObject] intValue];

            if (i < 100+[[dicRating objectForKey:@"AvRating"] integerValue]) {
                    [imgView setImage:[UIImage imageNamed:@"star_red"]];
            }
            else if (i == 100+[[dicRating objectForKey:@"AvRating"] integerValue]){
                if (tempInt <= 500000 && tempInt != 000000) {
                    [imgView setImage:[UIImage imageNamed:@"star_redHalf"]];
                }
                else if (tempInt == 0){
                    [imgView setImage:[UIImage imageNamed:@"star"]];
                }
                else{
                    [imgView setImage:[UIImage imageNamed:@"star_red"]];
                }
            }
            else {
                [imgView setImage:[UIImage imageNamed:@"star"]];
            }
        }
    }
    else
    {
        for (int i=100; i<=104; i++) {
            UIImageView *imgView = (UIImageView *)[self.view viewWithTag:i];
            [imgView setImage:[UIImage imageNamed:@"star"]];
        }
        
    }
    
    if ([[dicRating objectForKey:@"TotRatings"] integerValue] > 0) {
        _lblTotalAverage.text = [NSString stringWithFormat:@"%ld", [[dicRating objectForKey:@"TotRatings"] integerValue]];
    }
    
    [appDelegate stopIndicator];
    [_btnRefresh setHidden:NO];
    [refreshControl endRefreshing];
    [refreshActivityView removeFromSuperview];

}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    [_btnRefresh setHidden:NO];
    [refreshControl endRefreshing];
    [refreshActivityView removeFromSuperview];

    [SVProgressHUD dismiss];
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:request.error.localizedDescription andButtonTitle:@"Cancel"];

}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
        MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
        mo.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}


#pragma mark -
#pragma mark UIBarButtoItem Methods


- (IBAction)refreshButtonPressed:(id)sender
{
    if (refreshActivityView == nil) {
        refreshActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        refreshActivityView.frame = self.btnRefresh.frame;
    }
    [_btnRefresh setHidden:YES];
    [refreshActivityView startAnimating];
    [refreshActivityView hidesWhenStopped];
    [self.titleView addSubview:refreshActivityView];
    
    [appDelegate startIndicator];
    [self loadAverageRating];
    [self setAppearance];

}

-(BOOL)prefersStatusBarHidden{
    return true;
}


@end
