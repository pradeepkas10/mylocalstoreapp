//
//  StoresViewController.m
//  RConnect
//
//  Created by HiteshDhawan on 27/10/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "StoresViewController.h"
#import "StoreCell.h"
#import "StoreDetailsViewController.h"
#import "MyLocalStoreApp.pch"

@interface StoresViewController () < CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    UIRefreshControl *refreshControl;
    UIBarButtonItem *rightButton;
    UIActivityIndicatorView *refreshActivityView;
    
    CLLocation *currentLocation;
    CLLocationManager *locationManager;
    BOOL islocationUpdated ;
}

@end

@implementation StoresViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    islocationUpdated = false;
    // Do any additional setup after loading the view.
    [UIApplication sharedApplication].statusBarHidden = YES;

    // Get user current location.
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];

    self.searchView.layer.cornerRadius = 8.0;
    self.searchView.clipsToBounds = YES;
    
    [self.navigationController setNavigationBarHidden:YES];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};

    self.tblStores.estimatedRowHeight = 215;
    self.tblStores.rowHeight = UITableViewAutomaticDimension;
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor whiteColor];
    [self.tblStores addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    self.arrStores = [[NSUserDefaults standardUserDefaults] objectForKey:@"Stores"];
    
    [appDelegate startIndicator];
//    [self performSelector:@selector(LoadTableMethod) withObject:nil afterDelay:3.0];

}

- (void)dismissKeyboard
{
    [self.searchTxtField resignFirstResponder];
}


- (IBAction)toggleSideMenuView:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
    [self dismissViewControllerAnimated:YES completion:nil];
    });
}


-(NSMutableArray *)CheckForValidStoresFromArray:(NSMutableArray *)tempStoreArray {

    NSMutableArray *tempSelectedStores = [[NSMutableArray alloc]init];
    
    for (NSString *storeID in self.validStoreArray) {
        for (NSDictionary *dict in tempStoreArray) {
            if ([[dict objectForKey:@"TagIds"] rangeOfString:[NSString stringWithFormat:@"%@",storeID]].location != NSNotFound && [dict objectForKey:@"TagIds"] != nil) {
                
                if ([tempSelectedStores containsObject:dict]){
//                    NSLog(@"Already Exist - TAG ID  %@ -- STORE ID %@",[dict objectForKey:@"TagIds"],[dict objectForKey:@"Id"]);
                }
                else{
//                    [tempSelectedStores addObject:dict];
                    int count = (int)tempSelectedStores.count;
                    [tempSelectedStores insertObject:dict atIndex:count];
                }
            }
        }
    }
    return tempSelectedStores;
}


-(void)LoadTableMethod{
    
    if (self.arrStores.count > 0) {
        
        [appDelegate stopIndicator];
        [self SortStoresBasisOnDistance:self.arrStores];
    }
    else if (appDelegate.arrStores.count > 0) {
        
        [appDelegate stopIndicator];
        self.arrStores = appDelegate.arrStores;
        [self SortStoresBasisOnDistance:self.arrStores];
    }
    else {
        [appDelegate startIndicator];
        [self loadStores];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if ([self.arrStores count] == 0 || [[NSUserDefaults standardUserDefaults] objectForKey:@"Stores"] == nil) {
        
        [appDelegate startIndicator];
        [self loadStores];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark ASIHTTPRequest Methods

- (void)loadStores {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        //disabling back button till request not finished
        
        NSString *urlString = [NSString stringWithFormat:@"%@Stores", BASEURL];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@", _dateString, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        [request setRequestMethod:@"GET"];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request setDidFinishSelector:@selector(requestFinished:)];
        [request setDidFailSelector:@selector(requestFailed:)];
        [request startAsynchronous];

    }
    else {
        [_btnRefresh setHidden:NO];
        [refreshControl endRefreshing];
        [refreshActivityView removeFromSuperview];
        _btnRefresh.hidden = NO;
        
        [appDelegate stopIndicator];

        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:@"No internet connection." andButtonTitle:@"Cancel"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [appDelegate.locationManager startUpdatingLocation];
    NSDictionary *dicStores = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    
    if ([[dicStores allKeys] containsObject:@"Stores"]) {

        NSMutableArray *storeArr = [[NSMutableArray alloc] initWithArray:[dicStores objectForKey:@"Stores"]];
        [self SortStoresBasisOnDistance:storeArr];
        // Put code here thats at bottom page
    }
    
    
    [appDelegate stopIndicator];
    [_btnRefresh setHidden:NO];
    [refreshControl endRefreshing];
    [refreshActivityView removeFromSuperview];
}

-(void)SortStoresBasisOnDistance:(NSArray *)dicStores{
    
    NSMutableArray *storeArray = [[NSMutableArray alloc] init];
    NSMutableArray *storeArraySelected = [[NSMutableArray alloc] init];

    if (self.isComingfromMenu == NO && self.validStoreArray.count > 0){
        NSMutableArray *temArray = [self CheckForValidStoresFromArray:[NSMutableArray arrayWithArray:self.arrStores]];
        storeArraySelected = [self returnSortedArry:temArray];
        
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"Distance" ascending:YES];
        NSArray *selectedArr = [[NSArray alloc] initWithArray:[storeArraySelected mutableCopy]];
        selectedArr = [selectedArr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor1]];
        self.arr_SearchStores = [[NSMutableArray alloc] initWithArray:selectedArr];
    }

    
    if (currentLocation != nil) {
        
        storeArray = [self returnSortedArry:[NSMutableArray arrayWithArray:dicStores]];
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Distance" ascending:YES];
        self.arrStores = [[NSArray alloc] initWithArray:[storeArray mutableCopy]];
        self.arrStores = [self.arrStores sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        
        // Save data to prefrences
        [[NSUserDefaults standardUserDefaults] setObject:self.arrStores forKey:@"Stores"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else{
        self.arrStores = [[NSArray alloc] initWithArray:dicStores];
    }
    
    if (self.isComingfromMenu == YES && !(self.validStoreArray.count > 0)){
        self.arr_SearchStores = [[NSMutableArray alloc] initWithArray:self.arrStores];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tblStores reloadData];
    });
}

-(NSMutableArray *)returnSortedArry:(NSMutableArray *)dicStores{
    
    NSMutableArray *storeArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dic in dicStores) {
        
        @autoreleasepool {
            NSMutableDictionary *dicObj = [[NSMutableDictionary alloc] initWithDictionary:dic];
            
            CLLocation *storeLocation = [[CLLocation alloc] initWithLatitude:[[dicObj objectForKey:@"Latitude"] floatValue] longitude:[[dicObj objectForKey:@"Longitude"] floatValue]];
            NSNumber *temp = [NSNumber numberWithFloat:[[self kilometersFromPlace:currentLocation.coordinate andToPlace:storeLocation.coordinate]floatValue]];
            [dicObj setObject:temp forKey:@"Distance"];
            
            // geofencing
            CLCircularRegion *circle = [[CLCircularRegion alloc] initWithCenter:[storeLocation coordinate] radius:100.0 identifier:dicObj[@"Name"]];
            
            circle.notifyOnEntry = YES;
            circle.notifyOnExit = YES;
            
            
            
            if (appDelegate.latestGeofences == nil) {
                appDelegate.latestGeofences = [NSMutableArray array];
            }
            [appDelegate.latestGeofences addObject:circle];
            
            
            NSMutableArray *openArr = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"OpeningHours"]];;
            for (int i = 0; i < openArr.count; i++) {
                
                if (openArr[i]  == [NSNull null]) {
                    [openArr replaceObjectAtIndex:i withObject:@""];
                }
            }
            
            [dicObj setObject:openArr forKey:@"OpeningHours"];
            [storeArray addObject:dicObj];
        }
    }
    return  storeArray;
}



- (void)requestFailed:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    [_btnRefresh setHidden:NO];
    [refreshControl endRefreshing];
    [refreshActivityView removeFromSuperview];
    
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:request.error.localizedDescription andButtonTitle:@"Cancel"];

}


#pragma mark -
#pragma mark UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [self.arr_SearchStores count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"StoreCell";
    
    StoreCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.viewContent.layer.cornerRadius = 8.0;
    cell.viewContent.clipsToBounds = YES;
    
    cell.viewContent.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    cell.viewContent.layer.shadowOpacity = 1;
    cell.viewContent.layer.shadowOffset = CGSizeZero;
    cell.viewContent.layer.shadowRadius = 2.0;
    cell.viewContent.layer.masksToBounds = NO;
    
    
    NSString *storeNameSTR =  [[self.arr_SearchStores objectAtIndex:indexPath.row] objectForKey:@"Name"];
    if (![storeNameSTR isEqual:[NSNull null]]) {
        cell.lblName.text = storeNameSTR;
       // cell.storeName.text = storeNameSTR;
    }
    
    cell.storeImage.image = [UIImage imageNamed:@"locate"];

    NSString *shortName = [[self.arr_SearchStores objectAtIndex:indexPath.row] objectForKey:@"ShortName"];
    if (![shortName isEqual:[NSNull null]]) {
        cell.lblAddress.text = shortName;
    }
    
    NSString *address = [[self.arr_SearchStores objectAtIndex:indexPath.row] objectForKey:@"Address"];
    NSString *zip = [[self.arr_SearchStores objectAtIndex:indexPath.row] objectForKey:@"Zip"];
    NSString *city = [[self.arr_SearchStores objectAtIndex:indexPath.row] objectForKey:@"City"];
    NSString *finalAddressStr ;
     if (![address isEqual:[NSNull null]] && address != nil && ![zip isEqual:[NSNull null]] && zip != nil && ![city isEqual:[NSNull null]]  && city != nil) {

         finalAddressStr = [NSString stringWithFormat:@"%@%@%@%@%@",address,@", ",zip,@", ",city];
         cell.storeAddressLbl.text = finalAddressStr;
     }
     else if (![address isEqual:[NSNull null]] && address != nil && ![zip isEqual:[NSNull null]] && zip != nil ) {
         
         finalAddressStr = [NSString stringWithFormat:@"%@%@%@",address,@", ",zip];
         cell.storeAddressLbl.text = finalAddressStr;
     }
     else if (![address isEqual:[NSNull null]] && address != nil && ![city isEqual:[NSNull null]] && city != nil ) {
         
         finalAddressStr = [NSString stringWithFormat:@"%@%@%@",address,@", ",city];
         cell.storeAddressLbl.text = finalAddressStr;
     }
     else if (![zip isEqual:[NSNull null]] && zip != nil && ![city isEqual:[NSNull null]] && city != nil ) {
         
         finalAddressStr = [NSString stringWithFormat:@"%@%@%@",zip,@", ",city];
         cell.storeAddressLbl.text = finalAddressStr;
     }
     else if (![address isEqual:[NSNull null]] && address != nil  ) {
         
         finalAddressStr = [NSString stringWithFormat:@"%@",address];
         cell.storeAddressLbl.text = finalAddressStr;
     }

     else if (![zip isEqual:[NSNull null]] && zip != nil  ) {
         
         finalAddressStr = [NSString stringWithFormat:@"%@",zip];
         cell.storeAddressLbl.text = finalAddressStr;
     }
     else if (![city isEqual:[NSNull null]] && city != nil ) {
         
         finalAddressStr = [NSString stringWithFormat:@"%@",city];
         cell.storeAddressLbl.text = finalAddressStr;
     }


    NSString *distance = [NSString stringWithFormat:@"%.2f km", [[[self.arr_SearchStores objectAtIndex:indexPath.row] objectForKey:@"Distance"] floatValue]];
    if (![distance isEqual:[NSNull null]]) {
            cell.storeDistance.text = distance;
    }
    
    NSString *state = [[self.arr_SearchStores objectAtIndex:indexPath.row] objectForKey:@"CurrentState"];
    if (![state isEqual:[NSNull null]]) {
        if ([state isEqualToString:@"Open"])
        {
          cell.storeState.text = @"Open now";
            cell.storeState.textColor = [UIColor colorWithRed:3.0/255.0 green:137.0/255.0 blue:8.0/255.0 alpha:1];
        }
        else
        {
            cell.storeState.text = @"Closed now";
            cell.storeState.textColor = REDTHEMECOLOR;
        }
        //cell.storeState.text = state;
    }
    
    NSString *phone = [[self.arr_SearchStores objectAtIndex:indexPath.row] objectForKey:@"Telephone"];
    if (![state isEqual:[NSNull null]]) {
        cell.storePhone.text = phone;
    }
    tableView.estimatedRowHeight = 214;
    tableView.rowHeight = UITableViewAutomaticDimension;

    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    StoreDetailsViewController *detailsController = [mainStoryboard instantiateViewControllerWithIdentifier:@"StoreDetailsViewController"];
    detailsController.receivedDictionary = [self.arr_SearchStores objectAtIndex:indexPath.row];
    
    [self presentViewController:detailsController animated:NO completion:nil];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 215.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}


#pragma mark -
#pragma mark CLLocationManagerDelegate Methods

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    islocationUpdated = true;
    currentLocation = [locations lastObject];
    if (currentLocation != nil) {
        [locationManager stopUpdatingLocation];
    }
    
    [self LoadTableMethod];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if (islocationUpdated == false){
        currentLocation = [[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(28.587390, 77.310646) altitude:-1 horizontalAccuracy:kCLLocationAccuracyBest verticalAccuracy:kCLLocationAccuracyBest timestamp:[NSDate date]];
        if (currentLocation != nil) {
            [locationManager stopUpdatingLocation];
        }

        [self LoadTableMethod];
    }

}

-(void)get_SortedSearchArrayForText:(NSString *)str{
    
    if ([str isEqualToString:@""]) {
        self.arr_SearchStores = [[NSMutableArray alloc] initWithArray:self.arrStores];
        [self.tblStores reloadData];
    }
    else{
        if (self.isComingfromMenu == NO && self.validStoreArray.count > 0){
            NSMutableArray *temArray = [self CheckForValidStoresFromArray:[NSMutableArray arrayWithArray:self.arrStores]];
            self.arr_SearchStores = [NSMutableArray array];
            
            for (int i = 0; i < temArray.count; i++) {
                NSDictionary *dc = [temArray objectAtIndex:i];
                NSString *addStore = dc[@"Address"];
                NSString *nameStore = dc[@"Name"];
                
                NSRange range = [addStore  rangeOfString: str options: NSCaseInsensitiveSearch];
                NSRange range2 = [nameStore  rangeOfString: str options: NSCaseInsensitiveSearch];
                
                if ((range.location != NSNotFound) || (range2.location != NSNotFound)) {
                    [self.arr_SearchStores addObject:dc];
                }
            }
        }
        else{
            self.arr_SearchStores = [NSMutableArray array];
            
            for (int i = 0; i < self.arrStores.count; i++) {
                NSDictionary *dc = [self.arrStores objectAtIndex:i];
                NSString *addStore = dc[@"Address"];
                NSString *nameStore = dc[@"Name"];
                
                NSRange range = [addStore  rangeOfString: str options: NSCaseInsensitiveSearch];
                NSRange range2 = [nameStore  rangeOfString: str options: NSCaseInsensitiveSearch];
                
                if ((range.location != NSNotFound) || (range2.location != NSNotFound)) {
                    [self.arr_SearchStores addObject:dc];
                }
            }
        }
        
        [self.tblStores reloadData];
    }
    
    
}


#pragma mark -
#pragma mark UIBarButtoItem Methods


- (IBAction)refreshButtonPressed:(id)sender
{
    if (refreshActivityView == nil) {
        refreshActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        refreshActivityView.frame = self.btnRefresh.frame;
    }
    [_btnRefresh setHidden:YES];
    [refreshActivityView startAnimating];
    [refreshActivityView hidesWhenStopped];
    [self.titleView addSubview:refreshActivityView];
    
    _searchTxtField.text = @"";
    
    [appDelegate startIndicator];
    [self loadStores];
}


- (void)refreshTable {
    
    [appDelegate startIndicator];
    [self loadStores];
}



- (NSString *)kilometersFromPlace:(CLLocationCoordinate2D)from andToPlace:(CLLocationCoordinate2D)to {
    
    CLLocation *userLoc = [[CLLocation alloc] initWithLatitude:from.latitude longitude:from.longitude];
    //CLLocation *userTestingLoc = [[CLLocation alloc]initWithLatitude:59.914803 longitude:10.637692];
    CLLocation *storeLoc = [[CLLocation alloc] initWithLatitude:to.latitude longitude:to.longitude];
    
    CLLocationDistance distance = [userLoc distanceFromLocation:storeLoc]/1000;
    
    return [NSString stringWithFormat:@"%1f", distance];
}

- (NSString *)checkCurrentStateWithStoreTime:(NSString *)storeTime andDateFormatter:(NSDateFormatter *)dateFormatter andCurrentTime:(NSString *)timeNow
{
    NSArray *times = [storeTime componentsSeparatedByString:@"-"];
    NSString *strOpeningTime = [times objectAtIndex:0];
    NSString *strClosingTime = [times objectAtIndex:1];
    
    NSDate *openingTime = [dateFormatter dateFromString:strOpeningTime];
    NSDate *clossingTime = [dateFormatter dateFromString:strClosingTime];
    NSDate *currentTime = [dateFormatter dateFromString:timeNow];
    
    NSComparisonResult openingResult = [currentTime compare:openingTime];
    NSComparisonResult clossingResult = [currentTime compare:clossingTime];
    if (openingResult == NSOrderedSame || clossingResult == NSOrderedSame) {
        return @"Open";
    }
    else if (openingResult == NSOrderedAscending || clossingResult == NSOrderedAscending) {
        return @"Open";
    }
    else {
        return @"Closed";
    }
}

#pragma mark - TextField Delegates

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(get_SortedSearchArrayForText:) object:nil];
    NSString *textAfterUpdate = self.searchTxtField.text;
    textAfterUpdate = [textAfterUpdate stringByReplacingCharactersInRange:range withString:string];
    [self performSelector:@selector(get_SortedSearchArrayForText:) withObject:textAfterUpdate afterDelay:0.5];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self dismissKeyboard];
    return true;
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
    if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(BOOL)prefersStatusBarHidden{
    return true;
}

@end

