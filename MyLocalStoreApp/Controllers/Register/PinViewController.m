//
//  PinViewController.m
//  TwentyFourSeven
//
//  Created by Akhilesh on 9/26/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "PinViewController.h"
#import "MyLocalStoreApp.pch"




@implementation PinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //[self setLayoutView];
    // Dismiss keyboard gesture
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    NSArray * arrConsents = [[NSUserDefaults standardUserDefaults] objectForKey:DICConsents];
    for (int i=0; i<arrConsents.count; i++)
    {
        NSDictionary * dicConsents = [[NSDictionary alloc ]init];
        dicConsents = arrConsents[i];
        NSDictionary * dic = [arrConsents[i]objectForKey:@"CurrentVersion"];
        if ([dicConsents[@"Name"] isEqualToString:@"Master"]) {

            _MasterConsentsId = [NSString stringWithFormat:@"%@",dic[@"Id"]];
        }
    }

}

-(void)setLayoutView
{
    _pinView.layer.borderColor = THEMECOLOR.CGColor;
    _pinView.layer.borderWidth = 1;
    _pinView.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self setLayoutView];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _txtPin)
    {
        if ([_txtPin.text  isEqualToString: @""])
        {
            _pinView.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:204.0/255.0 blue:196.0/255.0 alpha:1];
            _pinView.layer.borderWidth = 0;
        }
        else
        {
            _pinView.backgroundColor = [UIColor whiteColor];
        }
        
    }
}

- (IBAction)pinTextFieldSelected:(id)sender {
    
    _viewPin.layer.borderColor = THEMECOLOR.CGColor;
    _viewPin.layer.borderWidth = 1;
    _viewPin.backgroundColor = UIColorFromRGB(0xFFFFFF);
    
    _txtPin.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter PIN" attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
}


#pragma mark - 
#pragma mark 

- (void)registerUser
{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        [appDelegate startIndicator];
        NSString *urlString = [NSString stringWithFormat:@"%@user", BASEURL];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@", _dateString, appDelegate.strUserId, _txtPin.text, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:appDelegate.strUserId, @"UserID", _txtPin.text, @"PIN", [_dicUserInfo objectForKey:@"UserName"], @"Name", nil];
        if ([[_dicUserInfo allKeys] containsObject:@"Gender"]) {
            [paramDic setObject:[_dicUserInfo objectForKey:@"Gender"] forKey:@"Gender"];
        }
        if ([[_dicUserInfo allKeys] containsObject:@"DateOfBirth"]) {
            [paramDic setObject:[_dicUserInfo objectForKey:@"DateOfBirth"] forKey:@"DateOfBirth"];
        }
        
        [paramDic setObject:@" " forKey:@"City"];
        [paramDic setObject:@" " forKey:@"Address"];
        [paramDic setObject:_MasterConsentsId forKey:@"ApprovedConsents"];
        NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
        [request setPostBody:[NSMutableData dataWithData:data]];
        
        [request startAsynchronous];
        
    }
    else {
        
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"No internet connection." andButtonTitle:@"Cancel"];

    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{

    [appDelegate stopIndicator];
    NSDictionary *dicResponse = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];    
    
    if ([[dicResponse allKeys] containsObject:@"UserId"])
    {
        appDelegate.strUserId = [dicResponse objectForKey:@"UserId"];
        [[NSUserDefaults standardUserDefaults] setObject:dicResponse forKey:DICProfileData];
        [[NSUserDefaults standardUserDefaults] setObject:[dicResponse objectForKey:@"Consents"] forKey:DICConsents];
        [[NSUserDefaults standardUserDefaults] setObject:[_dicUserInfo objectForKey:@"UserName"] forKey:@"UserName"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"UserAuthenticated"];
        [[NSUserDefaults standardUserDefaults] setObject:[dicResponse objectForKey:@"UserId"] forKey:@"UserId"];
        [[NSUserDefaults standardUserDefaults] setObject:[_dicUserInfo objectForKey:@"MSN"] forKey:@"MSN"];
        
        if (![[_dicUserInfo objectForKey:@"DateOfBirth"] isEqualToString:@""] && [_dicUserInfo objectForKey:@"DateOfBirth"] != nil) {
            [[NSUserDefaults standardUserDefaults] setObject:[_dicUserInfo objectForKey:@"DateOfBirth"] forKey:@"DateOfBirth"];
        }
        else{
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"DateOfBirth"];
        }
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        appDelegate.justLogedIn = YES;
        
        NSMutableArray *consentArray = [NSMutableArray arrayWithArray:[dicResponse objectForKey:@"Consents"]];
        BOOL isMasterConsentGven =  false;
        for (NSDictionary *dict in consentArray) {
            if([dict[@"Name"] isEqualToString: @"Master"]){
                if ([dict[@"State"] isEqualToString:@"ConsentGiven"]) {
                    isMasterConsentGven = true;
                }
            }
        }
        
        [self GoToPreferenceSelectionScreen];
    }
    else{
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:[[dicResponse objectForKey:@"ResponseStatus"] valueForKey:@"Message"]  andButtonTitle:@"Cancel"];

    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:request.error.localizedDescription  andButtonTitle:@"Cancel"];
}


#pragma mark - Decide Next SCreen
-(void)GoToAppScreen {
    HomeVC *home = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeVC"];
    MenuItemsTVC *sidemenu = [MenuItemsTVC sharedInstance];
    
    ENSideMenuNavigationController *naviController = [[ENSideMenuNavigationController alloc] initWithMenuViewController:sidemenu contentViewController:home];
    naviController.navigationBarHidden = YES;

    CGFloat sidemenuWide = SCREEN_WIDTH - 60 ;
    [naviController.sideMenu setMenuWidth:sidemenuWide];
    
    [appDelegate.window setRootViewController:naviController];
}

-(void)GoToPreferenceSelectionScreen {
    PreferencesViewController *next = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PreferencesViewController"];
    next.isNewMasterConsent = false;
    [self presentViewController:next animated:true completion:nil];
}

#pragma mark -
#pragma mark Custom Methods

- (void)dismissKeyboard
{
    [self.txtPin resignFirstResponder];
}


- (IBAction)backButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)submitButtonPressed:(id)sender
{
    if (self.txtPin.text.length > 0)
    {
        [self dismissKeyboard];
        [self registerUser];
    }
    else {
        
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"Please enter pin." andButtonTitle:@"Cancel"];
    }
}

-(BOOL)prefersStatusBarHidden{
    return true;
}

@end
