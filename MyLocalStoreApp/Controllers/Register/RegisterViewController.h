//
//  RegisterViewController.h
//  TwentyFourSeven
//
//  Created by Akhilesh on 9/26/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyLocalStoreApp.pch"
#import <WebKit/WebKit.h>


@interface RegisterViewController : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate,UIWebViewDelegate>

@property (nonatomic, strong) NSArray *arrCountryCode;
@property (weak, nonatomic) IBOutlet UISwitch *switch_terms;
@property (weak, nonatomic) IBOutlet UIImageView * bg_image;
@property (weak, nonatomic) IBOutlet UIView *view_terms;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneCode;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtDateOfBirth;
@property (weak, nonatomic) IBOutlet UITextField *txtGender;


@property (weak, nonatomic) IBOutlet UIButton *btnCheckMark;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblTerms;

@property (weak, nonatomic) IBOutlet UILabel * mobileLabel;
@property (weak, nonatomic) IBOutlet UILabel * nameLabel;
@property (weak, nonatomic) IBOutlet UILabel * dobLabel;
@property (weak, nonatomic) IBOutlet UILabel * dobMinimumAgeLabel;
@property (weak, nonatomic) IBOutlet UILabel * genderLabel;
@property (weak, nonatomic) IBOutlet UILabel * termsLabel;

@property (weak, nonatomic) IBOutlet UILabel * mobileLine;
@property (weak, nonatomic) IBOutlet UILabel * nameLine;
@property (weak, nonatomic) IBOutlet UILabel * dobLine;
@property (weak, nonatomic) IBOutlet UILabel * genderLine;


@property (weak, nonatomic) IBOutlet UIButton * Btn_Register;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mobileLineHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLineHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dobLineHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *genderLineHeight;




- (IBAction)registerButtonPressed:(id)sender;
- (IBAction)checkMarkButtonPressed:(id)sender;




@end
