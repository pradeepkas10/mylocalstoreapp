//
//  RegisterViewController.m
//  TwentyFourSeven
//
//  Created by Akhilesh on 9/26/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "RegisterViewController.h"
#import "UITextField+Padding.h"
#import "DateTimePicker.h"
#import "Reachability.h"
#import "SVProgressHUD.h"
#import "ASIHTTPRequest.h"
#import "PinViewController.h"
#import "SSKeychain.h"
#import "UIDevice+NTNUExtensions.h"
#import "HelperClass.h"
#import "NSString+PMUtils.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "TermsViewController.h"
#import "constant.h"
#import "MyLocalStoreApp.pch"

#define MyDateTimePickerHeight 260

@interface RegisterViewController () <TTTAttributedLabelDelegate, updateTerms>
{
    DateTimePicker *datePicker;
    UIPickerView *countryPicker;
    UIPickerView * genderPicker;
    UIToolbar *toolBar;
    int minimumAge;
    
}
@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    minimumAge = [[[NSUserDefaults standardUserDefaults] valueForKey:MINAge] intValue];
    
    [_switch_terms addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.navigationController setNavigationBarHidden:YES];
   // [self.switch_terms setOn:false];
    self.nameLabel.hidden = YES;
    self.mobileLabel.hidden = YES;
    self.genderLabel.hidden = YES;
    self.dobLabel.hidden = YES;
    self.view_terms.hidden = YES;
    self.Btn_Register.enabled = NO;
    self.Btn_Register.backgroundColor = [UIColor lightGrayColor];
    self.txtMobileNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:nil];
    self.txtDateOfBirth.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Date of Birth" attributes:nil];
    self.txtGender.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Gender (Optional)" attributes:nil];
    self.txtName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name" attributes:nil];

    [HelperClass setImageOntextField:self.txtDateOfBirth andimage:@"dobCalender" andMode:NO andframe:CGRectMake(-20, 0, 20, 20)];
    [HelperClass setImageOntextField:self.txtMobileNumber andimage:@"mobileblack" andMode:NO andframe:CGRectMake(-20, 0, 20, 20)];
    [HelperClass setImageOntextField:self.txtGender andimage:@"genderArrowIcon" andMode:NO andframe:CGRectMake(-20, 10, 20, 20)];
    
    // Create custom date picker
    datePicker = [[DateTimePicker alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, MyDateTimePickerHeight)];
    [datePicker addTargetForDoneButton:self action:@selector(donePressed)];
    [datePicker addTargetForCancelButton:self action:@selector(cancelPressed)];
    [self.view addSubview:datePicker];
    datePicker.hidden = NO;
    [datePicker setMode:UIDatePickerModeDate];
   
    NSDate *currentDate = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear: -minimumAge];
    NSDate *tempDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    [datePicker setDate:tempDate];
    
    [datePicker.picker addTarget:self action:@selector(pickerChanged:) forControlEvents:UIControlEventValueChanged];
    
    // Fetch list of phone code
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"CountryPhoneCode" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    self.arrCountryCode = [[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil] objectForKey:@"countries"];
    
    // Dismiss keyboard gesture
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    
    if ([appDelegate.dobStringMinimumAge isEqualToString:@""]) {
        self.dobMinimumAgeLabel.text = @"";
    }
    else{
        self.dobMinimumAgeLabel.text = appDelegate.dobStringMinimumAge;
    }
    
    
    if ([appDelegate.registrationTitle isEqualToString:@""]) {
        self.lblTerms.text = @"I accept the T&C and privacy rules";
    }
    else{
        self.lblTerms.text = appDelegate.registrationTitle;
    }
    
}

-(void)updatetermfunction
{
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"accept"] isEqualToString:@"1"])
    {
        self.Btn_Register.enabled = true;
        self.Btn_Register.backgroundColor = THEMECOLOR;
        [self.switch_terms setOn:true];
    }
    else
    {
        self.Btn_Register.enabled = false;
        self.Btn_Register.backgroundColor = [UIColor lightGrayColor];
        [self.switch_terms setOn:false];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"will appear");
}


-(BOOL)prefersStatusBarHidden{
    return true;
}

#pragma mark - Switch Value for Terms and Privacy

-(void)switchValueChanged:(UISwitch *)sender
{

    if (sender.isOn == 1)
    {
        TermsViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"termsController"];
        viewController.isloggedin = false;
        viewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
        viewController.delegate = self;
        [self presentViewController:viewController animated:true completion:nil];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"accept"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.Btn_Register.enabled = false;
        self.Btn_Register.backgroundColor = [UIColor lightGrayColor];
        [self.switch_terms setOn:false];
    }
}

#pragma mark - DateTimePickerDelegate Methods

- (void)pickerChanged:(id)sender {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    NSDate *currentDate = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear: -minimumAge];
    NSDate *tempDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    
    if ([datePicker.picker.date compare:tempDate] == NSOrderedDescending){
        [datePicker setDate:tempDate];
        [self.txtDateOfBirth setText:[dateFormatter stringFromDate:tempDate]];
    }
    else{
        [self.txtDateOfBirth setText:[dateFormatter stringFromDate:datePicker.picker.date]];
    }
}

- (void)donePressed {
    
    [UIView animateWithDuration:.5 animations:^{
        [datePicker setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, MyDateTimePickerHeight)];
    } completion:^(BOOL finished) {
        datePicker.hidden = YES;
    }];

    [UIView animateWithDuration:1.0 animations:^{
        self.dobLabel.hidden = NO;
        self.txtDateOfBirth.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Date of Birth" attributes:nil];
        [HelperClass activatetextField:self.dobLine height:self.dobLineHeight];
        
    } completion:^(BOOL finished) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.dobLabel.hidden = NO;
            [self.view bringSubviewToFront:self.dobLabel];
            self.txtDateOfBirth.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Date of Birth" attributes:nil];
            [HelperClass DeactivatetextField:self.dobLine height:self.dobLineHeight];
            
            if ([self.txtDateOfBirth.text isEqualToString: @"" ] || self.txtDateOfBirth.text == nil) {
                self.dobLabel.hidden = YES;
            }
            else {
                self.dobLabel.hidden = NO;
            }
            
        });
    }];
    
}

- (void)cancelPressed {

    [UIView animateWithDuration:.5 animations:^{
        [datePicker setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, MyDateTimePickerHeight)];
    } completion:^(BOOL finished) {
        datePicker.hidden = YES;
    }];
    
    [UIView animateWithDuration:1.0 animations:^{
        self.dobLabel.hidden = NO;
        self.txtDateOfBirth.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Date of Birth" attributes:nil];
        [HelperClass activatetextField:self.dobLine height:self.dobLineHeight];
        
    } completion:^(BOOL finished) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.dobLabel.hidden = NO;
            [self.view bringSubviewToFront:self.dobLabel];
            self.txtDateOfBirth.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Date of Birth" attributes:nil];
            [HelperClass DeactivatetextField:self.dobLine height:self.dobLineHeight];
            
            if ([self.txtDateOfBirth.text isEqualToString: @"" ] || self.txtDateOfBirth.text == nil) {
                self.dobLabel.hidden = YES;
            }
            else {
                self.dobLabel.hidden = NO;
            }
            
        });
    }];
}

-(void)newMethod:(UITextField *)textFld
{
    [self addCountryPickerView];
    [countryPicker reloadAllComponents];
    [countryPicker selectRow:[self getIndexOfValue:textFld.text presentInArray:self.arrCountryCode forKey:@"code"] inComponent:0 animated:NO];
}

-(void)showPicker{
    [self.view bringSubviewToFront:datePicker];
    [UIView animateWithDuration:0.2 animations:^{
        [datePicker setFrame:CGRectMake(0, self.view.frame.size.height - MyDateTimePickerHeight, self.view.frame.size.width, MyDateTimePickerHeight)];
    }];
}

-(void)hidePicker{
    [UIView animateWithDuration:0.2 animations:^{
        [datePicker setFrame:CGRectMake(datePicker.frame.origin.x, datePicker.frame.origin.y+MyDateTimePickerHeight , self.view.frame.size.width, MyDateTimePickerHeight)];
    }];
}
#pragma mark - UITextFieldDelegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    
    if ([textField isEqual:_txtPhoneCode]) {
        [self dismissKeyboard];

        [UIView animateWithDuration:1.0 animations:^{
            self.mobileLabel.hidden = NO;
            self.txtMobileNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:nil];
            [HelperClass activatetextField:_mobileLine height:_mobileLineHeight];

        } completion:^(BOOL finished) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.mobileLabel.hidden = NO;
                [self.view bringSubviewToFront:self.mobileLabel];
                self.txtMobileNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:nil];
                [HelperClass activatetextField:_mobileLine height:_mobileLineHeight];
            });
        }];
    }
    else if (textField == _txtMobileNumber)
    {
        [UIView animateWithDuration:1.0 animations:^{
            self.mobileLabel.hidden = NO;
            self.txtMobileNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:nil];
            [HelperClass activatetextField:self.mobileLine height:self.mobileLineHeight];
            
        } completion:^(BOOL finished) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.mobileLabel.hidden = NO;
                [self.view bringSubviewToFront:self.mobileLabel];
                self.txtMobileNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:nil];
                [HelperClass activatetextField:self.mobileLine height:self.mobileLineHeight];
            });
        }];

    }
    else if (textField == _txtName)
    {
        [UIView animateWithDuration:1.0 animations:^{
            self.nameLabel.hidden = NO;
            self.txtName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name" attributes:nil];
            [HelperClass activatetextField:self.nameLine height:self.nameLineHeight];
            
        } completion:^(BOOL finished) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.nameLabel.hidden = NO;
                [self.view bringSubviewToFront:self.nameLabel];
                self.txtName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name" attributes:nil];
                [HelperClass activatetextField:self.nameLine height:self.nameLineHeight];
            });
        }];

    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _txtMobileNumber)
    {
        [UIView animateWithDuration:1.0 animations:^{
            self.mobileLabel.hidden = NO;
            self.txtMobileNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:nil];
            [HelperClass activatetextField:self.mobileLine height:self.mobileLineHeight];
            
        } completion:^(BOOL finished) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.mobileLabel.hidden = NO;
                [self.view bringSubviewToFront:self.mobileLabel];
                self.txtMobileNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:nil];
                [HelperClass DeactivatetextField:self.mobileLine height:self.mobileLineHeight];
                
                if ([self.txtMobileNumber.text isEqualToString: @"" ] || self.txtMobileNumber.text == nil) {
                    self.mobileLabel.hidden = YES;
                }
                else {
                    self.mobileLabel.hidden = NO;
                }

            });
        }];
    }
    else if (textField == _txtName)
    {
        [UIView animateWithDuration:1.0 animations:^{
            self.nameLabel.hidden = NO;
            self.txtName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name" attributes:nil];
            [HelperClass activatetextField:self.nameLine height:self.nameLineHeight];
            
        } completion:^(BOOL finished) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissKeyboard];

                self.nameLabel.hidden = NO;
                [self.view bringSubviewToFront:self.nameLabel];
                self.txtName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name" attributes:nil];
                [HelperClass DeactivatetextField:self.nameLine height:self.nameLineHeight];
                
                if ([self.txtName.text isEqualToString: @"" ] || self.txtName.text == nil) {
                    self.nameLabel.hidden = YES;
                }
                else {
                    self.nameLabel.hidden = NO;
                }
                
            });
        }];
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    if ([textField isEqual:_txtMobileNumber]) {
        [self.txtMobileNumber resignFirstResponder];
        [self.txtName becomeFirstResponder];
    }
    else if ([textField isEqual:self.txtName]) {
        [self.txtName resignFirstResponder];
        [self dismissKeyboard];
    }
    else{
        [self dismissKeyboard];
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField isEqual:_txtMobileNumber]) {
        if (range.location > 9) {
            return NO;
        }
    }
    return YES;
}

- (NSInteger) getIndexOfValue:(id) value presentInArray:(NSArray *)array forKey:(NSString *) key {
    
    NSInteger i = 0;
    
    for (i = 0; i < array.count; i++) {
        
        if ([array[i][key] isEqual:value]) {
            break;
        }
    }
    return i;
}

#pragma mark - UIPickerViewDataSource Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == genderPicker)
    {
        return  2;
    }
    else
    {
    
        return [self.arrCountryCode count];
    }
}

#pragma mark - UIPickerViewDelegate Methods

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {

    if (pickerView == genderPicker)
    {
        if (row == 0)
        {
            return @"Male";
        }
        else
        {
            return  @"Female";
        }
    }
    else
    {
        return [NSString stringWithFormat:@"%@      %@", [[self.arrCountryCode objectAtIndex:row] objectForKey:@"code"], [[self.arrCountryCode objectAtIndex:row] objectForKey:@"name"]];
    }
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (pickerView == genderPicker)
    {
        
//        if (row == 0)
//            self.txtGender.text = @"Select Gender (Optional)";
//
//        else
        if (row == 0)
            self.txtGender.text = @"Male";
        else
            self.txtGender.text = @"Female";
    }
    else
    {
        self.txtPhoneCode.text = [[self.arrCountryCode objectAtIndex:row] objectForKey:@"code"];
    }
}


#pragma mark - Custom Methods

- (void)dismissKeyboard {
    
    [self.txtMobileNumber resignFirstResponder];
    [self.txtName resignFirstResponder];
    [self.txtPhoneCode resignFirstResponder];
    [self.txtDateOfBirth resignFirstResponder];
    [self.txtGender resignFirstResponder];
}

- (void)removeAllViews {
    [countryPicker removeFromSuperview];
    [genderPicker removeFromSuperview];
    [toolBar removeFromSuperview];
}

- (void)addCountryPickerView {
    
    countryPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 200, SCREEN_WIDTH, 200)];
    countryPicker.dataSource = self;
    countryPicker.delegate = self;
    countryPicker.backgroundColor = [UIColor whiteColor];
    countryPicker.showsSelectionIndicator = YES;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done:)];
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - countryPicker.frame.size.height - 50, SCREEN_WIDTH, 50)];
    [toolBar setBarStyle:UIBarStyleBlackTranslucent];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton, nil];
    [toolBar setItems:toolbarItems];
    
    [self.view addSubview:countryPicker];
    [self.view addSubview:toolBar];
    
    self.txtPhoneCode.inputView = countryPicker;
    self.txtPhoneCode.inputAccessoryView = toolBar;
    
    [self.view bringSubviewToFront:countryPicker];
    [self.view bringSubviewToFront:toolBar];
}

- (void)done:(id)sender {

    [self.txtPhoneCode resignFirstResponder];
    [countryPicker removeFromSuperview];
    [toolBar removeFromSuperview];
    countryPicker.hidden = YES;
    toolBar.hidden = YES;
}

#pragma mark- gender picker


- (void)addGenderPickerView {
    
    [toolBar removeFromSuperview];
    
    genderPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 100, SCREEN_WIDTH, 100)];
    genderPicker.dataSource = self;
    genderPicker.delegate = self;
    genderPicker.backgroundColor = [UIColor whiteColor];
    genderPicker.showsSelectionIndicator = YES;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done1:)];
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - genderPicker.frame.size.height - 50, SCREEN_WIDTH, 50)];
    [toolBar setBarStyle:UIBarStyleBlackTranslucent];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton, nil];
    [toolBar setItems:toolbarItems];
    
    [self.view addSubview:genderPicker];
    [self.view addSubview:toolBar];
    
    self.txtGender.text = @"Male";
    self.txtGender.inputView = genderPicker;
    self.txtGender.inputAccessoryView = toolBar;
    [self.view bringSubviewToFront:genderPicker];
    [self.view bringSubviewToFront:toolBar];
}

- (void)done1:(id)sender {

    [self.txtGender resignFirstResponder];
    [genderPicker removeFromSuperview];
    [toolBar removeFromSuperview];

    genderPicker.hidden = YES;
    toolBar.hidden = YES;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.genderLabel.hidden = NO;
        self.txtGender.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Gender (Optional)" attributes:nil];
        [HelperClass activatetextField:self.genderLine height:self.genderLineHeight];
        
    } completion:^(BOOL finished) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.genderLabel.hidden = NO;
            [self.view bringSubviewToFront:self.dobLabel];
            self.txtGender.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Gender (Optional)" attributes:nil];
            [HelperClass DeactivatetextField:self.genderLine height:self.genderLineHeight];
            
            if ([self.txtGender.text isEqualToString: @"" ] || self.txtGender.text == nil) {
                self.genderLabel.hidden = NO;
            }
            else {
                self.genderLabel.hidden = YES;
            }
            
        });
    }];

}

#pragma mark - Get OTP API

- (void)getPinNumber {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        [appDelegate startIndicator];
        
        NSString *urlString = [NSString stringWithFormat:@"%@pin", BASEURL];
        NSString *_dateString = [HelperClass getDateString];
        
        NSString *strPhoneCode = self.txtPhoneCode.text;
        if ([strPhoneCode hasPrefix:@"+"]) {
            strPhoneCode = [strPhoneCode substringFromIndex:1];
        }
        NSString *strMobileNumber = [NSString stringWithFormat:@"%@%@", strPhoneCode, self.txtMobileNumber.text];
        [[NSUserDefaults standardUserDefaults] setObject:strMobileNumber forKey:@"MSN"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        appDelegate.strMobileNumber = strMobileNumber;
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@", _dateString, appDelegate.strUserId, strMobileNumber, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setDelegate:self];
        [request setTimeOutSeconds:30];
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];

        NSDictionary *paramDic = @{@"UserID":appDelegate.strUserId,
                                   @"MSN":strMobileNumber};
        NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
        [request setPostBody:[NSMutableData dataWithData:data]];
        
        [request startAsynchronous];
    }
    else
    {
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:@"No internet connection."];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    NSDictionary *dicResponse = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    
    if ([[dicResponse objectForKey:@"Status"] isEqualToString:@"SmsSent"]) {
        
        NSMutableDictionary *dicInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:appDelegate.strMobileNumber, @"MSN", appDelegate.strUserId, @"UserId", self.txtName.text, @"UserName", nil];;
        if (_txtDateOfBirth.text.length > 0) {
            [dicInfo setObject:_txtDateOfBirth.text forKey:@"DateOfBirth"];
        }
        
        PinViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PinViewController"];
        viewController.dicUserInfo = [dicInfo mutableCopy];
        [self presentViewController:viewController animated:true completion:nil];
    }
    else if ([[dicResponse objectForKey:@"success"] integerValue] == 0) {
        
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:@"The app is already registered with another number on this mobile phone. Please enter the correct mobile number to proceed."];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:request.error.localizedDescription];
}

#pragma mark - IBAction Methods

- (IBAction)registerButtonPressed:(id)sender {
    
    if (self.txtName.text.length > 0 && self.txtMobileNumber.text.length > 0) {
        
        if (self.switch_terms.isOn) {
            
            [self getPinNumber];
        }
        else {
            
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:@"Please check for acceptance of privacy policy."];
        }
    }
    else {
        if (self.txtName.text.length == 0){
        
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:@"Please enter name."];
            
        }
        else{
            [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:@"Please enter your mobile number."];
        }
    }
}

- (IBAction)checkMarkButtonPressed:(id)sender
{
    self.btnCheckMark.selected = !self.btnCheckMark.isSelected;
}

- (IBAction)dateOfBirthButtonPressed:(id)sender
{
    [self dismissKeyboard];
    datePicker.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^
    {
        [datePicker setFrame:CGRectMake(0, self.view.frame.size.height - MyDateTimePickerHeight, self.view.frame.size.width, MyDateTimePickerHeight)];
    }];
}

- (BOOL)validateMobileNumber:(NSString *)mobileNumber
{
    NSString *mobileNumberPattern = @"[789][0-9]{9}";
    NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
    BOOL matched = [mobileNumberPred evaluateWithObject:mobileNumber];
    return matched;
}

#pragma mark - TTTAttributedLabelDelegate Methods

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    TermsViewController * termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsController"];
    termsVC.isloggedin = NO;
    [self presentViewController:termsVC animated:YES completion:nil];
}

-(void)setImageOntextField:(UITextField *)textfield andImage:(NSString *)image withSize:(CGSize)size{
    UIImageView *imv = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, size.width, size.height)];
    imv.image = [UIImage imageNamed:image];
    textfield.rightViewMode = UITextFieldViewModeAlways;
    textfield.rightView = imv;
}

- (IBAction)datePickerBtnClick:(id)sender {
    [self removeAllViews];
    datePicker.hidden = NO;

    [UIView animateWithDuration:1.0 animations:^{
        self.dobLabel.hidden = NO;
        self.txtDateOfBirth.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Date of Birth" attributes:nil];
        [HelperClass activatetextField:self.dobLine height:self.dobLineHeight];
        
    } completion:^(BOOL finished) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.dobLabel.hidden = NO;
            [self.view bringSubviewToFront:self.dobLabel];
            self.txtDateOfBirth.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Date of Birth" attributes:nil];
            [HelperClass activatetextField:self.dobLine height:self.dobLineHeight];
        });
    }];
    
    [self dismissKeyboard];
    if (datePicker.frame.origin.y >= self.view.frame.size.height) {
        [self performSelector:@selector(showPicker) withObject:nil afterDelay:1];
    }
    else{
        [self hidePicker];
    }
}

- (IBAction)countaryPickerClick:(id)sender {
    [self removeAllViews];
    countryPicker.hidden = NO;
    if (_txtPhoneCode.text.length == 0) _txtPhoneCode.text = [[self.arrCountryCode firstObject] objectForKey:@"code"];;
    [self performSelector:@selector(newMethod:) withObject:_txtPhoneCode afterDelay:0.4];
}


- (IBAction)genderBtnClick:(id)sender {
    [self removeAllViews];
    genderPicker.hidden = NO;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.genderLabel.hidden = NO;
        self.txtGender.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Gender (Optional)" attributes:nil];
        [HelperClass activatetextField:self.genderLine height:self.genderLineHeight];
        
    } completion:^(BOOL finished) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.genderLabel.hidden = NO;
            [self.view bringSubviewToFront:self.genderLabel];
            self.txtGender.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Gender (Optional)" attributes:nil];
            [HelperClass activatetextField:self.genderLine height:self.genderLineHeight];
        });
    }];
    [self dismissKeyboard];

    [self addGenderPickerView];
    [genderPicker reloadAllComponents];
}

@end
