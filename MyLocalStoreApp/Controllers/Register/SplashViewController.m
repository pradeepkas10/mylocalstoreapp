//
//  SplashViewController.m
//  RConnect
//
//  Created by HiteshDhawan on 05/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "SplashViewController.h"
#import "HomeVC.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    
  
    BOOL isUserAuthenticated = [[NSUserDefaults standardUserDefaults] boolForKey:@"UserAuthenticated"];

    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    if (status) {
        if (isUserAuthenticated)
        {
            [self loadUserProfile];
        }
        else {
            [self getUserID];
        }
    }
    else {

        if (isUserAuthenticated)
        {
            NSDictionary * dicgetUserProfile = [[NSDictionary alloc]init];
            dicgetUserProfile = [[NSUserDefaults standardUserDefaults]objectForKey:DICProfileData];
            [self loadUserProfileOffline:dicgetUserProfile];
        }
        else {
            NSDictionary * dicgetuser = [[NSDictionary alloc]init];
            dicgetuser = [[NSUserDefaults standardUserDefaults]objectForKey:@"dicGetUser"];
            [self updatedataWithdictUser:dicgetuser];
        }
    }


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidDisappear:(BOOL)animated{
    [UIApplication sharedApplication].statusBarHidden = NO;
    //[UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

-(BOOL)prefersStatusBarHidden{
    return true;
}


#pragma mark
#pragma mark - UserID Details
- (void)getUserID {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus reachStatus = [reachability currentReachabilityStatus];
    
    if (reachStatus) {
        
        NSString *urlString = [NSString stringWithFormat:@"%@initialize", BASEURL];
        
        NSString *_dateString = [HelperClass getDateString];
        NSString * Device_Id = [[appDelegate getUniqueDeviceIdentifierAsString] stringByReplacingOccurrencesOfString:@"-" withString:@""];
        
        //Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, Device_Id, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        NSDictionary *paramDic = @{@"DeviceId":Device_Id, @"Culture" : @"NO-nb"};
        
        NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
        [request setRequestMethod:@"POST"];
        [request setPostBody:[NSMutableData dataWithData:data]];
        
        [request setDidFinishSelector:@selector(getUserRequestSuccessful:)];
        [request setDidFailSelector:@selector(getUserRequestFailed:)];
        [request setDelegate:self];
        [request startAsynchronous];
    }
    else {
        
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:@"Some Error Occurred." andButtonTitle:@"Ok"];
    }
    
}

- (void)getUserRequestSuccessful:(ASIHTTPRequest *)request {
    
    NSDictionary *responseDic = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:request.responseData options:kNilOptions error:nil]];

    if([[responseDic allKeys] containsObject:@"UserId"]) {
        [[NSUserDefaults standardUserDefaults]setObject:responseDic forKey:@"dicGetUser"];
    [self updatedataWithdictUser:responseDic];
    
//    if([[responseDic allKeys] containsObject:@"UserId"]) {
//        appDelegate.strUserId = [responseDic objectForKey:@"UserId"];
//        NSArray *dicConsents = [responseDic objectForKey:@"Consents"];
//        NSLog(@"%@",dicConsents);
//
//        for (NSDictionary *dict in dicConsents) {
//            NSDictionary * dic = [dict objectForKey:@"CurrentVersion"];
//            if ([dict[@"Name"] isEqualToString:@"Master"]) {
//
//                appDelegate.privacyPolicyStr = @"";
//                [[NSUserDefaults standardUserDefaults] removeObjectForKey:PrivacyPolicy];
//
//
//                appDelegate.privacyPolicyStr = [dic objectForKey:@"PrivacyPolicy"];
//                appDelegate.minimumAgeFactor = [dic objectForKey:@"MinimumAge"];
//                appDelegate.dobStringMinimumAge = [dic objectForKey:@"MinimumAgeText"];
//                appDelegate.registrationTitle = [dic objectForKey:@"Title"];
//            }
//        }
//
//        [[NSUserDefaults standardUserDefaults]setObject:dicConsents forKey:DICConsents];
//        [[NSUserDefaults standardUserDefaults]setObject:appDelegate.privacyPolicyStr forKey:PrivacyPolicy];
//        [[NSUserDefaults standardUserDefaults]setObject:appDelegate.minimumAgeFactor forKey:MINAge];
//        [[NSUserDefaults standardUserDefaults] setObject:[responseDic objectForKey:@"UserId"] forKey:@"UserId"];
//
//        [[NSUserDefaults standardUserDefaults] synchronize];
//
//        [self MakeDecisionforRootView];
    }
    else{
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:@"No internet connection." andButtonTitle:@"Ok"];
    }
}

- (void)getUserRequestFailed:(ASIHTTPRequest *)request {
    
    NSDictionary *responseDic = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    NSLog(@"GET User Failed - %@",responseDic);
}

-(void)updatedataWithdictUser:(NSDictionary *)responseDic
{

        appDelegate.strUserId = [responseDic objectForKey:@"UserId"];
        NSArray *dicConsents = [responseDic objectForKey:@"Consents"];
        NSLog(@"%@",dicConsents);

        for (NSDictionary *dict in dicConsents) {
            NSDictionary * dic = [dict objectForKey:@"CurrentVersion"];
            if ([dict[@"Name"] isEqualToString:@"Master"]) {

                appDelegate.privacyPolicyStr = @"";
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:PrivacyPolicy];


                appDelegate.privacyPolicyStr = [dic objectForKey:@"PrivacyPolicy"];
                appDelegate.minimumAgeFactor = [dic objectForKey:@"MinimumAge"];
                appDelegate.dobStringMinimumAge = [dic objectForKey:@"MinimumAgeText"];
                appDelegate.registrationTitle = [dic objectForKey:@"Title"];
            }
        }

        [[NSUserDefaults standardUserDefaults]setObject:dicConsents forKey:DICConsents];
        [[NSUserDefaults standardUserDefaults]setObject:appDelegate.privacyPolicyStr forKey:PrivacyPolicy];
        [[NSUserDefaults standardUserDefaults]setObject:appDelegate.minimumAgeFactor forKey:MINAge];
        [[NSUserDefaults standardUserDefaults] setObject:[responseDic objectForKey:@"UserId"] forKey:@"UserId"];

        [[NSUserDefaults standardUserDefaults] synchronize];

        [self MakeDecisionforRootView];
}

#pragma mark
#pragma mark - DECISION FOR VIEW CONTROLLER

- (void)MakeDecisionforRootView {
    

    BOOL isUserAuthenticated = [[NSUserDefaults standardUserDefaults] boolForKey:@"UserAuthenticated"];
    
    if (isUserAuthenticated)
    {
        NSArray *consents = [[NSUserDefaults standardUserDefaults] objectForKey:DICConsents];
        NSString *masterConsetState;
        for (NSDictionary *dicr in consents) {
            if ([dicr[@"Name"] isEqualToString:@"Master"]){
                masterConsetState = dicr[@"State"];
            }
            else{
                //check if any of preferences has changed?
            }
        }
        
        if (![masterConsetState isEqualToString:@"ConsentGiven"]){
            //Terms

            TermsViewController *dashboard = [mainStoryboard instantiateViewControllerWithIdentifier:@"termsController"];
            dashboard.isloggedin = true;
            dashboard.isNewMasterConsent = true;

            MenuItemsTVC *sidemenu = [MenuItemsTVC sharedInstance];
            ENSideMenuNavigationController *naviController = [[ENSideMenuNavigationController alloc] initWithMenuViewController:sidemenu contentViewController:dashboard];
            naviController.navigationBarHidden = YES;
            appDelegate.isLoader = YES;

            CGFloat sidemenuWide = SCREEN_WIDTH - 75  ;
            [naviController.sideMenu setMenuWidth:sidemenuWide];

            [appDelegate.window makeKeyAndVisible];
            [appDelegate.window setRootViewController:naviController];
        }
        else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"PreferencesSet"] == false){
            PreferencesViewController *registerController = [mainStoryboard instantiateViewControllerWithIdentifier:@"PreferencesViewController"];
            registerController.isNewMasterConsent = false;
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:registerController];
            navController.navigationBarHidden = YES;
            appDelegate.window.rootViewController = navController;
        }
        else{
            appDelegate.needAPIRefresh = YES;
            HomeVC *home = [mainStoryboard instantiateViewControllerWithIdentifier:@"HomeVC"];
            
            MenuItemsTVC *sidemenu = [MenuItemsTVC sharedInstance];
            
            ENSideMenuNavigationController *naviController = [[ENSideMenuNavigationController alloc] initWithMenuViewController:sidemenu contentViewController:home];
            naviController.navigationBarHidden = YES;
            appDelegate.isLoader = YES;
            
            CGFloat sidemenuWide = SCREEN_WIDTH - 75  ;
            [naviController.sideMenu setMenuWidth:sidemenuWide];
            
            [appDelegate.window makeKeyAndVisible];
            [appDelegate.window setRootViewController:naviController];
        }
    }
    else {
        
        RegisterViewController *registerController = [mainStoryboard instantiateViewControllerWithIdentifier:@"registerController"];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:registerController];
        navController.navigationBarHidden = YES;
        appDelegate.window.rootViewController = navController;
        appDelegate.isLoader = NO;
    }
    
    appDelegate.strUserId = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"];
    appDelegate.strMobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"MSN"];
    
    if ([appDelegate.strUserId length] > 0) {

        [appDelegate loadFeedbackBacklogValue:false];
    }
}


#pragma mark
#pragma mark - GET USER PROFILE

- (void)loadUserProfile {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    [appDelegate startIndicator];
    if (status) {
        
        NSString *urlString = [NSString stringWithFormat:@"%@user/?UserID=%@", BASEURL, appDelegate.strUserId];
        
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request setDidFinishSelector:@selector(requestFinished_UserProfile:)];
        [request setDidFailSelector:@selector(requestFailed_UserProfile:)];
        
        
        [request startAsynchronous];
    }
    else {
        
        [appDelegate stopIndicator];
        [CustomAlertController showAlertOnController:appDelegate.window.rootViewController withAlertType:Simple withTitle:@"Error occurred" andMessage:@"No internet connection." andButtonTitle:@"Ok"];
    }
}

- (void)requestFinished_UserProfile:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    NSDictionary *dicUser = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    [[NSUserDefaults standardUserDefaults]setObject:dicUser forKey:DICProfileData];
    [self loadUserProfileOffline:dicUser];

    
}

- (void)requestFailed_UserProfile:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    
    [CustomAlertController showAlertOnController:appDelegate.window.rootViewController withAlertType:Simple withTitle:@"Error" andMessage:request.error.localizedDescription andButtonTitle:@"Ok"];
}

-(void)loadUserProfileOffline:(NSDictionary *)dicUser
{
    NSArray *dicConsents = [dicUser objectForKey:@"Consents"];
    NSLog(@"%@",dicConsents);
    [[NSUserDefaults standardUserDefaults]setObject:dicConsents forKey:DICConsents];



    for (NSDictionary *dict in dicConsents) {
        NSDictionary * dic = [dict objectForKey:@"CurrentVersion"];
        if ([dict[@"Name"] isEqualToString:@"Master"]) {

            appDelegate.privacyPolicyStr = @"";
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:PrivacyPolicy];

            appDelegate.privacyPolicyStr = [dic objectForKey:@"PrivacyPolicy"];
            appDelegate.minimumAgeFactor = [dic objectForKey:@"MinimumAge"];
            appDelegate.dobStringMinimumAge = [dic objectForKey:@"MinimumAgeText"];
            appDelegate.registrationTitle = [dic objectForKey:@"Title"];
        }
    }

    [[NSUserDefaults standardUserDefaults]setObject:dicConsents forKey:DICConsents];
    [[NSUserDefaults standardUserDefaults]setObject:appDelegate.privacyPolicyStr forKey:PrivacyPolicy];
    [[NSUserDefaults standardUserDefaults]setObject:appDelegate.minimumAgeFactor forKey:MINAge];

    [[NSUserDefaults standardUserDefaults] synchronize];
    [self saveThePreferenceCheckDate];
    [self MakeDecisionforRootView];
}

#pragma mark - HIT Consent API for Granular Consent Check
-(void)HitUserProfileAPIForConsents {
    
//    if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
//        HomeVC *vc = [appDelegate returnTopViewController];
//        vc.delegate = self;
        
        [self loadUserProfile];
//    }
    
}

-(void)saveThePreferenceCheckDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    NSString *today = [formatter stringFromDate:[NSDate date]];
    [[NSUserDefaults standardUserDefaults] setObject:today forKey:@"PreferenceCheckUpDate"];
}


@end
