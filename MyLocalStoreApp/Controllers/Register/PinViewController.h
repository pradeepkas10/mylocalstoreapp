//
//  PinViewController.h
//  TwentyFourSeven
//
//  Created by Akhilesh on 9/26/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyLocalStoreApp.pch"

@interface PinViewController : UIViewController<UITextFieldDelegate>

@property (nonatomic, strong) NSDictionary *dicUserInfo;
@property (nonatomic, strong) NSString * MasterConsentsId;
@property (weak, nonatomic) IBOutlet UIView *viewPin;
@property (weak, nonatomic) IBOutlet UITextField *txtPin;
@property (weak, nonatomic) IBOutlet UIView *pinView;
@property (weak, nonatomic) IBOutlet UILabel *pinLabel;
@property (weak, nonatomic) IBOutlet UILabel *pinLine;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pinLineHeight;

- (IBAction)backButtonPressed:(id)sender;
- (IBAction)submitButtonPressed:(id)sender;


@end
