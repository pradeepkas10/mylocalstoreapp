//
//  PreferencesViewController.m
//  MyLocalStoreApp
//
//  Created by Hitesh Dhawan on 27/03/18.
//  Copyright © 2018 SurbhiV. All rights reserved.
//

#import "PreferencesViewController.h"
#import "MyLocalStoreApp.pch"

@interface PreferencesViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *consentArray;
    NSDictionary *userDiction;
    NSMutableArray *approvedConsentArray;
    NSMutableArray *revokedConsentArray;
    NSString *DateOfBirth;
    UIAlertController *alert;
}
@end

@implementation PreferencesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDiction = [NSDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:DICProfileData]];
    DateOfBirth = userDiction[@"DateOfBirth"];
    
    approvedConsentArray = [NSMutableArray array];
    revokedConsentArray = [NSMutableArray array];
    
    NSArray * arrConsents = [[NSUserDefaults standardUserDefaults] objectForKey:DICConsents];
    consentArray = [NSMutableArray array];

    for (int i=0; i<arrConsents.count; i++)
    {
        NSDictionary * dicConsents = [[NSDictionary alloc ]init];
        dicConsents = arrConsents[i];

        NSMutableDictionary *consentDictionary = [NSMutableDictionary dictionary];
        if ([dicConsents[@"Name"] isEqualToString:@"Master"]) {
        }
        else{
            [consentDictionary setObject:dicConsents forKey: [NSString stringWithFormat:@"%@",dicConsents[@"Name"]]];
            [consentArray addObject:consentDictionary];
        }
        
        
        if ([[dicConsents objectForKey:@"State"] isEqualToString:@"ConsentGiven" ]) {
            NSString *idstr = [NSString stringWithFormat:@"%@",[[dicConsents objectForKey:@"CurrentVersion" ] objectForKey:@"Id"]];
            [approvedConsentArray addObject:idstr];
        }
        else{
            NSString *idstr = [NSString stringWithFormat:@"%@",[[dicConsents objectForKey:@"CurrentVersion" ] objectForKey:@"Id"]];
            [revokedConsentArray addObject:idstr];
        }
        
    }
            
    [self.tbl_consents reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TAbleview
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return consentArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PreferenceCell *cell = (PreferenceCell *)[tableView dequeueReusableCellWithIdentifier:@"PreferenceCell" forIndexPath:indexPath ];
    
    NSDictionary *dict = [consentArray objectAtIndex:indexPath.row];
    NSString *dictkey = [[dict allKeys] firstObject];
    
    [cell.prefSwitch addTarget:self action:@selector(consentValueChanged:) forControlEvents:UIControlEventValueChanged];

//    if (self.isNewMasterConsent == true) {
        if ([[[dict objectForKey:dictkey] objectForKey:@"State"] isEqualToString:@"ConsentGiven"]){
            cell.prefSwitch.on = true;
        }
        else
        {
            cell.prefSwitch.on = false; // Keeping all off as asked by Client
            //        // Make all granular consents default “un-toggled". We need the user to actively toggle and give these consents.
            //        // Mail : 03/04/18, 6:45 PM

        }
//    }
//    else{
//        cell.prefSwitch.on = false;
//    }
    
    if (![[[[dict objectForKey:dictkey] objectForKey:@"CurrentVersion"] objectForKey:@"Title"] isEqualToString:@""] && [[[dict objectForKey:dictkey] objectForKey:@"CurrentVersion"] objectForKey:@"Title"] != nil){
        
        NSString * htmlString = [[[dict objectForKey:dictkey] objectForKey:@"CurrentVersion"] objectForKey:@"Title"];
        NSString *str = [HelperClass stringByStrippingHTML:htmlString];
        cell.prefDescriptionLabel.text = str;
        [cell.prefDescriptionLabel sizeToFit];
    }
    else
    {
        NSString *titleStr = [dict objectForKey:@"Name"];
        cell.prefDescriptionLabel.text = titleStr;
        [cell.prefDescriptionLabel sizeToFit];
    }
    
    
    tableView.estimatedRowHeight = 41;
    tableView.rowHeight = UITableViewAutomaticDimension;
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dict = [consentArray objectAtIndex:indexPath.row];
    NSString *dictkey = [[dict allKeys] firstObject];
    
    if (![[[[dict objectForKey:dictkey] objectForKey:@"CurrentVersion"] objectForKey:@"Description"] isEqualToString:@""] && [[[dict objectForKey:dictkey] objectForKey:@"CurrentVersion"] objectForKey:@"Description"] != nil){
        
        NSString *titleStr = [[[dict objectForKey:dictkey] objectForKey:@"CurrentVersion"] objectForKey:@"Title"];
        NSString * htmlString = [[[dict objectForKey:dictkey] objectForKey:@"CurrentVersion"] objectForKey:@"Description"];
        NSString *str = [HelperClass stringByStrippingHTML:htmlString];
        
        alert = [UIAlertController alertControllerWithTitle:titleStr message:str preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:true completion:nil];
    }
    else if (![[[[dict objectForKey:dictkey] objectForKey:@"CurrentVersion"] objectForKey:@"Title"] isEqualToString:@""] && [[[dict objectForKey:dictkey] objectForKey:@"CurrentVersion"] objectForKey:@"Title"] != nil){
        
        NSString *titleStr = [[[dict objectForKey:dictkey] objectForKey:@"CurrentVersion"] objectForKey:@"Title"];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:titleStr message:@"" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:true completion:nil];

    }
    else {
        NSString *titleStr = [[dict objectForKey:dictkey] objectForKey:@"Name"];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:titleStr message:@"" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:true completion:nil];
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 45;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *vw = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 45)];
    vw.backgroundColor = [UIColor whiteColor];
    
    UIButton *continueBtn = [[UIButton alloc] initWithFrame: CGRectMake((SCREEN_WIDTH/2) - 100, 5, 200, 35)];
    [continueBtn setTitle:@"CONTINUE" forState:UIControlStateNormal];
    continueBtn.titleLabel.textColor = [UIColor whiteColor];
    continueBtn.backgroundColor = THEMECOLOR;
    [continueBtn addTarget:self action:@selector(SaveChangesMethod) forControlEvents:UIControlEventTouchUpInside];
    
    [vw addSubview:continueBtn];
    return vw;
}

#pragma mark - Go To Gome Screen


-(void)GoToHomeScreen{
    HomeVC *home = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeVC"];
    MenuItemsTVC *sidemenu = [MenuItemsTVC sharedInstance];
    
    ENSideMenuNavigationController *naviController = [[ENSideMenuNavigationController alloc] initWithMenuViewController:sidemenu contentViewController:home];
    naviController.navigationBarHidden = YES;
    
    CGFloat sidemenuWide = SCREEN_WIDTH - 60 ;
    [naviController.sideMenu setMenuWidth:sidemenuWide];
    
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"PreferencesSet"];
    [appDelegate.window setRootViewController:naviController];

}


#pragma mark - Change Consent / Continue Button Action

-(void)SaveChangesMethod{

    [appDelegate startIndicator];

    NSString *strURL = [NSString stringWithFormat:@"%@user", BASEURL];

    NSString *_dateString = [HelperClass getDateString];

    // Generate signature
    NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId, SALT];
    NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
    
    NSMutableDictionary *paramDic;
        
    paramDic = [[NSMutableDictionary alloc] initWithDictionary:@{@"UserID":appDelegate.strUserId,
                                                                 @"DateOfBirth":DateOfBirth,
                                                                 }];
    
    [paramDic setObject:approvedConsentArray forKey:@"ApprovedConsents"];
    [paramDic setObject:revokedConsentArray forKey:@"RevokedConsents"];

    
    NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];

    ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:strURL]];
    [request setRequestMethod:@"PUT"];
    request.name = @"SaveChanges";
    [request setTimeOutSeconds:30.0];
    [request setDelegate:self];

    [request addRequestHeader:@"Accept" value:@"application/json"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
    [request addRequestHeader:@"X-Liquid-Signature" value:signature];

    [request setPostBody:[NSMutableData dataWithData:data]];

    [request startAsynchronous];

}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSDictionary *dicUser = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    [[NSUserDefaults standardUserDefaults]setObject:dicUser forKey:DICProfileData];
    
    NSArray *dicConsents = [dicUser objectForKey:@"Consents"];
    [[NSUserDefaults standardUserDefaults]setObject:dicConsents forKey:DICConsents];


    
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"PreferencesSet"];
    [appDelegate stopIndicator];
    [self GoToHomeScreen];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error" andMessage:request.error.localizedDescription andButtonTitle:@"Dismiss"];
}

#pragma mark - Switch value changed

- (IBAction)consentValueChanged:(id)sender{
    
    PreferenceCell *cell = (PreferenceCell *)[[sender superview] superview];
    int index = (int)[self.tbl_consents indexPathForCell:cell].row ;
    
    NSDictionary *dict = [consentArray objectAtIndex:index];
    NSString *dictkey = [[dict allKeys] firstObject];
    NSString *idstr = [NSString stringWithFormat:@"%@",[[[dict objectForKey:dictkey] objectForKey:@"CurrentVersion" ] objectForKey:@"Id"]];

    
    if (cell.prefSwitch.on == true) {
        if ([revokedConsentArray containsObject:idstr]) {
            [revokedConsentArray removeObject:idstr];
        }
        if (![approvedConsentArray containsObject:idstr]) {
            [approvedConsentArray addObject:idstr];
        }
    }
    else {
        if (![revokedConsentArray containsObject:idstr]) {
            [revokedConsentArray addObject:idstr];
        }
        if ([approvedConsentArray containsObject:idstr]) {
            [approvedConsentArray removeObject:idstr];
        }
    }
    NSLog(@"Approved - %@",approvedConsentArray);
    NSLog(@"Revoked - %@",revokedConsentArray);
}


#pragma mark
#pragma mark - Touches began

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    if ([[[self.view subviews] lastObject] isEqual:alert] ) {
        [alert dismissViewControllerAnimated:true completion:nil];
    }
}

@end





/*
 
 {
 Address = " ";
 City = " ";
 Consents =     (
 {
 ChangeLog =             (
 {
 Id = 297;
 Version = 1;
 }
 );
 CurrentVersion =             {
 DefaultState = 0;
 Description = "<p><strong>Give email consent to receive emails from us.</strong></p>
 \n";
 Id = 297;
 MinimumAge = 0;
 Title = "Email consent";
 Version = 1;
 };
 Mandatory = 0;
 Name = Email;
 State = NoConsent;
 },
 {
 ChangeLog =             (
 {
 Id = 299;
 Version = 1;
 }
 );
 CurrentVersion =             {
 DefaultState = 0;
 Description = "<p>Give location consent to receive location based offers from us.</p>
 \n<p><strong>Location based offers are better deals.</strong></p>
 \n<p>We will not give location data to third parties.</p>
 \n";
 Id = 299;
 MinimumAge = 0;
 Title = "Location consent";
 Version = 1;
 };
 Mandatory = 0;
 Name = Location;
 State = NoConsent;
 },
 {
 ChangeLog =             (
 );
 CurrentVersion =             {
 DefaultState = 0;
 Description = "Master consent";
 Id = 271;
 MinimumAge = 13;
 MinimumAgeText = "I confirm that i am older than 13";
 PrivacyPolicy = "Privacy policy here";
 Title = "MLS Master consent";
 Version = 1;
 };
 LastApproved =             {
 DefaultState = 0;
 Description = "Master consent";
 Id = 271;
 MinimumAge = 13;
 MinimumAgeText = "I confirm that i am older than 13";
 PrivacyPolicy = "Privacy policy here";
 Title = "MLS Master consent";
 Version = 1;
 };
 Mandatory = 1;
 Name = Master;
 State = ConsentGiven;
 },
 {
 ChangeLog =             (
 {
 Id = 298;
 Version = 1;
 }
 );
 CurrentVersion =             {
 DefaultState = 0;
 Description = "<h1 id=\"give-email-consent-to-receive-emails-from-us-\">Give email consent to receive emails from us.</h1>
 \n";
 Id = 298;
 MinimumAge = 0;
 Title = "Profiling consent";
 Version = 1;
 };
 Mandatory = 0;
 Name = Profiling;
 State = NoConsent;
 },
 {
 ChangeLog =             (
 {
 Id = 296;
 Version = 1;
 }
 );
 CurrentVersion =             {
 DefaultState = 0;
 Description = "<p>Give SMS consent to receive SMS from us.</p>
 \n";
 Id = 296;
 Title = "SMS consent";
 Version = 1;
 };
 Mandatory = 0;
 Name = Sms;
 State = NoConsent;
 }
 );
 DateOfBirth = "2000-03-27";
 DeviceId = C92BC1DB87B242ABBD42688BD30257F4;
 Emails =     (
 );
 Msn = 919958422711;
 Name = Surbhi;
 PreferredStores =     (
 );
 SelectedPreferredStores =     (
 );
 UserGroups =     (
 {
 GroupDescription = "Single-use";
 GroupId = "Single-use";
 IsUserMember = 0;
 UserConfigurable = CodeConfigurable;
 },
 {
 GroupDescription = Open;
 GroupId = Open;
 IsUserMember = 1;
 UserConfigurable = Configurable;
 },
 {
 GroupDescription = "Multi-use";
 GroupId = "Multi-use";
 IsUserMember = 0;
 UserConfigurable = CodeConfigurable;
 }
 );
 UserId = MYLOCX569D830490584CAE899794E033BF2B50;
 UserMyPage = "http://www.barcodes.no";
 }

 
 
 
 */
