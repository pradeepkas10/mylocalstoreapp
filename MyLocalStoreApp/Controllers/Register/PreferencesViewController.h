//
//  PreferencesViewController.h
//  MyLocalStoreApp
//
//  Created by Hitesh Dhawan on 27/03/18.
//  Copyright © 2018 SurbhiV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreferencesViewController : UIViewController
 @property (weak , nonatomic) IBOutlet UITableView * tbl_consents;
@property BOOL isNewMasterConsent;

@end
