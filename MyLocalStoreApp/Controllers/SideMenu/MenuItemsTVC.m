//
//  MenuItemsTVC.m
//  ObjCExample
//
//  Created by Evgeny on 09.01.15.
//  Copyright (c) 2015 Evgeny Nazarov. All rights reserved.
//

#import "MenuItemsTVC.h"
#import "SVProgressHUD.h"
#import "MyLocalStoreApp.pch"


@implementation MenuItemsTVC
{
    NSMutableArray * arrsection1;
    NSMutableArray * arrsection2;
    NSMutableArray * arrsection3;
    NSMutableArray * imgsection1;
    NSMutableArray * imgsection2;
    NSMutableArray * imgsection3;
}
+ (instancetype)sharedInstance {
    
    static MenuItemsTVC *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [MenuItemsTVC new];
    });
    return sharedInstance;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.isMoreOpen = false;
    
    self.view.alpha = 0.85;
    
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    bgView.backgroundColor = THEMECOLOR;//[UIColor colorWithRed:6.0/255.0 green:66.0/255.0 blue:136.0/255.0 alpha:0.80];//THEMECOLOR;
    bgView.alpha = 1.0;
    
    UIView *backView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    backView.backgroundColor = [UIColor clearColor];
    [backView addSubview:bgView];

    self.tableView.backgroundView = backView;
    self.tableView.allowsSelection = YES;
    
    [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    self.mainMenuArray = [NSMutableArray arrayWithObjects:@"Home",@"Coupons",@"Stamp cards",@"Rewards",@"Shop",@"Games",@"Feedback",@"My page",@"Refer a Friend",@"My Local Stores",@"Store Locator",@"Scan & Get Rewards",@"Terms & Conditions",@"Contact Support",@"LogOut", nil];
    self.imageArray_mainMenu = [NSMutableArray arrayWithObjects:@"HomeIcon",@"CouponsIcon",@"StampIcon",@"RewardsIcon",@"ShopIcon",@"GamesIcon",@"FeedackIcon",@"MypageIcon",@"ReferaFriendIcon",@"MyLocalStoresIcon",@"StoreLocatorIcon",@"ScanGetRewardsIcon",@"TermsIcon",@"ContactSupportIcon",@"LogOutIcon", nil];
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}



#pragma mark - TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.mainMenuArray.count;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        cell.backgroundColor = [UIColor clearColor];
        [cell.textLabel setTextColor:[UIColor whiteColor]];
    
    }
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone ;
    CALayer * layer = [CALayer layer];
    layer.frame = CGRectMake (5 ,cell.contentView.frame.size.height - 0.6 ,self.tableView.frame.size.width ,0.6);
    [cell.layer addSublayer:layer];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    
    if (IS_IPHONE_4_OR_LESS || IS_IPHONE_5)
    {
      [cell.textLabel setFont:[UIFont fontWithName:LatoBold size:14.0]];
    }
    else
    {
        [cell.textLabel setFont:[UIFont fontWithName:LatoBold size:16.0]];
    }
    
    
    UILabel *count = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.size.width - 35, (cell.frame.size.height - 25)/2, 25, 25)];
    count.layer.borderWidth = 1.0;
    count.layer.borderColor = [UIColor whiteColor].CGColor;
    count.textColor = [UIColor whiteColor];
    count.textAlignment = NSTextAlignmentCenter;
    count.layer.cornerRadius = 12.5;
    count.clipsToBounds = YES;
    count.font = [UIFont fontWithName:LatoBlack size:13];
    count.tag = 121200;
    
    if ([[cell.contentView subviews] containsObject:[cell.contentView viewWithTag:121200]]) {
        [[cell.contentView viewWithTag:121200] removeFromSuperview];
    }
    tableView.separatorColor = [UIColor clearColor];
    [cell.contentView addSubview:count];

    if ([cell.contentView.subviews containsObject:[cell.contentView viewWithTag:1010122]]) {
        [[cell.contentView viewWithTag:1010122] removeFromSuperview];
    }

    if (indexPath.row == 1) {
        if (_arrCouponCount > 0) {
            count.hidden = NO;
            count.text = @"1";
            count.text = [NSString stringWithFormat:@"%d",_arrCouponCount];
        }
        else{
            count.hidden = YES;
            [count removeFromSuperview];
        }
    }
    else if (indexPath.row == 2)
    {
        if (_arrLoyaltyCount > 0) {
            count.hidden = NO;
            count.text = [NSString stringWithFormat:@"%d",_arrLoyaltyCount];
//                count.text = @"1";

        }
        else{
            count.hidden = YES;
            [count removeFromSuperview];
        }
    }
    else if (indexPath.row == 3)
    {
        if (_arrRewardsCount > 0) {
            count.hidden = NO;
            count.text = [NSString stringWithFormat:@"%d",_arrRewardsCount];
//                count.text = @"1";

        }
        else{
            count.hidden = YES;
            [count removeFromSuperview];
        }
    }
    else if (indexPath.row == 5){
        if (_arrOffersCount > 0) {
            count.hidden = NO;
            count.text = [NSString stringWithFormat:@"%d",_arrOffersCount];
        }
        else{
            count.hidden = YES;
            [count removeFromSuperview];
        }
    }
    else if (indexPath.row == 6){
        if (_feedbackCount > 0) {
            count.hidden = NO;
            count.text = [NSString stringWithFormat:@"%d",_feedbackCount];
        }
        else{
            count.hidden = YES;
            [count removeFromSuperview];
        }
    }
    else{
        count.hidden = YES;
        [count removeFromSuperview];
    }


    cell.textLabel.text = [NSString stringWithFormat:@" %@",self.mainMenuArray[indexPath.row]].uppercaseString;
    cell.imageView.image = [UIImage imageNamed:_imageArray_mainMenu[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    dispatch_async(dispatch_get_main_queue(), ^{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    
    if (indexPath.row == 0)// home
    {
        HomeVC *dashboard = [mainStoryboard instantiateViewControllerWithIdentifier:@"HomeVC"];
        dashboard.title = @"HOME";
        
        [dashboard.hamMenuBtn setImage:[UIImage imageNamed:@"hamMenuIcon"] forState:UIControlStateNormal];

        [appDelegate setTopViewwithVC:dashboard];
    }
    else if (indexPath.row == 1)// coupons
    {
        CouponDetailsViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"detailsController"];
        dashboard.title = @"COUPONS";
        dashboard.selectedPageOnDetail = self.selectedPageOnDetail;
        
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:false completion:nil];
        }
        _arrCouponCount = 0;
    }
    else if (indexPath.row == 2)// stamp cards
    {
        
        CouponDetailsViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"detailsController"];
        dashboard.title = @"LOYALTY"; // Stamp
        dashboard.selectedPageOnDetail = self.selectedPageOnDetail;

        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
        _arrLoyaltyCount = 0;
    }
    else if (indexPath.row == 3)// rewards
    {
        CouponDetailsViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"detailsController"];
        dashboard.title = @"REWARDS";
        dashboard.selectedPageOnDetail = self.selectedPageOnDetail;
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
        _arrRewardsCount = 0;
    }
    else if (indexPath.row == 4) // Shop
    {
//       UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Coming Soon" message:@"" preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            [self.navigationController popToRootViewControllerAnimated:YES];
//        }];
//        [alertController addAction:okAction];
//
//        [self presentViewController:alertController animated:YES completion:nil];
//        CFRunLoopWakeUp(CFRunLoopGetCurrent());
        
       AddMyLocalStoreViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"AddMyLocalStoreViewController"];
        dashboard.title = @"LOCAL STORE"; 
        
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
        _arrLoyaltyCount = 0;
    }
    else if (indexPath.row == 5) // Games
    {
        CouponDetailsViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"detailsController"];
        dashboard.title = @"OFFERS";
        dashboard.selectedPageOnDetail = self.selectedPageOnDetail;
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
        _arrOffersCount = 0;
    }
    else if (indexPath.row == 6)// Feedback
    {
        FeedbackViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"feedbackController"];
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            if(appDelegate.backlogVal > 0){
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"ISACTIVEFEEDBACK"];
            }
            [home presentViewController:dashboard animated:true completion:nil];
        }
    }
    else if (indexPath.row == 7) // My Page
    {
        MoreViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"moreController"];
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            //sideMenuController()?.setContentViewController(home)

          [home presentViewController:dashboard animated:false completion:nil];
        }
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Coming Soon" message:@"" preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            [self.navigationController popToRootViewControllerAnimated:YES];
//        }];
//        [alertController addAction:okAction];
//        [self presentViewController:alertController animated:YES completion:nil];
//        CFRunLoopWakeUp(CFRunLoopGetCurrent());

    }
    else if (indexPath.row == 8)// refer A friend
    {
        ReferViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"referController"];
        dashboard.isShareView = NO;
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
    }
    else if (indexPath.row == 9) // My local Stores
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Coming Soon" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        // [self.view.window.rootViewController presentViewController:alertController animated:YES completion:nil];

        CFRunLoopWakeUp(CFRunLoopGetCurrent());
    }
    else if (indexPath.row == 10) // Store locator
    {
        StoresViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"storesController"];
        dashboard.isComingfromMenu = YES;
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
    }
    
    else if (indexPath.row == 11)// Scan and get rewards
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Coming Soon" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
        [alertController addAction:okAction];
        //[self.view.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        [self presentViewController:alertController animated:YES completion:nil];
        CFRunLoopWakeUp(CFRunLoopGetCurrent());

    }
    else if (indexPath.row == 12)// Terms
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"Terms" forKey:@"Privacy"];
        TermsViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"termsController"];
        dashboard.isloggedin = true;

        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
    }
    else if (indexPath.row == 13) // contact Support
    {
        ContactUsVC *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"ContactUsVC"];
        if ([[appDelegate returnTopViewController] isKindOfClass:[HomeVC class]]) {
            HomeVC *home = [appDelegate returnTopViewController];
            [home hideSideMenuView];
            [home presentViewController:dashboard animated:true completion:nil];
        }
    }
    
    else if (indexPath.row == 14){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"confirm" message:@"Are you sure you want to log out?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [SVProgressHUD showWithStatus:@"Logging Out..." maskType:SVProgressHUDMaskTypeBlack];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"UserAuthenticated"];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"UserId"];
            [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"PreferencesSet"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:DICOffers];
            appDelegate.dicOffers = nil;
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            [appDelegate changeRootViewController];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                             }];
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        //[self.view.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        [self presentViewController:alertController animated:YES completion:nil];
        CFRunLoopWakeUp(CFRunLoopGetCurrent());
    }
       
    [tableView reloadData];
          });
}


#pragma mark-  Open More Menu Pressed

-(IBAction)OpenMoreMenuPressed:(id)sender
{

}


- (void)showHome {
//    self.selectedIndexPath = [NSIndexPath indexPathForRow:20 inSection:0];
//    HomeVC *dashboard = [mainStoryboard instantiateViewControllerWithIdentifier:@"HomeVC"];
//    [appDelegate setTopViewwithVC:dashboard];
    [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

@end
