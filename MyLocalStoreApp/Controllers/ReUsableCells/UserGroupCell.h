//
//  UserGroupCell.h
//  TFS
//
//  Created by Hitesh Dhawan on 30/06/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserGroupCell : UITableViewCell
{
//    __weak IBOutlet UILabel *titleLbl;
//    __weak IBOutlet UILabel *descriptionLbl;
//    __weak IBOutlet UISwitch *cellSwitch;
//    __weak IBOutlet UIButton *detailBtn;
   // __weak IBOutlet UILabel *titleLbl;
   // __weak IBOutlet UILabel *descriptionLbl;
    //__weak IBOutlet UISwitch *cellSwitch;
    //__weak IBOutlet UIButton *detailBtn;
    
}
@property (nonatomic, weak) IBOutlet UILabel *titleLbl;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLbl;
@property (nonatomic, weak) IBOutlet UIButton *detailBtn;
@property (nonatomic, weak) IBOutlet UISwitch *cellSwitch;
@property (nonatomic, weak) IBOutlet UILabel *prefrenceLbl;
@property (nonatomic, weak) IBOutlet UISwitch *preferencescellSwitch;
@property (nonatomic, weak) IBOutlet UILabel *systemgroupLbl;
@property (nonatomic, weak) IBOutlet UILabel *separtorLbl;


@end
