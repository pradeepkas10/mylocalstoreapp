//
//  FilterPriceCell.m
//  MyLocalStoreApp
//
//  Created by Surbhi on 16/12/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import "FilterPriceCell.h"

@implementation FilterPriceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)toUpdateUI {
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.rangeSlider = [[MARKRangeSlider alloc] initWithFrame:CGRectMake(40, 55, [UIScreen mainScreen].bounds.size.width-80, 35)];
    self.rangeSlider.backgroundColor = [UIColor whiteColor];
    [self  addSubview:self.rangeSlider];
    
    self.leftLbl.frame = CGRectMake(-6, -22, 45, 22);
    self.leftLbl.textAlignment = NSTextAlignmentCenter;
    self.leftLbl.font = [UIFont systemFontOfSize:12];
    self.leftLbl.backgroundColor = [UIColor whiteColor];
    
    self.rightLbl.frame = CGRectMake(-6, -22, 45, 22);
    self.rightLbl.textAlignment = NSTextAlignmentCenter;
    self.rightLbl.font = [UIFont systemFontOfSize:12];
    self.rightLbl.backgroundColor = [UIColor whiteColor];
    
    [self.rangeSlider.leftThumbView addSubview:self.leftLbl];
    [self.rangeSlider.rightThumbView addSubview:self.rightLbl];
    
}

@end
