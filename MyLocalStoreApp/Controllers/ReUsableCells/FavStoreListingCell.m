//
//  FavStoreListingCell.m
//  MyLocalStoreApp
//
//  Created by Surbhi on 19/12/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import "FavStoreListingCell.h"

@implementation FavStoreListingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.layer.shadowOffset = CGSizeMake(0, 10);
    self.layer.shadowColor = [[UIColor grayColor] CGColor];
    self.layer.shadowRadius = 2;
    self.layer.shadowOpacity = .40;
    CGRect shadowFrame = self.layer.bounds;
    CGPathRef shadowPath = [UIBezierPath bezierPathWithRect:shadowFrame].CGPath;
    self.layer.shadowPath = shadowPath;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
