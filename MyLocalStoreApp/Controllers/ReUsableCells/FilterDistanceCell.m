//
//  FilterDistanceCell.m
//  MyLocalStoreApp
//
//  Created by Surbhi on 16/12/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import "FilterDistanceCell.h"

@implementation FilterDistanceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)toUpdateUI {
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.rangeSlider = [[MARKRangeSlider alloc] initWithFrame:CGRectMake(50, 55, [UIScreen mainScreen].bounds.size.width-100, 35)];
    self.rangeSlider.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.rangeSlider];
    
    self.rightLbl.frame = CGRectMake(-9, -22, 55, 22);
    self.rightLbl.textAlignment = NSTextAlignmentCenter;
    self.rightLbl.font = [UIFont systemFontOfSize:12];
    self.rightLbl.backgroundColor = [UIColor whiteColor];
    
    self.rangeSlider.leftThumbView.frame = CGRectZero;
    
    self.rangeSlider.leftThumbView.hidden = true;
    [self.rangeSlider.rightThumbView addSubview:self.rightLbl];
    
}
@end
