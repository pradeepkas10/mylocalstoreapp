//
//  FilterPriceCell.h
//  MyLocalStoreApp
//
//  Created by Surbhi on 16/12/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MARKRangeSlider.h"


@interface FilterPriceCell : UITableViewCell


@property (nonatomic, strong) MARKRangeSlider *rangeSlider;
@property (weak, nonatomic) IBOutlet UILabel *leftLbl;
@property (weak, nonatomic) IBOutlet UILabel *rightLbl;

-(void)toUpdateUI ;

@end
