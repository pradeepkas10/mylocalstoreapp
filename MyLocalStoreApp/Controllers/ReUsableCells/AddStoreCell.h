//
//  AddStoreCell.h
//  MyLocalStoreApp
//
//  Created by Surbhi on 14/12/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddStoreCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *storeName;
@property (weak, nonatomic) IBOutlet UILabel *storeAddresssCity;
@property (weak, nonatomic) IBOutlet UILabel *storeDistance;
@property (weak, nonatomic) IBOutlet UIButton *btnAddFav;

@end
