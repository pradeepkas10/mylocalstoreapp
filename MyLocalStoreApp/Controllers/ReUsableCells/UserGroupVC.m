//
//  UserGroupVC.m
//  TFS
//
//  Created by Hitesh Dhawan on 01/07/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

#import "UserGroupVC.h"
#import "MyLocalStoreApp.pch"

@interface UserGroupVC ()<UITextFieldDelegate>
{
    
}
@property (nonatomic, strong) ActivationCodeTextField* textField;;
@end

@implementation UserGroupVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    _meldInnBtn.userInteractionEnabled = NO;
     [_meldInnBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    _groupTagLbl.text = _strGroupTag.uppercaseString;
    [self setPinLayout:_pin1Txt];
    [self setPinLayout:_pin2Txt];
    [self setPinLayout:_pin3Txt];
    [self setPinLayout:_pin4Txt];
    [self setPinLayout:_pin5Txt];
    [self setPinLayout:_pin6Txt];
    [self setPinLayout:_pin7Txt];
    [self setPinLayout:_pin8Txt];
    
//    ActivationCodeTextField* textField = [ActivationCodeTextField new];
//    textField.maxCodeLength = 8;
//    textField.customPlaceholder = @"_";
//    textField.delegate = self;
//    [textField setTextColor:[UIColor blackColor]];
//    self.textField = textField;
//    [self.view addSubview:textField];
//
//    
//        [_codeTextField becomeFirstResponder];


}

-(void)setPinLayout:(UITextField *)txt
{
    txt.backgroundColor = [UIColor whiteColor];
    txt.layer.borderColor = THEMECOLOR.CGColor;
    txt.layer.cornerRadius = 5;
    txt.layer.borderWidth = 1;
}
- (void)viewDidAppear:(BOOL)animated
{
    [self.pin1Txt becomeFirstResponder];
}
//- (void)viewDidLayoutSubviews
//{
//    [super viewDidLayoutSubviews];
//    
//    CGRect frame =  self.textField.frame;
//    frame.size.width = self.view.frame.size.width;//[self.textField minWidthTextField];
//    frame.size.height = MAX(self.textField.bounds.size.height, 44.0f);
//    frame.origin.x = 0.0f;//(self.view.bounds.size.width - frame.size.width)/2.0f;
//    frame.origin.y =  133.0f;
//    self.textField.frame = frame;
//    
//}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _pin8Txt){//11} && textField.text.length >0) {
        _meldInnBtn.userInteractionEnabled = YES;
        [_meldInnBtn setTitleColor:[UIColor colorWithRed:207.0/255.0 green:17.0/255.0 blue:47.0/255.0 alpha:1] forState:UIControlStateNormal];

    }
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
        if  (_pin1Txt.text.length >0 && _pin2Txt.text.length >0 && _pin3Txt.text.length >0 && _pin4Txt.text.length >0 && _pin5Txt.text.length >0 && _pin6Txt.text.length >0 && _pin7Txt.text.length >0 && _pin8Txt.text.length >0 )
        {
            _meldInnBtn.userInteractionEnabled = YES;
            [_meldInnBtn setTitleColor:[UIColor colorWithRed:207.0/255.0 green:17.0/255.0 blue:47.0/255.0 alpha:1] forState:UIControlStateNormal];
            //[_meldInnBtn setBackgroundColor:[UIColor blackColor]];
        }
    else
    {	
        _meldInnBtn.userInteractionEnabled = NO;
        [_meldInnBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        //[_meldInnBtn setBackgroundColor:[UIColor lightGrayColor]];
    }
    
    return YES;
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    
//    NSString* acceptableCharacters = @"0123456789";
//    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:acceptableCharacters] invertedSet];
//    
//    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
//    
//    return [string isEqualToString:filtered];
//}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==self.pin1Txt)
    {
        NSInteger insertDelta = string.length - range.length;
        if (self.pin1Txt.text.length + insertDelta > 1  && range.length == 0)
        {
            [self.pin2Txt becomeFirstResponder];
            if(self.pin2Txt.text.length ==0)
                return YES;
            else
                return NO;
        }
    }
    if (textField==self.pin2Txt)
    {
        NSInteger insertDelta = string.length - range.length;
        
        if (self.pin2Txt.text.length + insertDelta > 1  && range.length == 0)
        {
            [self.pin3Txt becomeFirstResponder];
            if(self.pin3Txt.text.length ==0)
                return YES;
            else
                return NO;
        }
        if (self.pin2Txt.text.length == 1 && [string isEqualToString:@""])
        {
            self.pin2Txt.text = @"";
            [self.pin1Txt becomeFirstResponder];
            return NO;
        }
    }
    if (textField==self.pin3Txt)
    {
        NSInteger insertDelta = string.length - range.length;
        
        if (self.pin3Txt.text.length + insertDelta > 1  && range.length == 0)
        {
            [self.pin4Txt becomeFirstResponder];
            if(self.pin4Txt.text.length ==0)
                return YES;
            else
                return NO;
        }
        if (self.pin3Txt.text.length == 1 && [string isEqualToString:@""])
        {
            self.pin3Txt.text = @"";
            [self.pin2Txt becomeFirstResponder];
            return NO;
        }
    }
    if (textField==self.pin4Txt)
    {
        NSInteger insertDelta = string.length - range.length;
        
        if (self.pin4Txt.text.length + insertDelta > 1  && range.length == 0)
        {
            [self.pin5Txt becomeFirstResponder];
            if(self.pin5Txt.text.length ==0)
                return YES;
            else
                return NO;
        }
        if (self.pin4Txt.text.length == 1 && [string isEqualToString:@""])
        {
            self.pin4Txt.text = @"";
            [self.pin3Txt becomeFirstResponder];
            return NO;
        }
    }
    if (textField==self.pin5Txt)
    {
        NSInteger insertDelta = string.length - range.length;
        
        if (self.pin5Txt.text.length + insertDelta > 1  && range.length == 0)
        {
            [self.pin6Txt becomeFirstResponder];
            if(self.pin6Txt.text.length ==0)
                return YES;
            else
                return NO;
        }
        if (self.pin5Txt.text.length == 1 && [string isEqualToString:@""])
        {
            self.pin5Txt.text = @"";
            [self.pin4Txt becomeFirstResponder];
            return NO;
        }
    }
    if (textField==self.pin6Txt)
    {
        NSInteger insertDelta = string.length - range.length;
        
        if (self.pin6Txt.text.length + insertDelta > 1  && range.length == 0)
        {
            [self.pin7Txt becomeFirstResponder];
            if(self.pin7Txt.text.length ==0)
                return YES;
            else
                return NO;
        }
        if (self.pin6Txt.text.length == 1 && [string isEqualToString:@""])
        {
            self.pin6Txt.text = @"";
            [self.pin5Txt becomeFirstResponder];
            return NO;
        }
    }
    if (textField==self.pin7Txt)
    {
        NSInteger insertDelta = string.length - range.length;
        
        if (self.pin7Txt.text.length + insertDelta > 1  && range.length == 0)
        {
            [self.pin8Txt becomeFirstResponder];
            if(self.pin8Txt.text.length ==0)
            {//[textField resignFirstResponder];
                return YES;
            }
            else
                return NO;
        }
        if (self.pin7Txt.text.length == 1 && [string isEqualToString:@""])
        {
            self.pin7Txt.text = @"";
            [self.pin6Txt becomeFirstResponder];
            return NO;
        }
    }
    if (textField == self.pin8Txt)
    {
        NSInteger insertDelta = string.length - range.length;
        if (self.pin8Txt.text.length == 1 && [string isEqualToString:@""])
        {
            self.pin8Txt.text = @"";
            [self.pin7Txt becomeFirstResponder];
            return NO;
        }
        if (self.pin8Txt.text.length + insertDelta > 1  && range.length == 0)
        {
            [self.pin8Txt resignFirstResponder];
             _meldInnBtn.userInteractionEnabled = YES;
            return NO;
        }
    }
    if (textField.tag==102)
    {
        // All digits entered
        if (range.location == 10)
        {
            return NO;
        }
        
        // Reject appending non-digit characters
        if (range.length == 0 &&
            ![[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[string characterAtIndex:0]])
        {
            return NO;
        }
    }
    return YES;
}
    /*
    if (textField == _pin1Txt)
    {
        if (range.location == 1)
        {
            [self.pin1Txt resignFirstResponder];
            [self.pin2Txt becomeFirstResponder];
        }
    }
    else if (textField == _pin2Txt)
    {
        if (range.location == 1)
        {
            [self.pin2Txt resignFirstResponder];
            [self.pin3Txt becomeFirstResponder];
        }
    }
    else if (textField == _pin3Txt)
    {
        if (range.location == 1)
        {
            [self.pin3Txt resignFirstResponder];
            [self.pin4Txt becomeFirstResponder];
        }
    }
    else if (textField == _pin4Txt)
    {
        if (range.location == 1)
        {
            [self.pin4Txt resignFirstResponder];
            [self.pin5Txt becomeFirstResponder];
        }
    }
    else if (textField == _pin5Txt)
    {
        if (range.location == 1)
        {
            [self.pin5Txt resignFirstResponder];
            [self.pin6Txt becomeFirstResponder];
        }
    }
    else if (textField == _pin6Txt)
    {
        if (range.location == 1)
        {
            [self.pin6Txt resignFirstResponder];
            [self.pin7Txt becomeFirstResponder];
        }
    }
    else if (textField == _pin7Txt)
    {
        if (range.location == 1)
        {
            [self.pin7Txt resignFirstResponder];
            [self.pin8Txt becomeFirstResponder];
        }
    }
    else if (textField == _pin8Txt)
    {
        if (range.location == 1)
        {
            [self.pin8Txt resignFirstResponder];
        }
    }
    return YES;
     */


- (IBAction)saveUserDataBtnClick:(id)sender
{
    NSString * strMobile, *strDate, *strName, *strCity, *strEmail;
    if ([[_dataDict allKeys] containsObject:@"Msn"]) {
        strMobile = [_dataDict objectForKey:@"Msn"];
       
    }
    if ([[_dataDict allKeys] containsObject:@"DateOfBirth"]) {
        strDate = [_dataDict objectForKey:@"DateOfBirth"];
        
    }
    
    if ([[_dataDict allKeys] containsObject:@"Name"]) {
        strName = [_dataDict objectForKey:@"Name"];
    }
    
    
    if ([[_dataDict allKeys] containsObject:@"City"]) {
        strCity = [_dataDict objectForKey:@"City"];
    }
    
    
    
    if ([[_dataDict allKeys] containsObject:@"Email"]) {
        strEmail = [_dataDict objectForKey:@"Email"];
    }

    
    [appDelegate startIndicator];
    
    NSString *strURL = [NSString stringWithFormat:@"%@user", BASEURL];
    
    NSString *_dateString = [HelperClass getDateString];
    
    // Generate signature
    NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId, SALT];
    NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
    
    ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:strURL]];
    [request setRequestMethod:@"PUT"];
    request.name = @"SaveChanges";
    [request setTimeOutSeconds:30.0];
    [request setDelegate:self];
    
    [request addRequestHeader:@"Accept" value:@"application/json"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request addRequestHeader:@"X-Liquid-TimeStamp" value:_dateString];
    [request addRequestHeader:@"X-Liquid-Signature" value:signature];
    
    //NSString *pin = [NSString stringWithFormat:@"%@",_txtPin.text];
    //	NSString *add = (_txtAddress.text) ? [NSString stringWithFormat:@"%@",_txtAddress.text] : @" ";
    
    NSMutableDictionary *paramDic;
    NSMutableDictionary * usergroupDict; //;= [[NSDictionary alloc]init];;
    NSString * userCode = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",_pin1Txt.text,_pin2Txt.text,_pin3Txt.text,_pin4Txt.text,_pin5Txt.text,_pin6Txt.text,_pin7Txt.text,_pin8Txt.text];
    usergroupDict = [[NSMutableDictionary alloc] initWithDictionary:@{@"GroupTag":_strGroupTag,
                                                                 @"IsUserMember":@"true",
                                                                 @"UserCode":userCode}];
    NSArray * sendingArray = [NSArray arrayWithObject:usergroupDict];

    if ([strEmail isEqualToString:@""]  || strEmail == nil ){
        paramDic = [[NSMutableDictionary alloc] initWithDictionary:@{@"UserID":appDelegate.strUserId,
                                                                     @"Name":strName,
                                                                     @"DateOfBirth":strDate,
                                                                     @"City":strCity,
                                                                     @"UserGroups":sendingArray
                                                                     }];
    }
    else{
        paramDic = [[NSMutableDictionary alloc] initWithDictionary:@{@"UserID":appDelegate.strUserId,
                                                                     @"Name":strName,
                                                                     @"DateOfBirth":strDate,
                                                                     @"Email":strEmail,
                                                                     @"City":strCity,
                                                                     @"UserGroups":sendingArray
                                                                     }];
    }
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
    [request setPostBody:[NSMutableData dataWithData:data]];
    
    [request startAsynchronous];

    
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [appDelegate stopIndicator];
    NSDictionary *dicUser = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];

    if ([[dicUser allKeys] containsObject:@"ResponseStatus"]) {
        NSDictionary *tempDict = [dicUser objectForKey:@"ResponseStatus"];
        
        if ([[tempDict allKeys] containsObject:@"Message"]) {
        
       //NSString * strErrorMsg = [tempDict objectForKey:@"Message"];
        
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"My Local Stores" message:@"The code is invalid" preferredStyle:UIAlertControllerStyleAlert];
        
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Ok"
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     
                                                                     _pin1Txt.text = @"";
                                                                     _pin2Txt.text = @"";
                                                                     _pin3Txt.text = @"";
                                                                     _pin4Txt.text = @"";
                                                                     _pin5Txt.text = @"";
                                                                     _pin6Txt.text = @"";
                                                                     _pin7Txt.text = @"";
                                                                     _pin8Txt.text = @"";
                                                                     _meldInnBtn.userInteractionEnabled = NO;
                                                                     [_meldInnBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                                                                     [_pin1Txt becomeFirstResponder];

                                                                 }];
        
            [alertController addAction:cancelAction];
            [self presentViewController:alertController animated:YES completion:nil];
            CFRunLoopWakeUp(CFRunLoopGetCurrent());
        }
    }
    else{
        [self dismissViewControllerAnimated:NO completion:^{
            
            
        }];
    }
  
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    //    [SVProgressHUD dismiss];
    [appDelegate stopIndicator];
}
- (IBAction)backBtnClick:(id)sender {
    [self dismissViewControllerAnimated:NO completion:^{
        
        
    }];
}

- (BOOL)prefersStatusBarHidden{
    return true;
}


@end
