//
//  FilterTimeCell.h
//  MyLocalStoreApp
//
//  Created by Surbhi on 15/12/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MARKRangeSlider.h"

@interface FilterTimeCell : UITableViewCell{
    
}


@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (nonatomic, strong) MARKRangeSlider *rangeSlider;
@property (weak, nonatomic) IBOutlet UILabel *leftLbl;
@property (weak, nonatomic) IBOutlet UILabel *rightLbl;

-(void)toUpdateUI ;


@end
