//
//  FavStoreListingCell.h
//  MyLocalStoreApp
//
//  Created by Surbhi on 19/12/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavStoreListingCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@property (weak, nonatomic) IBOutlet UIButton *btnFav;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewForLeftConstraint;

@end
