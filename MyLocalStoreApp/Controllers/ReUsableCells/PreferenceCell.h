//
//  PreferenceCell.h
//  MyLocalStoreApp
//
//  Created by Surbhi on 27/03/18.
//  Copyright © 2018 SurbhiV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreferenceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UISwitch *prefSwitch;
@property (weak, nonatomic) IBOutlet UILabel *prefDescriptionLabel;


@end
