//
//  AllStoreCollectionCell.h
//  MyLocalStoreApp
//
//  Created by Surbhi on 14/12/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllStoreCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UIImageView *imgProduct;


@end
