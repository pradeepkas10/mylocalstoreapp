//
//  UserGroupVC.h
//  TFS
//
//  Created by Hitesh Dhawan on 01/07/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivationCodeTextField.h"
@interface UserGroupVC : UIViewController
@property(weak,nonatomic) NSDictionary * dataDict;
@property(weak,nonatomic) NSString * strGroupTag;
@property (weak, nonatomic) IBOutlet UITextField *pin1Txt;
@property (weak, nonatomic) IBOutlet UITextField *pin2Txt;
@property (weak, nonatomic) IBOutlet UITextField *pin3Txt;
@property (weak, nonatomic) IBOutlet UITextField *pin4Txt;
@property (weak, nonatomic) IBOutlet UITextField *pin5Txt;
@property (weak, nonatomic) IBOutlet UITextField *pin6Txt;
@property (weak, nonatomic) IBOutlet UITextField *pin7Txt;
@property (weak, nonatomic) IBOutlet UITextField *pin8Txt;
@property (weak, nonatomic) IBOutlet ActivationCodeTextField *codeTextField;
@property (weak, nonatomic) IBOutlet UIButton *meldInnBtn;
@property (weak, nonatomic) IBOutlet UILabel *groupTagLbl;

- (IBAction)saveUserDataBtnClick:(id)sender;


@end
