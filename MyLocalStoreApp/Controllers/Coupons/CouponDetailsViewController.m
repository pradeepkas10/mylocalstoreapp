//
//  DetailsViewController.m
//  TwentyFourSeven
//
//  Created by Hitesh Dhawan on 06/10/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "CouponDetailsViewController.h"
#import "MyLocalStoreApp.pch"
#import <Photos/Photos.h>

@interface CouponDetailsViewController () <UIPopoverPresentationControllerDelegate>
{
    UIScrollView *_scrollView;
    UIActivityIndicatorView *refreshActivityView;
    UILabel *noContentLabel;
    FLAnimatedImageView *imageViewForGIF ;
    UIImageViewAligned *imageViewSelectiion;
    NSMutableDictionary *imageSizeArray;
}
@end

@implementation CouponDetailsViewController 

- (void)viewDidLoad {
    
    [appDelegate startIndicator];
    appDelegate.needAPIRefresh = NO;
    [UIApplication sharedApplication].statusBarHidden = YES;

    [self.btnLocateStore addTarget:self action:@selector(LocateStore:) forControlEvents:UIControlEventTouchUpInside];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotificationReceived:) name:@"SharingCompleted" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GoHome:) name:@"OfferCancelled" object:nil];

    [super viewDidLoad];
    imageSizeArray = [NSMutableDictionary dictionary];
    
    if (_scrollView != nil) {
        [_scrollView removeFromSuperview];
        _scrollView = nil;
    }
    
    appDelegate.dicOffers = [[NSUserDefaults standardUserDefaults]objectForKey:DICOffers];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];

    NSArray * arr_sections = [NSArray arrayWithArray:[self getContentFromDictionary:appDelegate.dicOffers]];
    
    if (arr_sections.count == 0 && status){
        [self updateOffers];
    }
    else{
        self.initialIndex = self.selectedPageOnDetail;
        [self updateCustomPageViewController];
        [refreshActivityView removeFromSuperview];
        _btnRefresh.hidden = NO;
        refreshActivityView.hidden = YES;
        [appDelegate stopIndicator];
    }
    
    MenuItemsTVC *mo = (MenuItemsTVC *)[[self.sideMenuController sideMenu] menuViewController];
    mo.selectedPageOnDetail = 0;
}

-(void)NotificationReceived:(NSNotification *)noti{
    // first show animation after 5 seconds update API
    NSDictionary *dict = noti.object;
    NSString *valu = dict[@"shouldAnimate"];
    if ([valu isEqualToString:@"1"]){
        [self playGif];
        appDelegate.needAPIRefresh = YES;
    }
    else {
        if (_scrollView != nil) {
            [_scrollView removeFromSuperview];
            _scrollView = nil;
        }
        [appDelegate startIndicator];
        [self performSelector:@selector(updateOffers) withObject:nil afterDelay:2.0];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleDefault;
//}

#pragma mark
#pragma mark - SHOW GIF
-(void)playGif
{
    FLAnimatedImage *animatedImage1;
    
    NSInteger index = self.pageControl.currentPage;
    CGRect frame = CGRectMake(0, 64, _scrollView.frame.size.width, _scrollView.frame.size.height - 130);

    
    if (([[[self.arrContent objectAtIndex:index] objectForKey:@"Type"] isEqualToString:@"Primary"] || [[[self.arrContent objectAtIndex:index] objectForKey:@"Type"] isEqualToString:@"Reward"]) && [[[self.arrContent objectAtIndex:index] objectForKey:@"TopText"] rangeOfString:@"\n"].location != NSNotFound) {
        frame.size.height = _scrollView.frame.size.height - 150;
    }
    if ([[[self.arrContent objectAtIndex:index] objectForKey:@"Type"] isEqualToString:@"Survey"] || [[[self.arrContent objectAtIndex:index] objectForKey:@"Type"] isEqualToString:@"Html"] || ![[[self.arrContent objectAtIndex:index] allKeys] containsObject:@"BarcodeUrl"]) {
        frame.size.height = _scrollView.frame.size.height;
    }
    if (![[[self.arrContent objectAtIndex:index] objectForKey:@"Type"] isEqualToString:@"LimitedAmount"] && [[[self.arrContent objectAtIndex:index] objectForKey:@"TopText"] isEqualToString:@""]) {
        frame.size.height = _scrollView.frame.size.height - 130;
    }
    if ([[[self.arrContent objectAtIndex:index] objectForKey:@"BarcodeUrl"]  isEqual: @""] || [[self.arrContent objectAtIndex:index] objectForKey:@"BarcodeUrl"] == nil) {
        frame.size.height = _scrollView.frame.size.height;
    }
    
    
    if ([[[self.arrContent objectAtIndex:index] objectForKey:@"Type"] isEqualToString:@"LimitedAmount"] || [[[self.arrContent objectAtIndex:index] objectForKey:@"TopText"] isEqualToString:@""]) {
        frame.size.height = frame.size.height;
    }
    
    
    //commenting by surbhi
    else if ([[[self.arrContent objectAtIndex:index] objectForKey:@"TopText"] rangeOfString:@"\n"].location != NSNotFound && [[self.arrContent objectAtIndex:index] objectForKey:@"TopText"] != nil) {
        frame.size.height = frame.size.height - 20;
    }
    else if ([[self.arrContent objectAtIndex:index] objectForKey:@"TopText"] == nil){
        frame.size.height = frame.size.height;
    }
    else
    {
        if (([[self.arrContent objectAtIndex:index] objectForKey:@"TopText"] != nil) && ([[self.arrContent objectAtIndex:index] objectForKey:@"SubText"] != nil && ![[[self.arrContent objectAtIndex:index] objectForKey:@"SubText"] isEqualToString: @""] && [[[self.arrContent objectAtIndex:index] objectForKey:@"SubText"] length] > 0)){
            frame.size.height = frame.size.height - 20;
        }
        else{
            frame.size.height = frame.size.height;
        }
    }
    
    NSDictionary *dc = [imageSizeArray valueForKey:[NSString stringWithFormat:@"%ld",(long)index]];
    float wide = [[dc objectForKey:@"width"] floatValue];
    float high = [[dc objectForKey:@"height"] floatValue];
    float newheight;
    
    if (wide > frame.size.width)
    {
        float ratio =  wide / frame.size.width;
        newheight = high / ratio;
        frame.size.height = newheight;
    }
    
    
    NSString *imageName;
    if (frame.size.height >= _scrollView.frame.size.height - 10)
    {
        imageName = @"big_bite_big";
    }
    else{
        imageName = @"big-bite-2";
    }

    NSURL *url1 = [[NSBundle mainBundle] URLForResource:imageName withExtension:@"gif"];
    NSData *data1 = [NSData dataWithContentsOfURL:url1];

    animatedImage1 = [FLAnimatedImage animatedImageWithGIFData:data1];
    imageViewForGIF = [[FLAnimatedImageView alloc] init];
    imageViewForGIF.animatedImage = animatedImage1;

    imageViewForGIF.backgroundColor = REDTHEMECOLOR;
    imageViewForGIF.alpha = 0.8;
    imageViewForGIF.contentMode = UIViewContentModeScaleAspectFit;
    imageViewForGIF.frame = frame;
    [appDelegate.window addSubview:imageViewForGIF];
    [self performSelector:@selector(removeAnimatedGIF) withObject:self afterDelay:4.0];
}

-(void)removeAnimatedGIF
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [imageViewForGIF removeFromSuperview];
        
        if (_scrollView != nil) {
            [_scrollView removeFromSuperview];
            _scrollView = nil;
        }
    });
    // Update offers now
    [appDelegate startIndicator];
    [self updateOffers];
}


#pragma mark -
#pragma mark API Methods

- (void)updateOffers {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:DICOffers];
        appDelegate.dicOffers = nil;

//        [self showLodingIndicator:YES];
        
        NSString *urlString = [NSString stringWithFormat:@"%@content/?UserId=%@", BASEURL, appDelegate.strUserId];
        NSString *_dateString = [HelperClass getDateString];

            // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId, SALT];
            //NSString *source = [NSString stringWithFormat:@"%@%@", _dateString, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:100.0];
        [request setDelegate:self];

        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        [request startAsynchronous];
        
    }
    else {
        
        [appDelegate stopIndicator];

        [refreshActivityView removeFromSuperview];
        refreshActivityView.hidden = YES;
        _btnRefresh.hidden = NO;
        [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error Occured" andMessage:@"No internet connection." andButtonTitle:@"Cancel"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    
    [refreshActivityView removeFromSuperview];
    _btnRefresh.hidden = NO;
    refreshActivityView.hidden = YES;
    [appDelegate stopIndicator];
    
    appDelegate.dicOffers = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    [NSUserDefaults.standardUserDefaults setObject:appDelegate.dicOffers forKey:DICOffers];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self updatedataWithdict:appDelegate.dicOffers];
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    
    [refreshActivityView removeFromSuperview];
    _btnRefresh.hidden = NO;
    refreshActivityView.hidden = YES;
    [appDelegate stopIndicator];
    
    [CustomAlertController showAlertOnController:self withAlertType:Simple withTitle:@"Error occurred" andMessage:request.error.localizedDescription andButtonTitle:@"Cancel"];
}
-(void)updatedataWithdict:(NSDictionary *)dict_offers
{
    [self getContentFromDictionary:dict_offers];
    [self updateCustomPageViewController];
}

-(NSArray *)getContentFromDictionary:(NSDictionary *)dict_offers{
    NSArray * arr_sections = appDelegate.dicOffers[@"Sections"];
    
    for (int i=0; i<arr_sections.count; i++) {
        NSDictionary *dicContent = arr_sections[i];
        if ([dicContent[@"Name"] isEqualToString:@"Coupons"] && [self.title isEqualToString:@"COUPONS"]) {
            
            self.arrContent = [NSArray arrayWithArray:[dicContent objectForKey:@"Content"]];
        }
        else if ([dicContent[@"Name"] isEqualToString:@"Loyalty"] && [self.title isEqualToString:@"LOYALTY"])
        {
            self.arrContent = [NSArray arrayWithArray:[dicContent objectForKey:@"Content"]];
        }
        else if ([dicContent[@"Name"] isEqualToString:@"Rewards"] && [self.title isEqualToString:@"REWARDS"])
        {
            self.arrContent = [NSArray arrayWithArray:[dicContent objectForKey:@"Content"]];
        }
        else if ([dicContent[@"Name"] isEqualToString:@"Offers"] && [self.title isEqualToString:@"OFFERS"])
        {
            self.arrContent = [NSArray arrayWithArray:[dicContent objectForKey:@"Content"]];
        }
    }
    
    return arr_sections;
}



#pragma mark - SHARE BUTTON
- (IBAction)shareButtonPressed:(id)sender {

    NSInteger index = self.pageControl.currentPage;
    ReferViewController *dashboard = [mainStoryboard instantiateViewControllerWithIdentifier:@"referController"];
    dashboard.isShareView = YES;
    dashboard.couponID = [NSString stringWithFormat:@"%@",[[_arrContent objectAtIndex:index]valueForKey:@"ContentId" ]];
    dashboard.campeignID = [NSString stringWithFormat:@"%@",[[_arrContent objectAtIndex:index]valueForKey:@"ScheduleId" ]];
    [self addChildViewController:dashboard];
    [dashboard didMoveToParentViewController:self];
    dashboard.view.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         dashboard.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:^(BOOL finished){
                     }];
    [self.view addSubview:dashboard.view];
}


#pragma mark -
#pragma mark Custom Methods

- (void)updateCustomPageViewController {
    
    if (_scrollView.subviews.count > 0) {
        for (UIView *vx in _scrollView.subviews) {
            [vx removeFromSuperview];
        }
    }

    [self performSelectorOnMainThread:@selector(createCustomPageViewController) withObject:nil waitUntilDone:1.5];
    
//    [self createCustomPageViewController];
}


- (void)createCustomPageViewController
{
    [self.view layoutIfNeeded];
    [self.scrollSuperView layoutIfNeeded];
    
    [appDelegate stopIndicator];
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, (SCREEN_WIDTH ), SCREEN_HEIGHT - 80)];
    _scrollView.delegate = self;
    _scrollView.scrollEnabled = YES;
    _scrollView.userInteractionEnabled = YES;
    _scrollView.pagingEnabled = YES;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    
    self.scrollSuperView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.scrollSuperView.layer.shadowOpacity = 1;
    self.scrollSuperView.layer.shadowOffset = CGSizeZero;
    self.scrollSuperView.layer.shadowRadius = 2;
    self.scrollSuperView.layer.masksToBounds = NO;
    [self.scrollSuperView addSubview:_scrollView];
    
    _scrollView.contentSize = CGSizeMake(self.arrContent.count * (SCREEN_WIDTH ), SCREEN_HEIGHT - 80);
    int xAxis = 0;
    
    [self.scrollSuperView setClipsToBounds:YES];
    
    if (imageSizeArray.count >0){
        [imageSizeArray removeAllObjects];
    }
    
    for (int i=0; i<_arrContent.count; i++) {
        
        @autoreleasepool {
            
            UIView *viewContent = [[UIView alloc] initWithFrame:CGRectMake(xAxis, 0, _scrollView.frame.size.width, _scrollView.frame.size.height)];
            [viewContent setTag:i+1];
            [_scrollView addSubview:viewContent];
            [viewContent layoutIfNeeded];
            
            UIImageViewAligned *imageView = [[UIImageViewAligned alloc] initWithFrame:CGRectMake(0, 0, viewContent.frame.size.width,  viewContent.frame.size.height - 130)];
            [imageView setTag:i+5000];
            [imageView setContentMode:UIViewContentModeScaleAspectFit];
            imageView.alignTop = YES;
            imageView.clipsToBounds = YES;
            [viewContent addSubview:imageView];
            

            if (([[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"Primary"] || [[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"Reward"]) && [[[self.arrContent objectAtIndex:i] objectForKey:@"TopText"] rangeOfString:@"\n"].location != NSNotFound) {
                [imageView setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  viewContent.frame.size.height - 150)];
            }
            
            
            if ([[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"Survey"] || [[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"Html"] || ![[[self.arrContent objectAtIndex:i] allKeys] containsObject:@"BarcodeUrl"]) {
                
                [imageView setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  viewContent.frame.size.height )];
            }
            
            if (![[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"LimitedAmount"] && [[[self.arrContent objectAtIndex:i] objectForKey:@"TopText"] isEqualToString:@""]) {
                //changes done by jitender
//                [imageView setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  imageView.frame.size.height+17)];
                [imageView setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  viewContent.frame.size.height - 130)];

            }
            
            if ([[[self.arrContent objectAtIndex:i] objectForKey:@"BarcodeUrl"]  isEqual: @""] || [[self.arrContent objectAtIndex:i] objectForKey:@"BarcodeUrl"] == nil) {
                [imageView setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  viewContent.frame.size.height )];
                
            }

            UILabel *lblExpireDate = [[UILabel alloc] initWithFrame:CGRectMake(0, imageView.frame.size.height+5, viewContent.frame.size.width, 17)];
            if ([[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"LimitedAmount"] || [[[self.arrContent objectAtIndex:i] objectForKey:@"TopText"] isEqualToString:@""]) {
                [imageView setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  imageView.frame.size.height )];

                lblExpireDate.frame = CGRectMake(lblExpireDate.frame.origin.x, lblExpireDate.frame.origin.y, lblExpireDate.frame.size.width, 0);
            }
            //commenting by surbhi
            else if ([[[self.arrContent objectAtIndex:i] objectForKey:@"TopText"] rangeOfString:@"\n"].location != NSNotFound && [[self.arrContent objectAtIndex:i] objectForKey:@"TopText"] != nil) {
                [imageView setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  imageView.frame.size.height - 20 )];

                lblExpireDate.frame = CGRectMake(lblExpireDate.frame.origin.x,imageView.frame.size.height + 5 , lblExpireDate.frame.size.width, 50);//lblExpireDate.frame.origin.y
            }
            else if ([[self.arrContent objectAtIndex:i] objectForKey:@"TopText"] == nil){
                [imageView setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  imageView.frame.size.height )];

                lblExpireDate.frame = CGRectMake(lblExpireDate.frame.origin.x, lblExpireDate.frame.origin.y, lblExpireDate.frame.size.width, 0);
            }
            else
            {
                if (([[self.arrContent objectAtIndex:i] objectForKey:@"TopText"] != nil) && ([[self.arrContent objectAtIndex:i] objectForKey:@"SubText"] != nil && ![[[self.arrContent objectAtIndex:i] objectForKey:@"SubText"] isEqualToString: @""] && [[[self.arrContent objectAtIndex:i] objectForKey:@"SubText"] length] > 0)){

                    [imageView setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  imageView.frame.size.height - 20)];
                    
                    lblExpireDate.frame = CGRectMake(lblExpireDate.frame.origin.x,imageView.frame.size.height + 5 , lblExpireDate.frame.size.width, 45);
                }
                else{
                    [imageView setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  imageView.frame.size.height)];
                }
            }
            
            if ([[self.arrContent objectAtIndex:i] objectForKey:@"SubText"] == nil || [[[self.arrContent objectAtIndex:i] objectForKey:@"SubText"] isEqualToString: @""] || [[[self.arrContent objectAtIndex:i] objectForKey:@"SubText"] length] == 0){
                [lblExpireDate setFont:[UIFont systemFontOfSize:15.0]];
                [lblExpireDate setTextAlignment:NSTextAlignmentCenter];
                [lblExpireDate setTag:i+6000];
                [lblExpireDate setNumberOfLines:0];
                [lblExpireDate setText:[[self.arrContent objectAtIndex:i] objectForKey:@"TopText"]];
                [viewContent addSubview:lblExpireDate];
            }
            else {
                [lblExpireDate setFont:[UIFont systemFontOfSize:15.0]];
                [lblExpireDate setTextAlignment:NSTextAlignmentCenter];
                [lblExpireDate setTag:i+6000];
                [lblExpireDate setNumberOfLines:0];
                
                NSString *couponSTr = [[self.arrContent objectAtIndex:i] objectForKey:@"TopText"];
                NSString *subtextStr = [[self.arrContent objectAtIndex:i] objectForKey:@"SubText"];
                NSString *finalStr = [NSString stringWithFormat:@"%@\n%@",couponSTr,subtextStr];
                
                [lblExpireDate setText:finalStr];
                [viewContent addSubview:lblExpireDate];
            }
            
            
            
            UIImageView *imageViewBarcode = [[UIImageView alloc] initWithFrame:CGRectMake((viewContent.frame.size.width - (viewContent.frame.size.width - 80))/2, lblExpireDate.frame.origin.y + lblExpireDate.frame.size.height+5, viewContent.frame.size.width - 80, 70)];
            [imageViewBarcode setTag:i+7000];
            [imageViewBarcode setContentMode:UIViewContentModeScaleAspectFit];
            [viewContent addSubview:imageViewBarcode];
            [imageViewBarcode sd_setImageWithURL:[NSURL URLWithString:[[self.arrContent objectAtIndex:i] objectForKey:@"BarcodeUrl"]]];
            
            
            UILabel *lblCouponText = [[UILabel alloc] initWithFrame:CGRectMake(0, imageViewBarcode.frame.origin.y + imageViewBarcode.frame.size.height+ 5, viewContent.frame.size.width, 20)];
            [lblCouponText setTag:i+8000];
            [lblCouponText setFont:[UIFont systemFontOfSize:15.0]];
            [lblCouponText setTextAlignment:NSTextAlignmentCenter];
            [lblCouponText setText:[[self.arrContent objectAtIndex:i] objectForKey:@"BottomText"]];
            [viewContent addSubview:lblCouponText];
            
            UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] init];
            activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            activityView.color = [UIColor grayColor];
            activityView.tag = i+9000;
            activityView.center = CGPointMake(CGRectGetMidX(viewContent.bounds), CGRectGetMidY(viewContent.bounds));
            [viewContent addSubview:activityView];
            
            
            if ([[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"LimitedAmount"]) {
                
                UILabel *lblAmounLeft = [[UILabel alloc] initWithFrame:CGRectMake(0, lblCouponText.frame.origin.y + lblCouponText.frame.size.height, viewContent.frame.size.width, 17)];
                [lblAmounLeft setTag:i+10000];
                [lblAmounLeft setFont:[UIFont systemFontOfSize:15.0]];
                lblAmounLeft.textAlignment = NSTextAlignmentCenter;
                lblAmounLeft.text = [NSString stringWithFormat:@"%d coupons left", [[[self.arrContent objectAtIndex:i] objectForKey:@"AmountLeft"] intValue]];
                [viewContent addSubview:lblAmounLeft];
            }
            
            [activityView startAnimating];
            
            if ([[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"Survey"] || [[[self.arrContent objectAtIndex:i] objectForKey:@"Type"] isEqualToString:@"Html"]) {
                
                [imageView sd_setImageWithURL:[NSURL URLWithString:[[self.arrContent objectAtIndex:i] objectForKey:@"TopImageUrl"]] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    
                    float wide = image.size.width;
                    float high = image.size.height;
                    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys: [NSString stringWithFormat:@"%f",wide],@"width",[NSString stringWithFormat:@"%f",high],@"height", nil];
                    //                    [imageSizeArray insertObject:dict atIndex:i];
                    [imageSizeArray setValue:dict forKey:[NSString stringWithFormat:@"%ld",(long)i]];
                    
                    [activityView stopAnimating];
                    [activityView hidesWhenStopped];
                }];
            } else {
                [imageView sd_setImageWithURL:[NSURL URLWithString:[[self.arrContent objectAtIndex:i] objectForKey:@"TopImageUrl"]] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    
                    float wide = image.size.width;
                    float high = image.size.height;
                    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys: [NSString stringWithFormat:@"%f",wide],@"width",[NSString stringWithFormat:@"%f",high],@"height", nil];
                    [imageSizeArray setValue:dict forKey:[NSString stringWithFormat:@"%ld",(long)i]];
                    
                    [activityView stopAnimating];
                    [activityView hidesWhenStopped];
                }];
            }
            
            UITapGestureRecognizer *tapGuesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewContentTapGuesture:)];
            tapGuesture.numberOfTapsRequired = 1;
            [viewContent addGestureRecognizer:tapGuesture];
            
            xAxis += _scrollView.frame.size.width;
        }
    }
    
    if (_arrContent.count == 0) {
        
        UILabel *noLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, (self.scrollSuperView.frame.size.height - 50)/2, self.scrollSuperView.frame.size.width - 20, 50)];
        noLabel.text = @"You have no content here now";
        noLabel.textColor = [UIColor grayColor];
        noLabel.font = [UIFont fontWithName:BertholdRegular size:16.0];
        noLabel.textAlignment = NSTextAlignmentCenter;
        [self.scrollSuperView addSubview:noLabel];
    }
    else{
        
    }
    
    if (self.arrContent.count>0) {

        if ([[[self.arrContent objectAtIndex:self.initialIndex] objectForKey:@"ShareType"] isEqualToString:@"Shareable"] && [self.title isEqualToString:@"COUPONS"]){
             self.btnShareView.hidden = NO;
        }
        else {
            self.btnShareView.hidden = YES;
        }
    }
    else {
        self.btnShareView.hidden = YES;
    }
    
    _scrollView.contentOffset = CGPointMake(_scrollView.frame.size.width * self.initialIndex, 0);
    self.pageControl.numberOfPages = self.arrContent.count;

    self.pageControl.currentPage = self.initialIndex;
    [self.scrollSuperView bringSubviewToFront:self.pageControl];
}

- (void)viewContentTapGuesture:(UITapGestureRecognizer *)guesture
{
    UIView *viewContent = (UIView *)guesture.view;
    
    if (viewContent.tag >= 100) {
        
        UIImageView *imageCoupon = (UIImageView *)[self.view viewWithTag:((viewContent.tag/100) + 5000 -1)];
        
        if (![[[self.arrContent objectAtIndex:imageCoupon.tag-5000] allKeys] containsObject:@"BarcodeUrl"]) {
            [imageCoupon setFrame:CGRectMake(imageCoupon.frame.origin.x, imageCoupon.frame.origin.y, imageCoupon.frame.size.width,  viewContent.frame.size.height-20)];
        }
        else if (![[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"Type"] isEqualToString:@"LimitedAmount"] && [[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"TopText"] isEqualToString:@""] && [[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"Type"] isEqualToString:@"Primary"]) {
            [imageCoupon setFrame:CGRectMake(imageCoupon.frame.origin.x, imageCoupon.frame.origin.y, imageCoupon.frame.size.width,  imageCoupon.frame.size.height)];
        }
        else if (([[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"Type"] isEqualToString:@"Primary"] || [[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"Type"] isEqualToString:@"Reward"]) && [[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"TopText"] rangeOfString:@"\n"].location != NSNotFound) {
            [imageCoupon setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  viewContent.frame.size.height-150)];
        }
        else if ([[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"Type"] isEqualToString:@"Primary"] && [[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"TopText"] isEqualToString:@""]) {
            [imageCoupon setFrame:CGRectMake(0, 0, viewContent.frame.size.width,  viewContent.frame.size.height-100)];
        }
        else {
            if (([[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"TopText"] != nil) && ([[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"SubText"] != nil && ![[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"SubText"] isEqualToString: @""] && [[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"SubText"] length] > 0)){
                
                [imageCoupon setFrame:CGRectMake(imageCoupon.frame.origin.x, imageCoupon.frame.origin.y, imageCoupon.frame.size.width,  viewContent.frame.size.height-150)];
            }
            else{
                [imageCoupon setFrame:CGRectMake(imageCoupon.frame.origin.x, imageCoupon.frame.origin.y, imageCoupon.frame.size.width,  viewContent.frame.size.height-130)];
            }
        }
        [self flipAnimationOnView:viewContent withImageView:imageCoupon withFGImg:[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"TopImageUrl"] andBGImg:[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"BackUrl"]];
    }
    else {
        
        UIImageView *imageCoupon = (UIImageView *)[self.view viewWithTag:(viewContent.tag+5000)-1];
        
        if ([[[self.arrContent objectAtIndex:imageCoupon.tag-5000] allKeys] containsObject:@"BackUrl"] && ![[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"BackUrl"] isEqualToString:@""]) {
            
            [imageCoupon setFrame:CGRectMake(imageCoupon.frame.origin.x, imageCoupon.frame.origin.y, imageCoupon.frame.size.width,  viewContent.frame.size.height)];
            
            [self flipAnimationOnView:viewContent withImageView:imageCoupon withFGImg:[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"TopImageUrl"] andBGImg:[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"BackUrl"]];
        }
        else if ([[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"Type"] isEqualToString:@"Html"] || [[[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"Type"] isEqualToString:@"Survey"]) {

            // Show webview.
            KioskWebViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"webController"];
            viewController.urlString = [[self.arrContent objectAtIndex:imageCoupon.tag-5000] objectForKey:@"ContentUrl"];
            [self presentViewController:viewController animated:YES completion:nil];
        }
    }
}

- (void)flipAnimationOnView:(UIView *)viewContent withImageView:(UIImageView *)imgView withFGImg:(NSString *)forgroundImageURL andBGImg:(NSString *)backgroundImageURL
{
    [UIView transitionWithView:viewContent duration:.3 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        
        if (viewContent.tag < 100) {
            
            viewContent.tag = viewContent.tag * 100;
            [imgView sd_setImageWithURL:[NSURL URLWithString:backgroundImageURL]];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            
            [self hideFiveElements:YES andTag:imgView.tag];
        }
        else {
            
            viewContent.tag = viewContent.tag / 100;
            [imgView sd_setImageWithURL:[NSURL URLWithString:forgroundImageURL]];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            
            [self hideFiveElements:NO andTag:imgView.tag];
        }
    } completion:nil];
}

- (void)hideFiveElements:(BOOL)status andTag:(NSInteger)tagValue {
    
    UILabel *lblExpireDate = (UILabel *)[self.view viewWithTag:tagValue+1000];
    [lblExpireDate setHidden:status];
    
    UIImageView *imageViewBarcode = (UIImageView *)[self.view viewWithTag:tagValue+2000];
    [imageViewBarcode setHidden:status];
    
    UILabel *lblCouponText = (UILabel *)[self.view viewWithTag:tagValue+3000];
    [lblCouponText setHidden:status];
    
    UILabel *lblAmounLeft = (UILabel *)[self.view viewWithTag:tagValue+5000];
    [lblAmounLeft setHidden:status];
}

- (void)showLodingIndicator:(BOOL)show {
    
    if (show) { // SHOW
        
        UIActivityIndicatorView * refreshIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        refreshIndicator.frame = CGRectMake(self.btnRefresh.frame.origin.x, self.btnRefresh.frame.origin.y, self.btnRefresh.frame.size.width, self.btnRefresh.frame.size.height);
        refreshIndicator.hidesWhenStopped = YES;
        refreshIndicator.tag = 1234;
    
        [self.view addSubview:refreshIndicator];
        
        [refreshIndicator startAnimating];
        self.btnRefresh.hidden = YES;
        
    } else { // HIDE
        
        UIActivityIndicatorView * refreshIndicator = (UIActivityIndicatorView *)[self.view viewWithTag:1234];
        if (refreshIndicator.isAnimating) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [refreshIndicator stopAnimating];
                self.btnRefresh.hidden = NO;
            });
        }
        [refreshIndicator removeFromSuperview];
        refreshIndicator = nil;
    }
}

- (IBAction)SideButtonPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)refreshButtonPressed:(id)sender {
    
    if (refreshActivityView == nil) {
        refreshActivityView = [[UIActivityIndicatorView alloc] initWithFrame:self.btnRefresh.frame];
        refreshActivityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    }
    [refreshActivityView startAnimating];
    [refreshActivityView hidesWhenStopped];

    _btnRefresh.hidden = YES;
    [self.imgHeader addSubview:refreshActivityView];
    
    int index = _scrollView.contentOffset.x / _scrollView.frame.size.width;
    
    UIActivityIndicatorView *activityView = (UIActivityIndicatorView *)[self.view viewWithTag:index+9000];
    [activityView startAnimating];
    
    [self hideFiveElements:YES andTag:index+5000];
    
    if (_arrContent.count > 0) {
        [[SDImageCache sharedImageCache] removeImageForKey:[[self.arrContent objectAtIndex:index] valueForKey:@"BarcodeUrl"] withCompletion:nil];
        [[SDImageCache sharedImageCache] removeImageForKey:[[self.arrContent objectAtIndex:index] valueForKey:@"TopImageUrl"] withCompletion:nil];
    }
    
    [appDelegate startIndicator];

    [self updateOffers];
}

- (IBAction)changePage:(id)sender {
    
    _scrollView.contentOffset = CGPointMake(_scrollView.frame.size.width*self.pageControl.currentPage+1, 0);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int currentIndex = _scrollView.contentOffset.x / _scrollView.frame.size.width;
    self.pageControl.currentPage = currentIndex;
    self.initialIndex = currentIndex;

    // Hide or Show share button
    if ([[[self.arrContent objectAtIndex:currentIndex] objectForKey:@"ShareType"] isEqualToString:@"Shareable"] && [self.title isEqualToString:@"COUPONS"]){
        self.btnShareView.hidden = NO;
    }
    else {
        self.btnShareView.hidden = YES;
    }
}

- (void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text) {
        [sharingItems addObject:text];
    }
    if (image) {
        [sharingItems addObject:image];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}


- (CGRect)getFrameOfImage:(UIImageView *) imgView
{
    if(!imgView)
        return CGRectZero;
    
    CGSize imgSize      = imgView.image.size;
    CGSize frameSize    = imgView.frame.size;
    
    CGRect resultFrame;
    
    if(imgSize.width < frameSize.width && imgSize.height < frameSize.height)
    {
        resultFrame.size    = imgSize;
    }
    else 
    {
        float widthRatio  = imgSize.width  / frameSize.width;
        float heightRatio = imgSize.height / frameSize.height;
        
        float maxRatio = MAX (widthRatio , heightRatio);
        
        resultFrame.size = CGSizeMake(imgSize.width / maxRatio, imgSize.height / maxRatio);
    }
    
    resultFrame.origin  = CGPointMake(imgView.center.x - resultFrame.size.width/2 , imgView.center.y - resultFrame.size.height/2);
    
    return resultFrame;
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
        CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
    
        if(location.y > 29 && location.y < 100 && location.x > (SCREEN_WIDTH - 65)/2 && location.x < ((SCREEN_WIDTH - 65)/2)+65) {
            [self GoHome:nil];
        }
}


-(void)GoHome:(NSNotification *)noti
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - Locate Store

- (IBAction)LocateStore:(id)sender {
    
    UIButton *sendButtn = (UIButton *)sender;
    int tagindex = (int)sendButtn.tag;
    tagindex = tagindex - 4000;
    
    StoresViewController *dashboard = [mainStoryboard instantiateViewControllerWithIdentifier:@"storesController"];
    dashboard.isComingfromMenu = NO;
    [self presentViewController:dashboard animated:true completion:nil];

}

-(BOOL)prefersStatusBarHidden{
    return true;
}

@end
