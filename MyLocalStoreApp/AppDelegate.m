 
//
//  AppDelegate.m
//  MyLocalStoreApp
//
//  Created by Surbhi on 08/11/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import "AppDelegate.h"
#import "MyLocalStoreApp-Bridging-Header.h"
#import "MyLocalStoreApp-Swift.h"
#import "SSKeychain.h"
#import "MyLocalStoreApp.pch"
#import "RegisterViewController.h"


@interface AppDelegate () <CLLocationManagerDelegate> {
    
    CLLocation *currentLocation;
    UIImageView *imageView;
    
}
@end

@implementation AppDelegate
+ (AppDelegate *) sharedInstance {
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [application setApplicationIconBadgeNumber:0];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    [self createIndicator];
    
    [GMSServices provideAPIKey:@"AIzaSyC_Qb_fuh2oep2SaAP3pJbOi9UgrcoEsoA"]; // Google Map API Key
    [self registerForRemoteNotifications]; // PUSH NOTIFICATION
    [self getUniqueDeviceIdentifierAsString];

    [[UIApplication sharedApplication] cancelAllLocalNotifications]; // geofencing
    [self setUpLocationManagerWithLauncingOptions:launchOptions];
    [self loadStores];

    SplashViewController *registerController = [mainStoryboard instantiateViewControllerWithIdentifier:@"SplashViewController"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:registerController];
    navController.navigationBarHidden = YES;
    appDelegate.window.rootViewController = navController;
    appDelegate.isLoader = NO;

    [self.window makeKeyAndVisible];
    [UIApplication sharedApplication].statusBarHidden = YES;
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"MyLocalStoreApp"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}


#pragma mark -
#pragma mark - UniqueDeviceIdentifier Methods

- (NSString *)getUniqueDeviceIdentifierAsString
{
    NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
    
    NSString *strApplicationUUID = [SSKeychain passwordForService:appName account:@"com.neuronimbus.MyLocalStoreApp"];
    if (strApplicationUUID == nil)
    {
        strApplicationUUID  = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [SSKeychain setPassword:strApplicationUUID forService:appName account:@"com.neuronimbus.MyLocalStoreApp"];
    }
   
    return strApplicationUUID;
}

#pragma mark -
#pragma mark - Remote Notification Methods

- (void) registerForRemoteNotifications {
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge categories:nil]];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (void) unregisterForRemoteNotifications {
    
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    NSLog(@"Fail to Register for Remote Notifications < %@ >", error.localizedDescription);
    self.strDeviceToken = @"randomDeviceTokenNotRegistered";
}


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
}

-(void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {

}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSLog(@"token %@", deviceToken);
    
    self.strDeviceToken = [[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (deviceToken == nil || [self.strDeviceToken isEqualToString:@""]) {
        self.strDeviceToken = @"randomDeviceTokenNotRegistered";
    }
}

	
#pragma mark -
#pragma mark - NSPushNotification Methods

- (void)registerDevice {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        NSString *urlString = [NSString stringWithFormat:@"%@device", BASEURL];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@", _dateString, [[self getUniqueDeviceIdentifierAsString] stringByReplacingOccurrencesOfString:@"-" withString:@""], self.strUserId, self.strDeviceToken, self.strMobileNumber, @"en", @"iOS", [[UIDevice currentDevice] systemVersion], SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        if (_strMobileNumber == nil)
        {
            return;
        }
        NSDictionary *paramDic = @{@"UserID":self.strUserId,
                                   @"DeviceId":[[self getUniqueDeviceIdentifierAsString] stringByReplacingOccurrencesOfString:@"-" withString:@""],
                                   @"DeviceToken":self.strDeviceToken,
                                   @"MSN":self.strMobileNumber,
                                   @"Language":@"en",
                                   @"DeviceType":@"iOS",
                                   @"AppVersion":[[UIDevice currentDevice] systemVersion]
                                   };
        
        NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
        [request setPostBody:[NSMutableData dataWithData:data]];
        
        [request startAsynchronous];
        
    }
}


#pragma mark -
#pragma mark - CLLocationManagerDelegate Methods

- (void) setUpLocationManagerWithLauncingOptions: (NSDictionary *)launchOptions {
    
    // Initialize Location Manager
    self.locationManager = [[CLLocationManager alloc] init];
    
    // Configure Location Manager
    [self.locationManager setDelegate:self];
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    _locationManager.distanceFilter=kCLDistanceFilterNone;
    [_locationManager requestWhenInUseAuthorization];
    [_locationManager startMonitoringSignificantLocationChanges];
    
    // Load Geofences
    self.geofences = [NSMutableArray arrayWithArray:[[self.locationManager monitoredRegions] allObjects]];
    self.strUserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserId"];
    
    [self.locationManager startUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    currentLocation = [locations lastObject];
    for (CLCircularRegion *circle in self.latestGeofences) {
        if (![self.geofences containsObject:circle]) {
            [self.locationManager startMonitoringForRegion:circle]; // Start Monitoring Region
        }
    }
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
        NSLog(@"LOCATION MANAGER  %s | ERROR :- %@", __PRETTY_FUNCTION__, [error localizedDescription]);
}



- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    
    //    [self pushCoupon];
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        
        [CustomAlertController showAlertOnController:self.window.rootViewController withAlertType:Simple withTitle:@"Welcome!" andMessage:[NSString stringWithFormat:@"you are nearby - %@", region.identifier]];
    }
    else{
        
        UILocalNotification * notification = [[UILocalNotification alloc] init];
        notification.alertBody = [NSString stringWithFormat:@"Welcome! : you are nearby - %@", region.identifier];
        notification.soundName = @"Default";
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    }
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        
        [CustomAlertController showAlertOnController:self.window.rootViewController withAlertType:Simple withTitle:@"EXIT" andMessage:[NSString stringWithFormat:@"you are now exit from - %@", region.identifier]];
    }
    else{
        
        UILocalNotification * notification = [[UILocalNotification alloc] init];
        notification.alertBody = [NSString stringWithFormat:@"Thank You! : you are now exit from - %@", region.identifier];
        notification.soundName = @"Default";
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    }
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    
    //    NSLog(@"LOCATION MANAGER didStartMonitoringForRegion :---> My eyes on you!");
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
    
    //    NSLog(@"LOCATION MANAGER ERROR :---> %@", [error localizedDescription]);
}

+ (NSString *)kilometersFromPlace:(CLLocationCoordinate2D)from andToPlace:(CLLocationCoordinate2D)to {
    
    CLLocation *userLoc = [[CLLocation alloc] initWithLatitude:from.latitude longitude:from.longitude];
    CLLocation *storeLoc = [[CLLocation alloc] initWithLatitude:to.latitude longitude:to.longitude];
    CLLocationDistance distance = [userLoc distanceFromLocation:storeLoc]/1000;
    return [NSString stringWithFormat:@"%1f", distance];
}


#pragma mark
#pragma mark - UserID Details
- (void)getUserID {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus reachStatus = [reachability currentReachabilityStatus];
    
    if (reachStatus) {
        
        NSString *urlString = [NSString stringWithFormat:@"%@initialize", BASEURL];

        NSString *_dateString = [HelperClass getDateString];
        NSString * Device_Id = [[self getUniqueDeviceIdentifierAsString] stringByReplacingOccurrencesOfString:@"-" withString:@""];
        
        //Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, Device_Id, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];

        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        NSDictionary *paramDic = @{@"DeviceId":Device_Id, @"Culture" : @"NO-nb"};
        
        NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:kNilOptions error:nil];
        [request setRequestMethod:@"POST"];
        [request setPostBody:[NSMutableData dataWithData:data]];
        
        [request setDidFinishSelector:@selector(getUserRequestSuccessful:)];
        [request setDidFailSelector:@selector(getUserRequestFailed:)];
        [request setDelegate:self];
        [request startAsynchronous];
    }
    else {
        
        [CustomAlertController showAlertOnController:self.window.rootViewController withAlertType:Simple withTitle:@"Error occurred" andMessage:@"No internet connection." andButtonTitle:@"Ok"];
    }
    
}

- (void)getUserRequestSuccessful:(ASIHTTPRequest *)request {
    
    NSDictionary *responseDic = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:request.responseData options:kNilOptions error:nil]];
    
    if([[responseDic allKeys] containsObject:@"UserId"]) {
        self.strUserId = [responseDic objectForKey:@"UserId"];
        NSArray *dicConsents = [responseDic objectForKey:@"Consents"];
        
        for (NSDictionary *dict in dicConsents) {
            NSDictionary * dic = [dict objectForKey:@"CurrentVersion"];
            if ([dict[@"Name"] isEqualToString:@"Master"]) {
                
                appDelegate.privacyPolicyStr = @"";
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:PrivacyPolicy];

                self.privacyPolicyStr = [dic objectForKey:@"PrivacyPolicy"];
                self.minimumAgeFactor = [dic objectForKey:@"MinimumAge"];
                self.dobStringMinimumAge = [dic objectForKey:@"MinimumAge"];
            }
        }
        
        
        
        [[NSUserDefaults standardUserDefaults]setObject:dicConsents forKey:DICConsents];
        [[NSUserDefaults standardUserDefaults]setObject:self.privacyPolicyStr forKey:PrivacyPolicy];
        [[NSUserDefaults standardUserDefaults]setObject:self.minimumAgeFactor forKey:MINAge];
        [[NSUserDefaults standardUserDefaults] setObject:[responseDic objectForKey:@"UserId"] forKey:@"UserId"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
         [self MakeDecisionforRootView];
    }
    else{
        RegisterViewController *registerController = [mainStoryboard instantiateViewControllerWithIdentifier:@"registerController"];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:registerController];
        navController.navigationBarHidden = YES;
        self.window.rootViewController = navController;
        _isLoader = NO;
    }
}

- (void)getUserRequestFailed:(ASIHTTPRequest *)request {

    NSDictionary *responseDic = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];
    NSLog(@"GET User Failed - %@",responseDic);
}


#pragma mark
#pragma mark - GET USER PROFILE

- (void)MakeDecisionforRootView {
    BOOL isUserAuthenticated = [[NSUserDefaults standardUserDefaults] boolForKey:@"UserAuthenticated"];
    
    if (isUserAuthenticated)
    {
        NSArray *consents = [[NSUserDefaults standardUserDefaults] objectForKey:DICConsents];
        NSString *masterConsetState;
        for (NSDictionary *dicr in consents) {
            if ([dicr[@"Name"] isEqualToString:@"Master"]){
                masterConsetState = dicr[@"State"];
            }
        }
        
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"PreferencesSet"] == false){
            PreferencesViewController *registerController = [mainStoryboard instantiateViewControllerWithIdentifier:@"PreferencesViewController"];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:registerController];
            navController.navigationBarHidden = YES;
            self.window.rootViewController = navController;
        }
        else if (![masterConsetState isEqualToString:@"ConsentGiven"]){
            //Terms
            
            TermsViewController *dashboard = [mainStoryboard instantiateViewControllerWithIdentifier:@"termsController"];
            dashboard.isloggedin = true;
            dashboard.isNewMasterConsent = true;
            
            MenuItemsTVC *sidemenu = [MenuItemsTVC sharedInstance];
            ENSideMenuNavigationController *naviController = [[ENSideMenuNavigationController alloc] initWithMenuViewController:sidemenu contentViewController:dashboard];
            naviController.navigationBarHidden = YES;
            _isLoader = YES;
            
            CGFloat sidemenuWide = SCREEN_WIDTH - 75  ;
            [naviController.sideMenu setMenuWidth:sidemenuWide];
            
            [self.window makeKeyAndVisible];
            [self.window setRootViewController:naviController];
        }
        else{
            appDelegate.needAPIRefresh = YES;
            HomeVC *home = [mainStoryboard instantiateViewControllerWithIdentifier:@"HomeVC"];
            MenuItemsTVC *sidemenu = [MenuItemsTVC sharedInstance];
            
            ENSideMenuNavigationController *naviController = [[ENSideMenuNavigationController alloc] initWithMenuViewController:sidemenu contentViewController:home];
            naviController.navigationBarHidden = YES;
            _isLoader = YES;
            
            CGFloat sidemenuWide = SCREEN_WIDTH - 75  ;
            [naviController.sideMenu setMenuWidth:sidemenuWide];
            
            [self.window makeKeyAndVisible];
            [self.window setRootViewController:naviController];
        }
    }
    else {
        
        RegisterViewController *registerController = [mainStoryboard instantiateViewControllerWithIdentifier:@"registerController"];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:registerController];
        navController.navigationBarHidden = YES;
        self.window.rootViewController = navController;
        _isLoader = NO;
    }
    
    
    self.strUserId = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"];
    self.strMobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"MSN"];
    
//    if ([self.strUserId length] > 0) {
//        [self loadFeedbackBacklogValue];
//    }
//    

}



#pragma mark
#pragma mark - SIDE MENU METHODS

-(void)setTopViewwithVC:(id)viewControl// Done By Surbhi
{
    ENSideMenuNavigationController *naviController = (ENSideMenuNavigationController *)[self.window rootViewController];
    if (![[naviController topViewController] isKindOfClass:[viewControl class]]) {
        naviController.navigationBarHidden = YES;
        [naviController setContentViewController:viewControl];
        CFRunLoopWakeUp(CFRunLoopGetCurrent());
    }
    [naviController hideSideMenuView];
}

-(id)returnTopViewController{
    ENSideMenuNavigationController *naviController = (ENSideMenuNavigationController *)[self.window rootViewController];
    return  [naviController topViewController];
}

-(void)changeRootViewController{
    RegisterViewController *registerController = [mainStoryboard instantiateViewControllerWithIdentifier:@"registerController"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:registerController];
    navController.navigationBarHidden = NO;
    _isLoader = NO;
    [SVProgressHUD dismiss];
    self.window.rootViewController = navController;
}

#pragma mark -
#pragma mark - UIBarButtoItem Methods

- (void)loadFeedbackBacklogValue:(BOOL)val {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        NSString *urlString = [NSString stringWithFormat:@"%@ratings/categories/?UserID=%@&CategoryId=%@", BASEURL, appDelegate.strUserId,@""];

        if (val == true) {
            urlString = [NSString stringWithFormat:@"%@/ratings/categories/?UserID=%@&CategoryId=%@", BASEURL, appDelegate.strUserId,@"20"];
        }

        NSString *_dateString = [HelperClass getDateString];
        // Generate signature

        NSString *source = [NSString stringWithFormat:@"%@%@%@", _dateString, appDelegate.strUserId, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];

        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];

        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];

        [request startAsynchronous];

    }
    else {
        
        [self stopIndicator];
        [CustomAlertController showAlertOnController:self.window.rootViewController withAlertType:Simple withTitle:@"Error occurred" andMessage:@"No internet connection." andButtonTitle:@"Cancel"];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSDictionary *dict_response = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];

    if ([[dict_response allKeys] containsObject:@"Categories"]) {
        NSArray *arr_feedback = [[NSArray alloc]init];
        arr_feedback = dict_response[@"Categories"];
        NSDictionary * dicFeedback = arr_feedback[0];
        
        [NSUserDefaults.standardUserDefaults setObject:dicFeedback forKey:DICFeedback];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self feedbackdata:dicFeedback];
    }
    else{
        NSLog(@"dict_response");
    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"BACKLOGUpdated" object:nil];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSDictionary * dicfeedback = [[NSDictionary alloc]init];
    dicfeedback = [[NSUserDefaults standardUserDefaults]objectForKey:DICFeedback];
    [self feedbackdata:dicfeedback];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"BACKLOGUpdated" object:nil];
}
#pragma mark -
#pragma mark - methods for offline data

-(void)feedbackdata:(NSDictionary *)dicFeedback
{
    if ([[dicFeedback allKeys] containsObject:@"RatingsPending"]) {

        _backlogVal = [[dicFeedback valueForKey:@"RatingsPending"] intValue];
        
        if (_backlogVal > 0){
            NSDictionary * dic_pendingRating = dicFeedback[@"PendingRating"];
            _feedbackTime = [dic_pendingRating valueForKey:@"TimeToLive"];
            _ratingId = [dic_pendingRating valueForKey:@"Id"];
            _dict_PendingRatingStore = dic_pendingRating[@"Store"];
        }
    }
    else if ([[dicFeedback objectForKey:@"http_code"] integerValue] == 200) {

        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"REGISTERDEVICE"];
        self.isLoader = YES;

        [[NSUserDefaults standardUserDefaults] synchronize];
    }

}

#pragma mark - ASIHTTPRequest Methods

- (void)loadStores {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status) {
        
        NSString *urlString = [NSString stringWithFormat:@"%@Stores", BASEURL];
        NSString *_dateString = [HelperClass getDateString];
        
        // Generate signature
        NSString *source = [NSString stringWithFormat:@"%@%@", _dateString, SALT];
        NSString *signature = [NSString stringWithFormat:@"%@", [source sha2Hash]];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setTimeOutSeconds:30.0];
        [request setDelegate:self];
        
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request addRequestHeader:@"X-Liquid-Timestamp" value:_dateString];
        [request addRequestHeader:@"X-Liquid-Signature" value:signature];
        
        [request setDidFinishSelector:@selector(requestFinishedd:)];
        [request setDidFailSelector:@selector(requestFailedd:)];
        [request startAsynchronous];
    }
}

- (void)requestFinishedd:(ASIHTTPRequest *)request {
    
    NSDictionary *dicStores = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:[request responseData] options:kNilOptions error:nil]];

    if ([[dicStores allKeys] containsObject:@"Stores"]) {
        
        NSMutableArray *storeArray = [[NSMutableArray alloc] init];
        
        if (currentLocation != nil) {
            
            for (NSDictionary *dic in [dicStores objectForKey:@"Stores"]) {
                
                @autoreleasepool {
                    
                    NSMutableDictionary *dicObj = [[NSMutableDictionary alloc] initWithDictionary:dic];
                    
                    CLLocation *storeLocation = [[CLLocation alloc] initWithLatitude:[[dicObj objectForKey:@"Latitude"] floatValue] longitude:[[dicObj objectForKey:@"Longitude"] floatValue]];
                    [dicObj setObject:[AppDelegate kilometersFromPlace:currentLocation.coordinate andToPlace:storeLocation.coordinate] forKey:@"Distance"];
                    
                    // geofencing
                    CLCircularRegion *circle = [[CLCircularRegion alloc] initWithCenter:[storeLocation coordinate] radius:100.0 identifier:dicObj[@"Name"]];
                    
                    circle.notifyOnEntry = YES;
                    circle.notifyOnExit = YES;
                    
                    if (self.latestGeofences == nil) {
                        self.latestGeofences = [NSMutableArray array];
                    }
                    [self.latestGeofences addObject:circle];
                    
                    
                    NSMutableArray *openArr = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"OpeningHours"]];;
                    for (int i = 0; i < openArr.count; i++) {
                        
                        if (openArr[i]  == [NSNull null]) {
                            [openArr replaceObjectAtIndex:i withObject:@""];
                        }
                    }
                    [dicObj setObject:openArr forKey:@"OpeningHours"];
                    
                    
                    
                    [storeArray addObject:dicObj];
                }
            }
            
            [self.locationManager startUpdatingLocation];
            
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Distance" ascending:YES];
            self.arrStores = [[NSArray alloc] initWithArray:[storeArray mutableCopy]];
            self.arrStores = [self.arrStores sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
            
            //
            // Save data to prefrences
            [[NSUserDefaults standardUserDefaults] setObject:self.arrStores forKey:@"Stores"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else {
            
            self.arrStores = [[NSArray alloc] initWithArray:[dicStores objectForKey:@"Stores"]];
        }
    }
    
}

- (void)requestFailedd:(ASIHTTPRequest *)request
{
        NSLog(@"error ----- %@", [request.error localizedDescription]);
}

#pragma mark - Loader
//MARK:- INDICATOR VIEW
-(void)createIndicator
{
    self.activityView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    self.activityView1.backgroundColor = [UIColor clearColor];
    
    UIImageView *back = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    back.backgroundColor =[UIColor blackColor];
    back.alpha = 0.5;
    [self.activityView1 addSubview:back];
    
    self.logoView = [[UIView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 75)/2, (SCREEN_HEIGHT - 75)/2, 75, 75)];
    self.logoView.layer.cornerRadius = 37.5;
    self.logoView.backgroundColor = [UIColor whiteColor];
    
    self.imageForR = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 55, 55)];
    self.imageForR.image = [UIImage imageNamed:@"appHeader"];
    self.imageForR.backgroundColor = [UIColor clearColor];
    self.imageForR.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.logoView addSubview:self.imageForR];
    [self.activityView1 addSubview:self.logoView];
    [self.window addSubview:self.activityView1];
    [self.window sendSubviewToBack:self.activityView1];
}

-(void)startIndicator
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.window addSubview:self.activityView1];
        [self rotate360Degrees];
        [self.window bringSubviewToFront:self.activityView1];
    });
}

-(void)stopIndicator
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.imageForR.layer  removeAllAnimations];
        [self.window willRemoveSubview:self.activityView1];
        [self.activityView1 removeFromSuperview];
    });
}

-(void)rotate360Degrees
{
    CABasicAnimation *rotate = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    rotate.fromValue = @(0);
    rotate.toValue = @(180);
    rotate.duration = 50;
    rotate.repeatCount = 100.0;
    [self.imageForR.layer addAnimation:rotate forKey:@"myRotationAnimation"];
}


#pragma mark - CUSTOM Loader
-(void)createCustomLoaderForAPIwithImage:(NSString *)imageStr withAnimation:(BOOL)animate
{
    if (self.activityView3 == nil)
    {
        self.activityView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        self.activityView3.backgroundColor = [UIColor clearColor];
        UIImageView *back = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        back.backgroundColor =[UIColor whiteColor];
        back.alpha = 0.3;
        [self.activityView3 addSubview:back];
        
        self.logoView3 = [[UIView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 90)/2, (SCREEN_HEIGHT - 90)/2, 90, 90)];
        self.logoView3.layer.cornerRadius = 45;
        self.logoView3.backgroundColor = [UIColor whiteColor];
        
        self.imageFor3rdAnimation = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 70, 70)];
        if (animate == YES){
            self.imageFor3rdAnimation.frame = CGRectMake(10, 10, 55, 55);
        }
        else{
            self.imageFor3rdAnimation.frame = CGRectMake(0 , 0, 90, 90);
        }
        self.imageFor3rdAnimation.backgroundColor = [UIColor clearColor];
        self.imageFor3rdAnimation.contentMode = UIViewContentModeScaleAspectFit;
        
        [self.logoView3 addSubview:self.imageFor3rdAnimation];
        
        [self.activityView3 addSubview:self.logoView3];
        
    }
    if (animate == YES){
        self.imageFor3rdAnimation.frame = CGRectMake(10, 10, 70, 70);
    }
    else{
        self.imageFor3rdAnimation.frame = CGRectMake(0, 0, 90, 90);
    }
    self.imageFor3rdAnimation.image = [UIImage imageNamed:imageStr];
    
    if (animate == YES){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.window addSubview:self.activityView3];
            [self rotate360DegreesSHARE];
            [self.window bringSubviewToFront:self.activityView3];
        });
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.window addSubview:self.activityView3];
            [self.window bringSubviewToFront:self.activityView3];
        });
    }
}

-(void)startIndicatorSHAREwithAnimation:(BOOL)animate
{
    if (animate == YES){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.window addSubview:self.activityView3];
            [self rotate360Degrees];
            [self.window bringSubviewToFront:self.activityView3];
        });
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.window addSubview:self.activityView3];
            [self.window bringSubviewToFront:self.activityView3];
        });
    }
}

-(void)stopIndicatorSHARE
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.imageFor3rdAnimation.layer  removeAllAnimations];
        [self.window sendSubviewToBack:self.activityView3];
        [self.window willRemoveSubview:self.activityView3];
        [self.activityView3 removeFromSuperview];
        
    });
}


-(void)rotate360DegreesSHARE
{
    CABasicAnimation *rotate = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    rotate.fromValue = @(0);
    rotate.toValue = @(180);
    rotate.duration = 50;
    rotate.repeatCount = 100.0;
    [self.imageFor3rdAnimation.layer addAnimation:rotate forKey:@"myRotationAnimation"];
}

#pragma mark - WELCOME POP UP

-(void)showWelcomePop:(NSString *)name{
    
    self.activityView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    self.activityView2.backgroundColor = [UIColor clearColor];
    
    UIImageView *back = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    back.backgroundColor =[UIColor blackColor];
    back.alpha = 0.5;
    [self.activityView2 addSubview:back];
    
    CGRect frame_popUp = CGRectMake(35, (SCREEN_HEIGHT - 150)/2, SCREEN_WIDTH - 70, 150);
    CGRect frame_Logo = CGRectMake((frame_popUp.size.width - 185)/ 2, 15, 185, 45);
    CGRect frame_Label = CGRectMake(20, 65, SCREEN_WIDTH - 100, 50);

    
    if (IS_IPHONE_5) {
        frame_popUp = CGRectMake(25, (SCREEN_HEIGHT - 175)/2, SCREEN_WIDTH - 50, 175);
        frame_Logo = CGRectMake((frame_popUp.size.width - 185)/ 2, 15, 185, 45);
        frame_Label = CGRectMake(20, 65, SCREEN_WIDTH - 100, 75);
    }
    
    
    UIView *popBox = [[UIView alloc] initWithFrame:frame_popUp];
    popBox.backgroundColor = [UIColor whiteColor];
    popBox.layer.cornerRadius = 8.0;
    
    UIImageView *logo = [[UIImageView alloc] initWithFrame:frame_Logo];
    logo.image = [UIImage imageNamed:@"appLogoHeader"];
    logo.backgroundColor = [UIColor clearColor];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [popBox addSubview:logo];
    
    UILabel *welcomeLbl = [[UILabel alloc] initWithFrame:frame_Label];
    welcomeLbl.numberOfLines = 0;
    welcomeLbl.font = [UIFont fontWithName:BertholdBold size:15];
    welcomeLbl.textColor = [UIColor blackColor];
    welcomeLbl.textAlignment = NSTextAlignmentCenter;
    welcomeLbl.text = [NSString stringWithFormat:@"Welcome %@!\nWe hope you will enjoy the app.",name];
    [popBox addSubview:welcomeLbl];
    
    [self.activityView2 addSubview:popBox];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.window addSubview:self.activityView2];
        [self.window bringSubviewToFront:self.activityView2];
    });
}

-(void)hideWelcomePop{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.window willRemoveSubview:self.activityView2];
        [self.activityView2 removeFromSuperview];
    });
}


@end
