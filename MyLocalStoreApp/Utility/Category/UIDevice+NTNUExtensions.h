//
//  UIDevice+NTNUExtensions.h
//  TwentyFourSeven
//
//  Created by HiteshDhawan on 05/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (NTNUExtensions)

- (NSString *)ntnu_deviceDescription;
- (NSString *)ntnu_deviceType;

@end
