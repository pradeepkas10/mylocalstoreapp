//
//  UITextField+Padding.h
//  TwentyFourSeven
//
//  Created by Hitesh Dhawan on 27/09/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Padding)

- (void)setLeftPadding:(int)paddingValue withImageName:(NSString *)imageName;
- (void)setRightPadding:(int)paddingValue withImageName:(NSString *)imageName;

@end
