//
//  UITextField+Padding.m
//  TwentyFourSeven
//
//  Created by Hitesh Dhawan on 27/09/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "UITextField+Padding.h"

@implementation UITextField (Padding)

- (void)setLeftPadding:(int)paddingValue withImageName:(NSString *)imageName
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, paddingValue, self.frame.size.height)];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
}

- (void)setRightPadding:(int)paddingValue withImageName:(NSString *)imageName
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, paddingValue, self.frame.size.height)];
    UIImageView *paddingImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, paddingValue, paddingView.frame.size.height)];
    paddingImageView.image = [UIImage imageNamed:imageName];
    [paddingImageView setContentMode:UIViewContentModeRight];
    [paddingView addSubview:paddingImageView];
    self.rightView = paddingView;
    self.rightViewMode = UITextFieldViewModeAlways;
}

@end
