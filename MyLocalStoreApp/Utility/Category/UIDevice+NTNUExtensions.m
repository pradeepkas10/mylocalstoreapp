//
//  UIDevice+NTNUExtensions.m
//  TwentyFourSeven
//
//  Created by HiteshDhawan on 05/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "UIDevice+NTNUExtensions.h"
#import <sys/utsname.h>

@implementation UIDevice (NTNUExtensions)

- (NSString *)ntnu_deviceDescription
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}

- (NSString *)ntnu_deviceType
{
    return [[self ntnu_deviceTypeLookupTable] objectForKey:[self ntnu_deviceDescription]];
}

- (NSDictionary *)ntnu_deviceTypeLookupTable
{
    return @{
             @"i386": @"Simulator",
             @"x86_64": @"Simulator",
             @"iPod1,1": @"iPod",
             @"iPod2,1": @"iPod 2G",
             @"iPod3,1": @"iPod 3G",
             @"iPod4,1": @"iPod 4G",
             @"iPhone1,1": @"iPhone",
             @"iPhone1,2": @"iPhone 3G",
             @"iPhone2,1": @"iPhone 3GS",
             @"iPhone3,1": @"iPhone 4",
             @"iPhone3,3": @"iPhone 4",
             @"iPhone4,1": @"iPhone 4S",
             @"iPhone5,1": @"iPhone 5",
             @"iPhone5,2": @"iPhone 5",
             @"iPhone5,3": @"Phone 5C",
             @"iPhone5,4": @"iPhone 5C",
             @"iPhone6,1": @"iPhone 5S",
             @"iPhone6,2": @"iPhone 5S",
             @"iPhone7,1": @"iPhone 6 Plus",
             @"iPhone7,2": @"iPhone 6",
             @"iPhone8,2": @"iPhone 6S Plus",
             @"iPhone8,1": @"iPhone 6S",
             @"iPad1,1": @"iPad",
             @"iPad2,1": @"iPad 2",
             @"iPad3,1": @"iPad 3G",
             @"iPad3,4": @"iPad 4G",
             @"iPad2,5": @"iPadMini",
             @"iPad4,1": @"iPad5G Air",
             @"iPad4,2": @"iPad5G Air",
             @"iPad4,4": @"iPad Mini 2G",
             @"iPad4,5": @"iPad Mini 2G"
             };
}

@end
