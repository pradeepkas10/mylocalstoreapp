//
//  VerticalSwipeInteractionController.h
//  RConnect
//
//  Created by Avineet on 24/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "BaseInteractionController.h"

@interface VerticalSwipeInteractionController : BaseInteractionController < UIGestureRecognizerDelegate >

@end
