//
//  PresentingAnimationController.m
//  RConnect
//
//  Created by Avineet on 24/11/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "PresentingAnimationController.h"
//#import "CouponsViewController.h"
//#import "CouponDetailsViewController.h"
//#import "LoyaltyViewController.h"

@implementation PresentingAnimationController


- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.80;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    
//    UIViewController * fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
//    
//    CouponDetailsViewController * toVC = (CouponDetailsViewController *)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
//    
//    UIView *toView = toVC.view;
//    UIView *fromView = self.startingView;
//    
//    if (self.isCoupon) {
//        self.initialframe = [(CouponsViewController *)[APP_DELEGATE currentVC] initialRect];
//    } else {
//        self.initialframe = [(LoyaltyViewController *)[APP_DELEGATE currentVC] initialRect];
//    }
//    
//    UIView* containerView = [transitionContext containerView];
//    [containerView addSubview:toView];
//    [containerView sendSubviewToBack:toView];
//    
//    //-----
//    
//    CGRect frame = CGRectMake(8, self.initialframe.origin.y + 64 + 8,
//                              SCREEN_WIDTH - 16, self.initialframe.size.height - 8);
//    
//    if (frame.origin.y <= 64) {
//        frame.size.height += frame.origin.y - 64;
//        frame.origin.y = 64;
//        
//    } else if (frame.origin.y + frame.size.height > SCREEN_HEIGHT - 49
//               && frame.origin.y < SCREEN_HEIGHT - 49) {
//        
//        frame.size.height -= frame.origin.y + frame.size.height - SCREEN_HEIGHT + 49;
//    }
//    
//    UIView * backGroundView = [[UIView alloc] initWithFrame:frame];
//    backGroundView.backgroundColor = [UIColor whiteColor];
//    backGroundView.opaque = YES;
//    backGroundView.layer.opaque = YES;
//    [containerView addSubview:backGroundView];
//    
//    //-----
//    
//    
//    // Add a perspective transform
//    CATransform3D transform = CATransform3DIdentity;
//    transform.m34 = -0.002;
//    [containerView.layer setSublayerTransform:transform];
//    
//    
//    // Give both VCs the same start frame
//    frame = [transitionContext initialFrameForViewController:fromVC];
//    fromView.frame = CGRectMake(8,
//                                self.initialframe.origin.y + 64 + 8,
//                                SCREEN_WIDTH - 16,
//                                self.initialframe.size.height - 8);
//    toView.frame = frame;
//    
//    
//    // create two-part snapshots of both the from- and to- views
//    NSArray* toViewSnapshots = [self createSnapshots:toView afterScreenUpdates:YES];
//    UIView* flippedSectionOfToView = [toViewSnapshots objectAtIndex:1];
//    
//    // customise
//    CGRect snapshotRegion = CGRectMake(8,
//                                       self.initialframe.origin.y + 64 + 8,
//                                       SCREEN_WIDTH - 16,
//                                       self.initialframe.size.height - 8);
//    
//    UIView *leftHandView = [fromVC.view resizableSnapshotViewFromRect:snapshotRegion
//                                                   afterScreenUpdates:NO
//                                                        withCapInsets:UIEdgeInsetsZero];
//    
//    leftHandView.frame = snapshotRegion;
//    [containerView addSubview:leftHandView];
//
//    NSArray* fromViewSnapshots = @[leftHandView];
//
//    UIView* flippedSectionOfFromView = [fromViewSnapshots objectAtIndex:0];
//    
//    // change the anchor point so that the view rotate around the correct edge
//    [self updateAnchorPointAndOffset:CGPointMake(0.5, 0.0) view:flippedSectionOfFromView];
//    
//    [self updateAnchorPointAndOffset:CGPointMake(0.5, 1.0) view:flippedSectionOfToView];
//    
//    // rotate the to- view by 90 degrees, hiding it
//    flippedSectionOfToView.layer.transform = [self rotate:-M_PI_2];
//    
//    // animate
//    NSTimeInterval duration = [self transitionDuration:transitionContext];
//    
//    [UIView animateKeyframesWithDuration:duration
//                                   delay:0.0
//                                 options:UIViewKeyframeAnimationOptionCalculationModeLinear
//                              animations:^{
//            [UIView addKeyframeWithRelativeStartTime:0.0
//                                    relativeDuration:0.2
//                                          animations:^{
//                                                                                            
//                                              flippedSectionOfFromView.frame = CGRectMake(8, SCREEN_HEIGHT/2, SCREEN_WIDTH - 16, self.initialframe.size.height);
//                                              
//                                              
//                                              ((UIView *)toViewSnapshots[0]).frame = CGRectMake(8, SCREEN_HEIGHT/2, SCREEN_WIDTH - 16, self.initialframe.size.height);
//                                              
//                                          }];
//                                  
//            [UIView addKeyframeWithRelativeStartTime:0.1
//                                    relativeDuration:0.4
//                                          animations:^{
//                                              
//                                              flippedSectionOfFromView.layer.transform = [self rotate:M_PI_2];
//                                          }];
//                                  
//            [UIView addKeyframeWithRelativeStartTime:0.5
//                                    relativeDuration:0.4
//                                          animations:^{
//                                              // rotate the to- view to 0 degrees
//                                              flippedSectionOfToView.layer.transform = [self rotate:-0.001];
//                                              
//                                              flippedSectionOfToView.frame=CGRectMake(0, 0, toVC.view.frame.size.width, toVC.view.frame.size.height/2);
//                                              
//                                              ((UIView *)toViewSnapshots[0]).frame=CGRectMake(0, toVC.view.frame.size.height/2, toVC.view.frame.size.width, toVC.view.frame.size.height/2);
//                                              
//                                              ((UIView *)toViewSnapshots[1]).frame=CGRectMake(0, 0, toVC.view.frame.size.width, toVC.view.frame.size.height/2);
//                                              
//                                          }];
//                                  
//                              } completion:^(BOOL finished) {
//                                  
//                                  // remove all the temporary views
//                                  if ([transitionContext transitionWasCancelled]) {
//                                      [self removeOtherViews:fromView];
//                                  } else {
//                                      [self removeOtherViews:toView];
//                                      if (APP_DELEGATE.settingsInteractionController) {
//                                          [APP_DELEGATE.settingsInteractionController wireToViewController:toVC forOperation:InteractionOperationDismiss];
//                                      }
//                                  }
//                                  // inform the context of completion
//                                  [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
//                                  
//                              }];
    
}

- (NSArray*)createSnapshots:(UIView*)view afterScreenUpdates:(BOOL) afterUpdates {
    
//    UIView* containerView = view.superview;
//    CGRect snapshotRegion;
//    
//    // snapshot the left-hand side of the view
//    snapshotRegion = CGRectMake(8, self.initialframe.origin.y + 64 + 8, SCREEN_WIDTH - 16, self.initialframe.size.height - 8);
//    
//    UIView *leftHandView = [view resizableSnapshotViewFromRect:CGRectMake(0,view.frame.size.height/2,view.frame.size.width,view.frame.size.height/2)
//                                            afterScreenUpdates:afterUpdates
//                                                 withCapInsets:UIEdgeInsetsZero];
//    leftHandView.frame = snapshotRegion;
//    [containerView addSubview:leftHandView];
//    
//    // snapshot the right-hand side of the view
//    snapshotRegion = CGRectMake(8, 0, SCREEN_WIDTH - 16, view.frame.size.height/2);
//    
//    UIView *rightHandView = [view resizableSnapshotViewFromRect:CGRectMake(0, 0,view.frame.size.width,view.frame.size.height/2)  afterScreenUpdates:afterUpdates withCapInsets:UIEdgeInsetsZero];
//    rightHandView.frame = snapshotRegion;
//    [containerView addSubview:rightHandView];
//    
//    // send the view that was snapshotted to the back
//    [containerView sendSubviewToBack:view];
//    
//    return @[leftHandView, rightHandView];
    return nil;
}

- (void)removeOtherViews:(UIView*)viewToKeep {
    UIView* containerView = viewToKeep.superview;
    for (UIView* view in containerView.subviews) {
        if (view != viewToKeep) {
            [view removeFromSuperview];
        }
    }
}

- (void)updateAnchorPointAndOffset:(CGPoint)anchorPoint view:(UIView*)view {
    
    view.layer.anchorPoint = anchorPoint;
    float xOffset =  anchorPoint.y - 0.5;
    
    view.frame = CGRectOffset(view.frame, 0, xOffset * view.frame.size.height);
    
}

- (CATransform3D) makeRotationAndPerspectiveTransform:(CGFloat) angle {
    CATransform3D transform = CATransform3DMakeRotation(angle, 1.0f, 0.0f, 0.0f);
    transform.m34 = 1.0 / -500;
    return transform;
}

- (CATransform3D) rotate:(CGFloat) angle {
    return  CATransform3DMakeRotation(angle, 1.0, 0.0, 0.0);
}

@end
