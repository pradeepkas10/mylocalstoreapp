//
//  CustomAlertController.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 06/05/16.
//  Copyright © 2016 Surbhi Varma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    
    Simple ,
    Network
    
} AlertType;

@interface CustomAlertController : NSObject

+(void)showAlertOnController:(UIViewController*)controller
               withAlertType:(AlertType)alertType
               andAttributes:(NSDictionary*)attributes;

+(void)showAlertOnController:(UIViewController*)controller
               withAlertType:(AlertType)alertType
                   withTitle:(NSString *)title
                  andMessage:(NSString *)message;

+(void)showAlertOnController:(UIViewController*)controller
               withAlertType:(AlertType)alertType
                   withTitle:(NSString *)title
                  andMessage:(NSString *)message
              andButtonTitle:(NSString *)BtnTitle;

@end
