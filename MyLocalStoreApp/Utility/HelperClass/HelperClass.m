//
//  HelperClass.m
//  LiQUiD BARCODES
//
//  Created by HiteshDhawan on 23/12/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import "HelperClass.h"
#import "constant.h"


@implementation HelperClass

+ (NSString *)getDateString {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    // ISO-8601 format
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    
    return [formatter stringFromDate:[NSDate date]];
}

+ (NSString *)GetGUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge NSString *)string;
}

+ (BOOL) isValidEmail:(NSString *)emailString {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailString];
}

+(void)incrementOpenCount{
    
    NSInteger launches = [[NSUserDefaults standardUserDefaults]integerForKey:RecipeOpenCount];
    launches += 1;

    [[NSUserDefaults standardUserDefaults] setInteger:launches forKey:RecipeOpenCount];
   
    [[NSUserDefaults standardUserDefaults]synchronize];
}
+(void)resetCount
{
    
    NSInteger launches = [[NSUserDefaults standardUserDefaults]integerForKey:RecipeOpenCount];
    launches = 0;
    
    [[NSUserDefaults standardUserDefaults] setInteger:launches forKey:RecipeOpenCount];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
}


+(UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}


+(void)activatetextField:(UILabel *)lineLabel height:(NSLayoutConstraint *)height{
    lineLabel.backgroundColor = THEMECOLOR;
    height.constant = 2.0;
}


+(void)DeactivatetextField:(UILabel *)lineLabel height:(NSLayoutConstraint *)height{
    lineLabel.backgroundColor = [UIColor blackColor];
    height.constant = 1.0;
}


+(void)setImageOntextField:(UITextField *)textField andimage:(NSString *)imagename andMode:(BOOL)mode andframe:(CGRect)frame{
    
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:frame];
    imageV.image = [UIImage imageNamed:imagename];
    
    if (mode == YES){
        [textField setLeftViewMode:UITextFieldViewModeAlways];
        [textField setLeftView:imageV];
    }
    else{
        [textField setRightViewMode:UITextFieldViewModeAlways];
        [textField setRightView:imageV];
    }
}

+(NSDictionary *)manageDicOffers:(NSDictionary *)dicOffers
{
    NSMutableDictionary * diccoupons = [[NSMutableDictionary alloc]init];
    NSMutableArray* arrCouponsToDisplay = [[NSMutableArray alloc] init];

    NSMutableArray* arrCoupons = [[NSMutableArray alloc] init];
    NSMutableArray* arrLoyalty = [[NSMutableArray alloc] init];
    NSMutableArray* arrRewards = [[NSMutableArray alloc] init];
    NSMutableArray* arrOffers = [[NSMutableArray alloc] init];
    NSArray * arr_sections = dicOffers[@"Sections"];

    for (int i=0; i<arr_sections.count; i++) {
    NSDictionary *dicContent = arr_sections[i];
    if ([dicContent[@"Name"] isEqualToString:@"Coupons"]) {

        for (NSDictionary *dic in [dicContent objectForKey:@"Content"]) {
//            NSLog(@"dic ==== %@",dic);
            [arrCoupons addObject:dic];
            NSString * thumbURLStr = [dic valueForKey:@"ThumbUrl"];
            if (![thumbURLStr isEqual:[NSNull null]] && thumbURLStr != nil  && thumbURLStr.length > 0) {
                [arrCouponsToDisplay addObject:dic];
            }
        }
         [diccoupons setValue:dicContent forKey:@"Coupons"];
    }
    else if ([dicContent[@"Name"] isEqualToString:@"Loyalty"])
    {
        arrLoyalty = [[NSMutableArray alloc] init];
        for (NSDictionary *dic in [dicContent objectForKey:@"Content"]) {
            [arrLoyalty addObject:dic];
        }
        [diccoupons setValue:dicContent forKey:@"Loyalty"];
    }
    else if ([dicContent[@"Name"] isEqualToString:@"Rewards"])
    {
        arrRewards = [[NSMutableArray alloc] init];
        for (NSDictionary *dic in [dicContent objectForKey:@"Content"]) {
            [arrRewards addObject:dic];
            NSString * thumbURLStr = [dic valueForKey:@"ThumbUrl"];
            if (![thumbURLStr isEqual:[NSNull null]] && thumbURLStr != nil && thumbURLStr.length > 0 ) {
                [arrCouponsToDisplay addObject:dic];
            }
        }
        [diccoupons setValue:dicContent forKey:@"Rewards"];

    }
    else if ([dicContent[@"Name"] isEqualToString:@"Offers"])
    {
        arrOffers = [[NSMutableArray alloc] init];
        for (NSDictionary *dic in [dicContent objectForKey:@"Content"]) {
            [arrOffers addObject:dic];
        }
        [diccoupons setValue:dicContent forKey:@"Offers"];
        }
    }
    return diccoupons;
}

#pragma mark - Return Strring from Dictionary
+(NSData *)httpBodyForParamsDictionary:(NSDictionary *)paramDictionary
{
    NSMutableArray *parameterArray = [NSMutableArray array];
    
    [paramDictionary enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
        NSString *param = [NSString stringWithFormat:@"%@=%@", key, obj];
        [parameterArray addObject:param];
    }];
    
    NSString *string = [parameterArray componentsJoinedByString:@"&"];
    
    return [string dataUsingEncoding:NSUTF8StringEncoding];
}

#pragma mark - Return Consent Dictionary from Array

+(NSDictionary *)ReturnConsentDictionaryFromArray:(NSArray *)array forConsentType:(NSString *)consentType{
    NSArray * arrConsents = array;
    for (int i=0; i < arrConsents.count; i++)
    {
        NSDictionary * dicConsents = [[NSDictionary alloc ]init];
        dicConsents = arrConsents[i];
        NSDictionary * dic = [arrConsents[i]objectForKey:@"CurrentVersion"];
        if ([dicConsents[@"Name"] isEqualToString:consentType]) {
            return dicConsents;
        }
    }
    return nil;
}


#pragma mark - Strip HTML to give String

+(NSString *) stringByStrippingHTML:(NSString *)htmlString {
    NSRange r;
    NSString *s = [htmlString copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}
@end
