//
//  HelperClass.h
//  LiQUiD BARCODES
//
//  Created by HiteshDhawan on 23/12/15.
//  Copyright © 2015 Neuronimbus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface HelperClass : NSObject

+ (NSString *)getDateString;
+ (NSString *)GetGUID;
+ (BOOL) isValidEmail:(NSString *)emailString;
+(void)incrementOpenCount;
+(void)resetCount;
+(UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size;
+(void)activatetextField:(UILabel *)lineLabel height:(NSLayoutConstraint *)height;
+(void)DeactivatetextField:(UILabel *)lineLabel height:(NSLayoutConstraint *)height;
+(void)setImageOntextField:(UITextField *)textField andimage:(NSString *)imagename andMode:(BOOL)mode andframe:(CGRect)frame;
+(NSDictionary *)manageDicOffers:(NSDictionary *)dicoffers;
+(NSDictionary *)ReturnConsentDictionaryFromArray:(NSArray *)array forConsentType:(NSString *)consentType;
+(NSString *) stringByStrippingHTML:(NSString *)htmlString;
+(NSData *)httpBodyForParamsDictionary:(NSDictionary *)paramDictionary;
@end
