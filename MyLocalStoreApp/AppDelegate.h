//
//  AppDelegate.h
//  MyLocalStoreApp
//
//  Created by Surbhi on 08/11/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@import GoogleMaps;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;




@property (strong, nonatomic) NSString *strUserId;
@property (strong, nonatomic) NSDictionary *dicOffers;
@property (strong, nonatomic) NSString *strDeviceToken;
@property (strong, nonatomic) NSString *strMobileNumber;
@property (assign, nonatomic) BOOL isLoader;
@property (assign, nonatomic) BOOL viewOnceLoaded;
@property (assign, nonatomic) BOOL justLogedIn;
@property (assign, nonatomic) BOOL needAPIRefresh;
@property (strong, nonatomic) NSString * feedbackTime;
@property (strong, nonatomic) NSString * ratingId;
@property (strong, nonatomic) NSMutableDictionary * dict_PendingRatingStore;
@property (strong, nonatomic) NSMutableArray *geofences;
@property (strong, nonatomic) NSMutableArray *latestGeofences;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSArray *arrStores;
@property (assign, nonatomic) int backlogVal;
@property (assign, nonatomic) NSString * privacyPolicyStr;
@property (assign, nonatomic) NSNumber * minimumAgeFactor;
@property (strong, nonatomic) NSString * dobStringMinimumAge;
@property (strong, nonatomic) NSString * registrationTitle;

@property UIView *activityView1;
@property UIView *activityView2;
@property UIView *activityView3;

@property UIImageView *imageForR;
@property UIImageView *imageFor3rdAnimation;

@property UIView *logoView;
@property UIView *logoView3;

@property UIImageView *imageText;

+ (AppDelegate *) sharedInstance;

-(NSString *)getUniqueDeviceIdentifierAsString;
-(void)loadFeedbackBacklogValue:(BOOL)val;
-(void)changeRootViewController;
-(void)registerDevice;
-(void)loadStores;
-(void) registerForRemoteNotifications;
-(void) unregisterForRemoteNotifications;
-(void)setTopViewwithVC:(id)viewControl;
-(id)returnTopViewController;
-(void)startIndicator;
-(void)stopIndicator;
-(void)showWelcomePop:(NSString *)name;
-(void)hideWelcomePop;
-(void)createCustomLoaderForAPIwithImage:(NSString *)imageStr withAnimation:(BOOL)animate;
-(void)startIndicatorSHAREwithAnimation:(BOOL)animate;
-(void)stopIndicatorSHARE;

@end

